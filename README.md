Built with Laravel 5.8

PHP 7.2

After cloning this repository, run following command in terminal:
composer update

To login go to http://localhost:3000/timesheet and enter following credentials:

admin@test.com
password: 123456Qw

To signup for new account, go to http://localhost:3000/signup
