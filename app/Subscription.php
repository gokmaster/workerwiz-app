<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class Subscription extends Model
{
    public function assignUnusedSubscriptionPlanToCompany($companyId) {
        $url = env("LOGIN_API_URL") . "/api/subscription/unused/assign/tocompany"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $form_param = [
            'company_id' => $companyId,
        ];

        $HttpReq = new HttpRequest;
      
        return $HttpReq->post($url, $header, $form_param);
    }
}
?>