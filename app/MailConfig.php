<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class MailConfig extends Model
{
    public function fetch($codename){
        $url = env("LOGIN_API_URL") . "/api/mailconfig/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'codename' => $codename,
            'secretkey' => env("API_CALL_KEY"),
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url, $header, $postdata);
    }

    public function fetchInUse(){
        $url = env("LOGIN_API_URL") . "/api/mailconfig/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'in_use' => 1,
            'secretkey' => env("API_CALL_KEY"),
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url, $header, $postdata);
    }
}
?>