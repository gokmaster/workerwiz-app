<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class Menu extends Model
{
    public function menuItems(){
        $url = env("LOGIN_API_URL") . "/api/role/menu"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $form_param = [
            'user_id' => Session::get('user_details')['id'],
        ];

        $HttpReq = new HttpRequest;
      
        return $HttpReq->post($url, $header, $form_param);
    }
}
?>