<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class Customfield extends Model
{
    public function fieldsGroupedByFormSection($formName, $campaignId=0){
        $url = env("LOGIN_API_URL") . "/api/customfield/formfields/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $form_param = [
            'form' => $formName,
            'campaign_id' => $campaignId,
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $header, $form_param);

        if ($data) {
            $allFieldsGrouped = [];
            
            // create 2D-array with form_section as array-key of 1st level array
            foreach($data as $d) {
                if (!isset($allFieldsGrouped[$d['form_section']])) {
                    $allFieldsGrouped[$d['form_section']] = [];
                }
                array_push($allFieldsGrouped[$d['form_section']], $d);
            }
            return $allFieldsGrouped;
        }
        
        return $data;
    }

    public function fieldValuesOfLinkedTableRow($linkedtable, $linkedtableId){
        $url = env("LOGIN_API_URL") . "/api/customfield/linkedtablerow/values/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $form_param = [
            'linkedtable' => $linkedtable,
            'linkedtable_id' => $linkedtableId,
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url, $header, $form_param);
    }
}
?>