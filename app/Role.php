<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class Role extends Model
{
    public function usersPermittedTasks($userId) {
        $url = env("LOGIN_API_URL") . "/api/role/usertasks"; 
        
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'user_id' => $userId,
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $postdata);
    }

    public function userPermissions($userId) {
        $url = env("LOGIN_API_URL") . "/api/role/userpermissions"; 
        
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'user_id' => $userId,
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);
        return array_column($data, 'codename');
    }

    public function taskfetch($frontendView) {
        $url = env("LOGIN_API_URL") . "/api/task/fetch"; 
        
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'frontend_view' => $frontendView,
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $postdata);
    }
}
?>