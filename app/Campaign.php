<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class Campaign extends Model
{
    public function campaignsOfCompanyFetch(){
        $url = env("LOGIN_API_URL") . "/api/campaign/ofcompany/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $form_param = [
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url, $header, $form_param);
    }

   
}
?>