<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Config;
use Crypt;
use App\MailConfig;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $mailConfModel = new MailConfig;
        $mail = $mailConfModel->fetchInUse();

        $config = array(
            'driver'     => $mail['driver'],
            'host'       => $mail['host'],
            'port'       => $mail['port'],
            'from'       => array('address' => $mail['from_address'], 'name' => $mail['from_name']),
            'encryption' => $mail['encryption'],
            'username'   => $mail['username'],
            'password'   => Crypt::decrypt($mail['password']),
            'sendmail'   => '/usr/sbin/sendmail -bs',
            'pretend'    => false,
        );
        Config::set('mail', $config);
    }
}