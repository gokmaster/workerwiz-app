<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Menu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app', function($view) {
            // get menu for user's particular role and pass to main layout
           $m = new Menu;
           $menu = $m->menuItems();
                    
           $view->with('spData', array('menu' => $menu));
       });
    }
}
