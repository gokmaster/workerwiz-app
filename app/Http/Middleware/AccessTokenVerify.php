<?php

namespace App\Http\Middleware;

use App\Lib\HttpRequest;
use App\Lib\Url;
use Session;
use Closure;

class AccessTokenVerify {
    public function handle($request, Closure $next) {
        $domain = env("LOGIN_URL");

        $UrlClass = new Url;
        $redirectTo = bin2hex($UrlClass->currentUrl($_SERVER)); // url to redirect to after receive access token

        $receiveTokenUrl =  bin2hex($UrlClass->rootDomain($_SERVER) . "accesstoken/receive") ; // url that will receive the access-token via POST request
   
        $getTokenUrl = $domain . '/accesstoken/get/' . $redirectTo . '/' . $receiveTokenUrl;

        $data;

        // redirect user to login-app if he does not have access-token or if access-token is invalid
        if (Session::has('access_token')) {
            $url =  $domain . "/accesstoken/validate";

            $param = [
                'access_token' => Session::get('access_token'),
            ];

            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , [], $param);

            if ($data === null) {
                return redirect($getTokenUrl); // redirect to login-app to get access-token
            } else if (!array_key_exists("email",$data)) {
                // if access-token is successfully verified,
                // an array containing user details such as email is returned
                return redirect($getTokenUrl); // redirect to login-app to get access-token
            }
        } else {
            return redirect($getTokenUrl); // redirect to login-app to get access-token
        }

        Session::put('user_details', $data);

        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        // get company details of user
        $compUrl = env("LOGIN_API_URL") . "/api/company/fetch";
        $HttpReq = new HttpRequest;
        $postdata = ['company_id' => $data['company_id']];
        $companyData = $HttpReq->post($compUrl , $header, $postdata);
        Session::put('company_details', $companyData);

        return $next($request);
    }
}
