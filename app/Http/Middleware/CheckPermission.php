<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Route;
use Session;
use App\Role;
use App\Subscription;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        $userId = Session::get('user_details')['id'];

        $rolesModel = new Role;
        $data = $rolesModel->usersPermittedTasks($userId);

        $allRoutes = $data['all_tasks'];
             
        $destinationURL = class_basename(Route::currentRouteAction());

        // first check if user's company has subscribed to the appropriate plan for this task
        $taskBeingAccessed = $rolesModel->taskfetch($destinationURL);
        $company = Session::get('company_details');

        if ($company['subscription_plan_no'] < $taskBeingAccessed['subscription_plan_no']) {
            // if company's subscription plan is lower than the minimum required plan for the task
            return redirect()->route('subscription-upgrade',['expired'=>0]);
        } else if ($taskBeingAccessed['subscription_plan_no'] > 1 // if it requires a subscription-plan that is above a free-plan
                    && strtotime($company['subscription_expirydate']) < strtotime('now') ) { // and company's subscription plan already expired
            // extend company's subscription expiry-date if it has any unused subscriptions
            $model = new Subscription;
            $assigned = $model->assignUnusedSubscriptionPlanToCompany($company['id']);
            
            if ($assigned['success'] == false) { // if no unused subscriptions assigned
                // redirect to subscription-expired page
                return redirect()->route('subscription-upgrade',['expired'=>1]);
            }
        }
                        
        $userPermissions = $data['user_task_ids']; 
      
        $allowed = false; //initialized to false
        
        foreach($allRoutes as $key => $route){ //go through each route...
            if(strcmp($destinationURL, $route['frontend_view']) === 0 ){ //...in order to find out if your destination is in the list
                //if it IS, check if the key is in the allowed user permission
                $allowed = in_array($route['id'], $userPermissions); //return true if the key (i.e permissionId) is also in the $userPermission array

                if($allowed && strcmp($destinationURL, $route['frontend_view']) == 0 ){ //if it is allowed, let the user through
                    //dd($route['frontend_view']);
                    return $next($request);    
                }
            }
        }

        //if it is not redirect to whereversafe (or error message if you disabled the front end).
        return redirect()->route('permission-denied');
    }
}