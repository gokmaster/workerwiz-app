<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;
use App\Role;

class HomeController extends Controller {    
    public function index() {
        //return Session::get('company_details');
        //return Session::get('user_details');
        //return  Session::get('access_token');
        return redirect(route('timesheet-form'));
    }

}