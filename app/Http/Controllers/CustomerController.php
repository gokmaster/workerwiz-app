<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use App\Country;
use App\Customfield;
use App\Campaign;
use App\Role;
use Config;
use Crypt;
use DateTime;
use Session;

class CustomerController extends Controller { 
    public function createForm(Request $request) {  
        $cpModel = new Campaign;
        $campaigns = $cpModel->campaignsOfCompanyFetch();
        
        // if campaigns exist AND $_GET does NOT have 'campaign' parameter
        if ($campaigns && !$request->exists('campaign')) {
            return view('customer/customer_choose_campaign',[
                'campaigns' => $campaigns,
            ]);
        }

        $model = new Country; 
        $fieldModel = new Customfield;

        $campaignId = 0;
        $campaign = "";

        if ($request->exists('campaignid')) {
            $campaignId = hex2bin($_GET['campaignid']);
        }

        if ($request->exists('campaign')) {
            $campaign = hex2bin($_GET['campaign']);
        }
        
        return view('customer/customer_create',[
            'countries' => $model->countriesList(),
            'customfields' => $fieldModel->fieldsGroupedByFormSection('customer', $campaignId),
            'campaign_id' => $campaignId,
            'campaign' => $campaign,
        ]);
    }

    public function editForm($customerId) {
        $model = new Country; 
        $fieldModel = new Customfield;

        $data = $this->customerDataFetch($customerId);

        if (isset($data['creditcard_number'])) {
            if ($data['creditcard_number'] != null) {
                $data['creditcard_number'] = Crypt::decrypt($data['creditcard_number']);
            }
        }

        $priceComfortMin = 100;
        $priceComfortMax = 300;
        if (isset($data['price_comfort'])) {
            if ($data['price_comfort'] != null) {
                $priceComfort = explode(" - ",$data['price_comfort']);
                $priceComfortMin = trim($priceComfort[0],"$");
                $priceComfortMax = trim($priceComfort[1],"$");
            }
        }
      
        return view('customer/customer_edit',[
            'countries' => $model->countriesList(),
            'customfields' => $fieldModel->fieldsGroupedByFormSection('customer', $data['campaign_id']),
            'customfieldValues' => $fieldModel->fieldValuesOfLinkedTableRow('customer', $data['id']),
            'customer' => $data,
            'priceComfortMin' => $priceComfortMin,
            'priceComfortMax' => $priceComfortMax,
        ]);
    }

    public function create(Request $request) {     
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
            ]);
            
            $postdata = $request->except('_token');
            $postdata['company_id'] = Session::get('user_details')['company_id'];

            if (isset($postdata['creditcard_number'])) {
                $postdata['creditcard_number'] = Crypt::encrypt($postdata['creditcard_number']);
            }

            if (isset($postdata['card_expiry_date'])) {
                $expDate = '01/'.$postdata['card_expiry_date']; // format from mm/yyyy to dd/mm/yyyy
                $expDate = date_format(date_create_from_format('d/m/Y', $expDate), 'Y-m-d'); // format to yyyy-mm-dd
                $d = new DateTime($expDate);  
                $postdata['card_expiry_date'] = $d->format('Y-m-t'); // get last day of month
            }
                                      
            $url = env("LOGIN_API_URL") . "/api/customer/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return redirect()->back()->with('success_message', json_encode($data['message']));
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input            
            return redirect(route('customer-all'))->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function edit(Request $request) {     
        try {
            $this->validate($request, [
                'customer_id' => 'required',
                'fname' => 'required',
                'lname' => 'required',
            ]);
            
            $postdata = $request->except('_token');
            $postdata['company_id'] = Session::get('user_details')['company_id'];

            if (isset($postdata['creditcard_number'])) {
                $postdata['creditcard_number'] = Crypt::encrypt($postdata['creditcard_number']);
            }

            if (isset($postdata['card_expiry_date'])) {
                $expDate = '01/'.$postdata['card_expiry_date']; // format from mm/yyyy to dd/mm/yyyy
                $expDate = date_format(date_create_from_format('d/m/Y', $expDate), 'Y-m-d'); // format to yyyy-mm-dd
                $d = new DateTime($expDate);  
                $postdata['card_expiry_date'] = $d->format('Y-m-t'); // get last day of month
            }

            $fieldModel = new Customfield;
            $customfieldValues = $fieldModel->fieldValuesOfLinkedTableRow('customer', $postdata['customer_id']);

            if ($customfieldValues) {
                $inputCodenames = array_column($customfieldValues, 'input_codename');

                foreach ($inputCodenames as $ic) {
                    if (!isset($postdata[$ic])) {
                        $postdata[$ic] = ""; // assign blank-value if this input has no value but had a value previously
                    }
                }
            }            
                                      
            $url = env("LOGIN_API_URL") . "/api/customer/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return redirect(route('customer-all'))->with('success_message', json_encode($data['message']));
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input            
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function fetchByNameEmailPhone(Request $request) {
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['company_id'] =  Session::get('user_details')['company_id'];

            if (!isset($postdata['email'])) {
                $postdata['email'] = "";
            }

            if (!isset($postdata['phone'])) {
                $postdata['phone'] = "";
            }
            
            $url = env("LOGIN_API_URL") . "/api/customer/fetchby/name"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data) {
                return json_encode([
                    'success' => true,
                    'customer_data' => $data,
                ]);
            } else {
                return json_encode([
                    'success' => false,
                    'message' => "Customer record not found",
                ]);
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]);
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]); 
        }
    }

    public function customerAll() {
        $model = new Role;
        return view('customer/customer_all',[
            'customers' => $this->customerAllFetch(),
            'viewerPermissions' => $model->userPermissions(Session::get('user_details')['id']),
        ]);
    }

    public function customerProfile($customerId) {
        $data = $this->customerDataFetch($customerId);

        if ($data['creditcard_number'] != null) {
            $data['creditcard_number'] = Crypt::decrypt($data['creditcard_number']);
        }

        $model = new Role;
       
        return view('customer/customer_profile',[
           'customer' => $data,
           'viewerPermissions' => $model->userPermissions(Session::get('user_details')['id']),
        ]);
    }

    private function customerDataFetch($customerId) {
        $url = env("LOGIN_API_URL") . "/api/customer/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'customer_id' => $customerId,
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $postdata);
    }

    private function customerAllFetch() {
        $url = env("LOGIN_API_URL") . "/api/customer/ofcompany/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];

        $HttpReq = new HttpRequest;
        $customers = $HttpReq->post($url , $header, $postdata);

        if (count($customers)) {
            foreach ($customers as $key=>$cust) {
                if ($cust['creditcard_number'] != null) {
                    $customers[$key]['creditcard_number'] = Crypt::decrypt($cust['creditcard_number']);
                }
            }  
        }
        return $customers; 
    }

}