<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;

class JobtitleController extends Controller { 
   public function jobtitleCreateForm(){
      return view('jobtitle/jobtitle_create',[
         'jobtitles' => $this->fetch(),
      ]);
   }

   public function jobtitleCreate(Request $request){
      try {
         $this->validate($request, [
            'job_title' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                         
         $url = env("LOGIN_API_URL") . "/api/jobtitle/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function jobtitleEdit(Request $request){
      try {
         $this->validate($request, [
             'job_title' => 'required',
         ]);
             
         $postdata = $request->except('_token');
                         
         $url = env("LOGIN_API_URL") . "/api/jobtitle/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('jobtitle/partial/jobtitle_table',[
                        'jobtitles' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => json_encode($e->errors()),
         ]);
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   public function jobtitleDelete(Request $request) {
      try {
         $postdata = $request->except('_token');
                       
         $url = env("LOGIN_API_URL") . "/api/jobtitle/delete"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('jobtitle/partial/jobtitle_table',[
                        'jobtitles' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   private function fetch() {
      $url = env("LOGIN_API_URL") . "/api/jobtitles"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $HttpReq = new HttpRequest;
      $postdata = [
         'company_id' => Session::get('user_details')['company_id']
      ];
      return $HttpReq->post($url , $header, $postdata);
   }
}