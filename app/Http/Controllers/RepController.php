<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;

class RepController extends Controller { 
   public function repCreateForm(){
      return view('rep/rep_create',[
         'reps' => $this->fetch(),
      ]);
   }

   public function repCreate(Request $request){
      try {
         $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'country' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                         
         $url = env("LOGIN_API_URL") . "/api/rep/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function repEdit(Request $request){
      try {
         $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'country' => 'required',
         ]);
             
         $postdata = $request->except('_token');
                         
         $url = env("LOGIN_API_URL") . "/api/rep/edit"; 
         $postdata['company_id'] = Session::get('user_details')['company_id'];
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('rep/partial/rep_table',[
                        'reps' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => json_encode($e->errors()),
         ]);
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   public function repDelete(Request $request) {
      try {
         $postdata = $request->except('_token');
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                       
         $url = env("LOGIN_API_URL") . "/api/rep/delete"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('rep/partial/rep_table',[
                        'reps' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   private function fetch() {
      $url = env("LOGIN_API_URL") . "/api/rep/all"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];
      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }
}