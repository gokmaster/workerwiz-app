<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;

class DepartmentController extends Controller { 
   public function departmentCreateForm(){
      return view('department/department_create',[
         'departments' => $this->fetch(),
      ]);
   }

   public function departmentCreate(Request $request){
      try {
         $this->validate($request, [
            'department_name' => 'required',
         ]);
             
         $postdata = $request->except('_token');
                         
         $url = env("LOGIN_API_URL") . "/api/department/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function departmentEdit(Request $request){
      try {
         $this->validate($request, [
             'department_name' => 'required',
         ]);
             
         $postdata = $request->except('_token');
                         
         $url = env("LOGIN_API_URL") . "/api/department/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('department/partial/department_table',[
                        'departments' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => json_encode($e->errors()),
         ]);
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   public function departmentDelete(Request $request) {
      try {
         $postdata = $request->except('_token');
                       
         $url = env("LOGIN_API_URL") . "/api/department/delete"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('department/partial/department_table',[
                        'departments' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   private function fetch() {
      $url = env("LOGIN_API_URL") . "/api/departments"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , $header, []);
   }
}