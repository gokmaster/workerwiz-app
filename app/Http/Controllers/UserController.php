<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use App\Lib\Utils\Paginate;
use App\Lib\Utils\Timezone;
use Session;
use Mail;
use App\Role;
use App\MailConfig;

class UserController extends Controller {    
    public function signupForm() {
        $timezone = new Timezone;
        
        return view('signup/signup', [
            'timezones' => $timezone->timezoneListing(),
        ]);
    }

    public function signup(Request $request) {
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'password' => [
                    'required',
                    'min:8',             // must be at least 8 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                ],
                'company_name' => 'required',
                'timezone' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $email = $postdata['email'];
            $postdata['password'] = password_hash($postdata['password'], PASSWORD_BCRYPT);
            $postdata['verification_code'] = md5($email.time().mt_rand());
            
            // remove this items
            unset($postdata['re_password']);
            unset($postdata['agree-term']);
            
            $url = env("LOGIN_API_URL") . "/api/unverifieduser/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $this->sendSignupEmail($email, $postdata['fname'], $postdata['verification_code']);
                return view('signup/signup_success', [
                    'email' => $email,
                ]);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            $msg = $e->errors();
            if (isset($msg['password'])) {
                $msg = "Password must contain atleast 1 digit, 1 uppercase letter, 1 lowercase letter, and be 8 characters long.";
            }
            return redirect()->back()->with('fail_message', json_encode($msg))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function signupEmailVerify($verification_code) {
        try {                      
            $url = env("LOGIN_API_URL") . "/api/unverifieduser/email/verify"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];

            $postdata = [
                'verification_code' => $verification_code
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return view('signup/signup_email_verify', [
                    'success' => true,
                    'message' => $data['message']
                ]);
            } else {
                return view('signup/signup_email_verify', [
                    'success' => false,
                    'message' => $data['message']
                ]);
            }
        } catch(\Exception $e) {
            return view('signup/signup_email_verify', [
                'success' => false,
                'message' => $e->getMessage()
            ]); 
        } 
    }

    public function invitedUserCreateForm($verification_code) {
        try {                      
            $url = env("LOGIN_API_URL") . "/api/user/invited/verify"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];

            $postdata = [
                'verification_code' => $verification_code
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return view('signup/invited_user_create', [
                    'success' => true,
                    'message' => $data['message'],
                    'user' => $data['user'],
                ]);
            } else {
                return view('signup/invited_user_create', [
                    'success' => false,
                    'message' => $data['message']
                ]);
            }
        } catch(\Exception $e) {
            return view('signup/invited_user_create', [
                'success' => false,
                'message' => $e->getMessage()
            ]); 
        } 
    }

    public function invitedUserCreate(Request $request) {
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'password' => [
                    'required',
                    'min:8',             // must be at least 8 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                ],
                'verification_code' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['password'] = password_hash($postdata['password'], PASSWORD_BCRYPT);
                       
            // remove this items
            unset($postdata['re_password']);
            unset($postdata['agree-term']);
            
            $url = env("LOGIN_API_URL") . "/api/user/invited/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return view('signup/invited_usercreate_success');
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            $msg = $e->errors();
            if (isset($msg['password'])) {
                $msg = "Password must contain atleast 1 digit, 1 uppercase letter, 1 lowercase letter, and be 8 characters long.";
            }
            return redirect()->back()->with('fail_message', json_encode($msg))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function createUserForm() {
        return view('user/usercreate', [
            'roles' => $this->allRoles(),
            'departments' => $this->departmentsAll(),
            'jobtitles' => $this->jobTitleAll(),
        ]);
    }

    public function editUserForm($userId) {
        return view('user/useredit', [
            'userdata' => $this->userDetails($userId),
            'roles' => $this->allRoles(),
            'departments' => $this->departmentsAll(),
            'jobtitles' => $this->jobTitleAll(),   
        ]);
    }

    public function userEditOwnForm() {
        $userId = Session::get('user_details')['id'];
        return view('user/useredit_own', [
            'userdata' => $this->userDetails($userId) 
        ]);
    }

    public function userInviteCreateForm() {
        return view('user/user_invite', [
            'roles' => $this->allRoles(),
        ]);
    }   

    public function createUser(Request $request) {
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'password' => 'min:6',
                'dob' => 'required',
                'emp_startdate' => 'required',
                'weekly_normal_hours' => 'required|numeric|min:0',
                'hourly_rate' => 'required|numeric|min:0',
                'role_id' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['dob'] = date("Y-m-d", strtotime($postdata['dob']) ); // format date to yyyy-mm-dd
            $postdata['emp_startdate'] = date("Y-m-d", strtotime($postdata['emp_startdate']) ); // format date to yyyy-mm-dd
            $postdata['work_starttime'] = date("H:i:s", strtotime($postdata['work_starttime']) );
            $postdata['work_endtime'] = date("H:i:s", strtotime($postdata['work_endtime']) );
            $postdata['company_id'] = Session::get('user_details')['company_id'];

            $allowedApps = ['1']; // 1 is app-ID of HR service app
            $postdata['allowed_apps'] = json_encode($allowedApps);

            if ($postdata['emp_enddate'] !== null) {
                $postdata['emp_enddate'] = date("Y-m-d", strtotime($postdata['emp_enddate']) ); // format date to yyyy-mm-dd
            }

            $pword = mt_rand(22405,9898899);
            $postdata['password'] = password_hash($pword, PASSWORD_BCRYPT);
            
            if (array_key_exists('profilepic', $postdata)) {
                 // image upload
                request()->validate([
                    'profilepic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:9048',
                ]);
                $imageName = mt_rand(1,9898899) . time(). '.'.request()->profilepic->getClientOriginalExtension();
                request()->profilepic->move(public_path('images'), $imageName);
                $postdata['profilepic'] = $imageName;
            }

            $url = env("LOGIN_API_URL") . "/api/user/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $this->sendAccountCreatedEmail($postdata['email'], $postdata['fname'], $pword);
                return redirect(route('user-all'))->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function editUser(Request $request) {
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'password' => 'min:6',
                'dob' => 'required',
                'emp_startdate' => 'required',
                'weekly_normal_hours' => 'required|numeric|min:0',
                'hourly_rate' => 'required|numeric|min:0',
                'role_id' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['dob'] = date("Y-m-d", strtotime($postdata['dob']) ); // format date to yyyy-mm-dd
            $postdata['emp_startdate'] = date("Y-m-d", strtotime($postdata['emp_startdate']) ); // format date to yyyy-mm-dd
            $postdata['work_starttime'] = date("H:i:s", strtotime($postdata['work_starttime']) );
            $postdata['work_endtime'] = date("H:i:s", strtotime($postdata['work_endtime']) );

            if ($postdata['emp_enddate'] !== null) {
                $postdata['emp_enddate'] = date("Y-m-d", strtotime($postdata['emp_enddate']) ); // format date to yyyy-mm-dd
            }

            if (array_key_exists('profilepic', $postdata)) {
                 // image upload
                request()->validate([
                    'profilepic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:9048',
                ]);
                $imageName = mt_rand(1,9898899) . time(). '.'.request()->profilepic->getClientOriginalExtension();
                request()->profilepic->move(public_path('images'), $imageName);
                $postdata['profilepic'] = $imageName;
            }

            $url = env("LOGIN_API_URL") . "/api/user/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect(route('user-all'))->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function userEditOwn(Request $request) {
        try {
            $this->validate($request, [
                'dob' => 'required'
            ]);

            $postdata = $request->except('_token');
            $postdata['dob'] = date("Y-m-d", strtotime($postdata['dob']) ); // format date to yyyy-mm-dd
            $userId = Session::get('user_details')['id'];
            $postdata['id'] = $userId;
          
            if (array_key_exists('profilepic', $postdata)) {
                 // image upload
                request()->validate([
                    'profilepic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:9048',
                ]);
                $imageName = mt_rand(1,9898899) . time(). '.'.request()->profilepic->getClientOriginalExtension();
                request()->profilepic->move(public_path('images'), $imageName);
                $postdata['profilepic'] = $imageName;
            }

            $url = env("LOGIN_API_URL") . "/api/user/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect(route('user-profile', ['user_id'=>$userId]))->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function userInviteCreate(Request $request) {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'emp_startdate' => 'required',
                'work_starttime' => 'required',
                'work_endtime' => 'required',
                'weekly_normal_hours' => 'required|numeric|min:0',
                'hourly_rate' => 'required|numeric|min:0',
                'role_id' => 'required',
            ]);
              
            $postdata = $request->except('_token');
            $email = $postdata['email'];  
            $postdata['emp_startdate'] = date("Y-m-d", strtotime($postdata['emp_startdate']) ); // format date to yyyy-mm-dd
            $postdata['work_starttime'] = date("H:i:s", strtotime($postdata['work_starttime']) );
            $postdata['work_endtime'] = date("H:i:s", strtotime($postdata['work_endtime']) );
            $postdata['company_id'] = Session::get('user_details')['company_id'];
            $postdata['verification_code'] = md5($postdata['company_id'].$email.time().mt_rand());

            $url = env("LOGIN_API_URL") . "/api/user/invite/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $this->sendUserInviteEmail($email, $postdata['name'], $postdata['verification_code']);
                $msg = "Successfully sent user-invite to $email";
                return redirect()->back()->with('success_message', $msg);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function deleteUser(Request $request) {
        try {
            $postdata = $request->except('_token');
       
            $url = env("LOGIN_API_URL") . "/api/user/delete"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $html =  view('user/partial/users_table',[
                    'users' => $this->usersFetch(),
                 ])->render();

                return json_encode([
                    'success' => true,
                    'message' => $data['message'],
                    'html' => $html,
                ]);
            } else {
                return json_encode([
                    'success' => true,
                    'message' => $data['message'],
                ]); 
            }
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function unArchiveUser(Request $request) {
        try {
            $postdata = $request->except('_token');
       
            $url = env("LOGIN_API_URL") . "/api/user/unarchive"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $html =  view('user/partial/users_archived_table',[
                    'users' => $this->archivedUsersFetch(),
                 ])->render();

                return json_encode([
                    'success' => true,
                    'message' => $data['message'],
                    'html' => $html,
                ]);
            } else {
                return json_encode([
                    'success' => true,
                    'message' => $data['message'],
                ]); 
            }
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function allUsers() {
        return view('user/users', [
            'users' => $this->usersFetch()
        ]);
    }

    public function allArchivedUsers() {
        return view('user/users_archived', [
            'users' => $this->archivedUsersFetch()
        ]);
    }

    public function allProfiles(Request $request) {
        $paginate = new Paginate;
        $data = $paginate->makePaginate($this->usersFetch(), 21, $request); //pagination 21 items per page

        return view('user/profile_all', [
            'users' => $data
        ]);
    }

    public function userProfile($userId) {
        $model = new Role;
        $user = Session::get('user_details');
        $userData = $this->userDetails($userId);

        if (isset($userData['success'])) {
            if ($userData['success'] == false) {
                return $userData['message'];
            }
        }
        
        return view('user/profile', [
            'userdata' => $userData,
            'loggedInUser' => $user,
            'viewerPermissions' => $model->userPermissions($user['id']),
            'attachmentTypes' => $this->attachmentTypeOfGroupFetch('profile'),
            'attachments' => $this->profileAttachmentsOfUser($userId),
            'timezone' => Session::get('company_details')['timezone'],
        ]);
    }

    public function passwordChangeForm() {
        return view('user/passwordchange', [
            'userId' => Session::get('user_details')['id']
        ]);
    }

    public function passwordChange(Request $request) {   
        try {
            $this->validate($request, [
                'password' => 'required',
                'new_password' =>  [
                    'required',
                    'min:8',             // must be at least 8 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                ],
                'user_id' => 'required',
            ]);
                
            $postdata = $request->except('_token');
                      
            $url = env("LOGIN_API_URL") . "/api/user/password/change"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect()->back()->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', $data['message'])->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            $msg = $e->errors();
            if (isset($msg['new_password'])) {
                $msg = "Password must contain atleast 1 digit, 1 uppercase letter, 1 lowercase letter, and be 8 characters long.";
            }
            return redirect()->back()->with('fail_message', $msg)->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        } 
    }

    public function resetUserPasswordForm() {
        return view('user/user_passwordreset', [
            'users' => $this->usersFetch()
        ]);
    }

    public function resetUserPassword(Request $request) {
        try {                
            $postdata = $request->except('_token');
                      
            $url = env("LOGIN_API_URL") . "/api/user/edit"; 

            $pword = mt_rand(22405,9898899);
            $postdata['password'] = password_hash($pword, PASSWORD_BCRYPT);

            $userData = $this->userDetails($postdata['id']);

            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $this->sendPasswordResetEmail($userData['email'], $pword);
                return redirect()->back()->with('success_message', "Successfully resetted password");
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    private function profileAttachmentsOfUser($userId) {
        $url = env("LOGIN_API_URL") . "/api/attachment/profile/ofuser"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'user_id' => $userId,
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function attachmentTypeOfGroupFetch($group) {
        $url = env("LOGIN_API_URL") . "/api/attachmenttype/ofgroup/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'group' => $group,
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function userDetails($userId) {
        $url = env("LOGIN_API_URL") . "/api/user/details"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'user_id' => $userId,
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function jobTitleAll() {
        $url = env("LOGIN_API_URL") . "/api/jobtitles"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function departmentsAll() {
        $url = env("LOGIN_API_URL") . "/api/departments"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, []);
    }

    private function allRoles() {
        $url = env("LOGIN_API_URL") . "/api/role/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function usersFetch() {
        $url = env("LOGIN_API_URL") . "/api/users"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function archivedUsersFetch() {
        $url = env("LOGIN_API_URL") . "/api/users/archived"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function sendAccountCreatedEmail($mailTo, $name, $pword)
    {        
        $data = [
            'name' => $name,
            'password' => $pword
        ];

        $mailConfModel = new MailConfig;
        $mailConfig = $mailConfModel->fetch("default");
        $mailFrom = $mailConfig['from_address'];

        Mail::send('mail/account_created_pword', $data, function($message) use($mailTo, $mailFrom, $name) {
            $message->to($mailTo, $name)->subject(env('APP_NAME') . " User Account Created");
            $message->from($mailFrom,'noreply');
        }); 
    }

    private function sendPasswordResetEmail($mailTo, $pword)
    {        
        $data = [
            'password' => $pword
        ];

        $mailConfModel = new MailConfig;
        $mailConfig = $mailConfModel->fetch("default");
        $mailFrom = $mailConfig['from_address'];

        Mail::send('mail/pword_reset', $data, function($message) use($mailTo, $mailFrom) {
            $message->to($mailTo)->subject(env('APP_NAME').' User Account Password Resetted');
            $message->from($mailFrom,'noreply');
        }); 
    }

    private function sendSignupEmail($mailTo, $name, $verificationCode) {
        $data = [
            'name' => $name,
            'verificationCode' => $verificationCode
        ];

        $mailConfModel = new MailConfig;
        $mailConfig = $mailConfModel->fetch("default");
        $mailFrom = $mailConfig['from_address'];

        Mail::send('mail/signup_email_verify', $data, function($message) use($mailTo, $mailFrom) {
            $message->to($mailTo)->subject("Verify Email Address for your ". env('APP_NAME') ." User Account");
            $message->from($mailFrom,'noreply');
        }); 
    }

    private function sendUserInviteEmail($mailTo, $name, $verificationCode) {
        $company = ucfirst(Session::get('company_details')['company_name']);
        $data = [
            'name' => $name,
            'verificationCode' => $verificationCode,
            'company' => $company,
        ];

        $mailConfModel = new MailConfig;
        $mailConfig = $mailConfModel->fetch("default");
        $mailFrom = $mailConfig['from_address'];

        Mail::send('mail/user_invite', $data, function($message) use($mailTo, $mailFrom, $company) {
            $message->to($mailTo)->subject("Invite from $company to create ". env('APP_NAME') ." User Account");
            $message->from($mailFrom,'noreply');
        }); 
    }
}