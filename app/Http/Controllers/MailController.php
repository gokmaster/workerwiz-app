<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller {
   public function basic_email(){
      $data = array('name'=>"user444");

      $email = "gok@supercharged.com.fj"; 

      // Mail::to($email)
      //    ->cc(['go2012.gp@gmail.com'])
      //    ->send(new document());
   
     Mail::send('mail', $data, function($message) {
         $message->to("gok@supercharged.com.fj", 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from(env('MAIL_USERNAME'),'noreply');
      });
      echo "Basic Email Sent. Check your inbox.";
   }
   
   public function html_email(){
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('gok@supercharged.com.fj', 'Tutorials Point')->subject
            ('Laravel HTML Testing Mail');
         $message->from(env('MAIL_USERNAME'),'noreply');
      });
      echo "HTML Email Sent. Check your inbox.";
   }
   public function attachment_email(){
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('gok@supercharged.com.fj', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
         $filepath = storage_path('payroll/payslip/ADI_ARIETA_LEWANAVANUA_15758537261754051821.pdf');
         $message->attach($filepath);
         $message->from(env('MAIL_USERNAME'),'noreply');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }
}