<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
use Mail;
use Crypt;
use DateTime;
use App\MailConfig;

class SubscriptionController extends Controller {
    public function upgradeSubscriptionForm($expired) {
        return view('subscription/subscription_upgrade', [
            'plan5' => $this->planFetch(5),
            'plan10' => $this->planFetch(10),
            'expired' => $expired
        ]);  
    }

    public function order(Request $request) {
        try {
            $this->validate($request, [
               'subscription_plan_no' => 'required|integer|not_in:0|min:1',
               'subscription_period' => 'required|integer|not_in:0|min:1',
               'user_quantity' => 'required|integer|not_in:0|min:1',
            ]);
            
            $postdata = $request->except('_token');
            $plan = $this->planFetch($postdata['subscription_plan_no']);

            $postdata['company_id'] = Session::get('user_details')['company_id'];
            $postdata['created_by'] = Session::get('user_details')['id'];
            $postdata['subscription_period_days'] = $postdata['subscription_period'] * 30;
            $postdata['subscription_period_months'] = $postdata['subscription_period'];
            $postdata['payment_amount'] = $plan['price'] * $postdata['subscription_period_months'] * $postdata['user_quantity'];
               
            $url = env("LOGIN_API_URL") . "/api/subscriptionplan/order/create"; 
            
            $header = [
               'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
           
            if ($data['success'] == true) {
                $postdata['subscription_plan_order_id'] = $data['subscription_plan_order_id'];
                $postdata['plan_title'] = $plan['title'];
                return view('subscription/subscription_orderdetails', [
                    'order' => $postdata,
                ]); 
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function paymentForm(Request $request) {
        try {
            $this->validate($request, [
               'subscription_plan_order_id' => 'required',
            ]);
            
            $postdata = $request->except('_token');

            return view('subscription/subscription_payment', [
                'subscription_plan_order_id' => $postdata['subscription_plan_order_id']
            ]);              
           
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function payment(Request $request) {
        try {
            $this->validate($request, [
               'subscription_plan_order_id' => 'required',
               'cardholder_name' => 'required',
               'creditcard_number' => 'required',
               'card_expiry_date' => 'required',
               'cvv' => 'required',
            ]);
            
            $postdata = $request->except('_token');
            
            $postdata['company_id'] = Session::get('user_details')['company_id'];
            $postdata['creditcard_number'] = Crypt::encrypt($postdata['creditcard_number']);

            if (isset($postdata['card_expiry_date'])) {
                $expDate = '01/'.$postdata['card_expiry_date']; // format from mm/yyyy to dd/mm/yyyy
                $expDate = date_format(date_create_from_format('d/m/Y', $expDate), 'Y-m-d'); // format to yyyy-mm-dd
                $d = new DateTime($expDate);  
                $postdata['card_expiry_date'] = $d->format('Y-m-t'); // get last day of month
            }

            $url = env("LOGIN_API_URL") . "/api/subscriptionplan/order/edit"; 
            
            $header = [
               'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect(route('subscription-payment-response'))->with('success_message', "Success"); 
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function paymentResponsePage() {
        return view('subscription/subscription_payment_response'); 
    }

    public function ordersAll() {
        $url = env("LOGIN_API_URL") . "/api/subscriptionplan/order/unpaid/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, []);

        return view('subscription/subscription_order_all',[
            'orders' => $data
        ]);
    }

    public function companySubscriptionCreate(Request $request) {
        try {
            $this->validate($request, [
               'subscription_plan_order_id' => 'required',
            ]);
            
            $postdata = $request->except('_token');
                          
            $url = env("LOGIN_API_URL") . "/api/subscription/forcompany/create"; 
            
            $header = [
               'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $this->sendSubscripOrderPaymentSuccessEmail($postdata['subscription_plan_order_id']);
                return redirect()->back()->with('success_message', "Successfully sent payment-receipt confirmation email"); 
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    private function sendSubscripOrderPaymentSuccessEmail($subscripPlanOrderId) {
        $mailConfModel = new MailConfig;
        $mailConfig = $mailConfModel->fetchInUse();
        $mailFrom = $mailConfig['from_address'];

        $data = $this->userWhoCreatedOrderFetch($subscripPlanOrderId);
        $mailTo = $data['email'];
        $subscripPlan = ucfirst($data['subscription_plan']);
        $subject = env('APP_NAME').": Payment for $subscripPlan Subscription Plan Successful";
       
        Mail::send('mail/subscriptionplan_pay_success', $data, function($message) use($mailTo, $mailFrom, $subject) {
            $message->to($mailTo, '')->subject($subject);
            $message->from($mailFrom,'noreply');
        }); 
    }

    private function userWhoCreatedOrderFetch($subscripPlanOrderId) {
        $HttpReq = new HttpRequest;
        $url = env("LOGIN_API_URL") . "/api/subscriptionplan/order/createdby"; 

        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        $postdata = [
            'subscription_plan_order_id' => $subscripPlanOrderId,
        ];

        return $HttpReq->post($url , $header, $postdata);
    }

    private function planFetch($planNo) {
        $HttpReq = new HttpRequest;
        $url = env("LOGIN_API_URL") . "/api/subscriptionplan/fetch"; 
        $postdata = [
            'subscription_plan_no' => $planNo,
        ];
        return $HttpReq->post($url , [], $postdata);
    }
}