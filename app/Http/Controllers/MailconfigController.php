<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
use Crypt;

class MailconfigController extends Controller {
   public function mailServerAddForm(Request $request) {     
      return view('mailconfig/mailserver_add');
   }

   public function mailServerAdd(Request $request) {     
      try {
          $this->validate($request, [
              'driver' => 'required',
              'host' => 'required',
              'port' => 'required|numeric|min:0',
              'username' => 'required',
              'password' => 'required',
              'from_address' => 'required',
          ]);
          
          $postdata = $request->except('_token');
          $postdata['password'] = Crypt::encrypt($postdata['password']);
          $postdata['codename'] = $postdata['host'];
          $postdata['company_id'] = Session::get('user_details')['company_id'];
                       
          $url = env("LOGIN_API_URL") . "/api/mailconfig/create"; 
          
          $header = [
              'Authorization' => 'Bearer ' . Session::get('access_token'),        
          ];
  
          $HttpReq = new HttpRequest;
          $data = $HttpReq->post($url , $header, $postdata);

          if ($data['success'] == true) {
              return redirect()->back()->with('success_message', json_encode($data['message']));
          } else {
              return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
          }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
          // When there is any invalid input            
          return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
          return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function mailServerChange(Request $request) {     
      try {
         $this->validate($request, [
            'mailconfig_id' => 'required',
         ]);
         
         $postdata = $request->except('_token');
                              
         $url = env("LOGIN_API_URL") . "/api/mailserver/change"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];

         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);

         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', json_encode($data['message']));
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
          // When there is any invalid input            
          return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
          return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function mailServerChangeForm(Request $request) {     
      return view('mailconfig/mailserver_change',[
         'mailservers' => $this->mailServerFetchAll()
      ]);
   }

   private function mailServerFetchAll() {
      $url = env("LOGIN_API_URL") . "/api/mailconfig/fetch/all"; 
          
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }
}