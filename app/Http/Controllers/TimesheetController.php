<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;
use DateTime;
use App\Lib\Utils\Timezone;

class TimesheetController extends Controller {    
    public function timesheetform() {
        $tz = new Timezone;
        
        $url = env("LOGIN_API_URL") . "/api/timesheet/currentweek"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = [
            'user_id' => Session::get('user_details')['id'],
            'timezone_utc_offset' => $tz->timezoneUTCoffset(Session::get('company_details')['timezone']),
        ];
        

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);
        
        return view('timesheet/timesheet', [
            'timesheet' => $data,
            'timezone' => Session::get('company_details')['timezone'],
        ]);
    }

    public function timesheetApproveForm() {
        $url = env("LOGIN_API_URL") . "/api/team/members/ofuser"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = [
            'user_id' => Session::get('user_details')['id'],
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);

        return view('timesheet/timesheet_approve', [
            'users' => $data 
        ]);
    }

    public function timesheetAllUsers() {
        $tz = new Timezone;
     
        $url = env("LOGIN_API_URL") . "/api/timesheet/users/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),      
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id'],
            'timezone_utc_offset' => $tz->timezoneUTCoffset(Session::get('company_details')['timezone']),
        ];
        $data = $HttpReq->post($url , $header, $postdata);

        $timesheet = [];
        $row = [];
        $days = [];
        $fname = "";
        $lname = "";

        // Create 2D array where each user represents one row
        foreach ($data as $key=>$d) {
            if ($key == 0) { // first iteration of loop
                $fname = $d['fname'];
                $lname = $d['lname'];
            }
            $start = 1;

            if ($key == 0 || ($d['fname'] == $fname && $d['lname'] == $lname)) { // if names are the same that means it is the same user
                $this->createUserTimesheetRecordsForEachDayofWeek($d, $days, $row, $fname, $lname, $start);
            } else if ($d['fname'] != $fname) {       
                $this->adjustUserTimesheetIfUserWasAbsentOnLastDayOfWeek($d, $days, $row, $fname, $lname);
                array_push($timesheet, $row); // push last row into array for previous user
               
                // different user so create new row to store this new user
                // reset arrays back to empty;
                $row = []; 
                $days = [];

                $fname = $d['fname'];
                $lname = $d['lname'];
                $start = 1;
                
                $this->createUserTimesheetRecordsForEachDayofWeek($d, $days, $row, $fname, $lname, $start);
            } // else if ($d['fname'] != $fname)
        } // foreach

        $this->adjustUserTimesheetIfUserWasAbsentOnLastDayOfWeek($d, $days, $row, $fname, $lname);
        array_push($timesheet, $row); // push last row 

        $timesheets2 = $this->calculateNormalHoursForAllUsers($timesheet);

        $dateHeader = [
            date("Y-m-d", strtotime("last week monday")),
            date("Y-m-d", strtotime("last week tuesday")),
            date("Y-m-d", strtotime("last week wednesday")),
            date("Y-m-d", strtotime("last week thursday")),
            date("Y-m-d", strtotime("last week friday")),
            date("Y-m-d", strtotime("last week saturday")),
            date("Y-m-d", strtotime("last week sunday")),
        ];
       
        return view('timesheet/timesheet_allusers', [
            'timesheets' => $timesheets2,
            'dateHeader' => $dateHeader,
            'timezone' => Session::get('company_details')['timezone'],
        ]);
    }

    public function timesheetUserRecords(Request $request) {
        $postdata = $request->except('_token');

        return json_encode($this->userRecordsHtml($postdata));
    }

    private function createUserTimesheetRecordsForEachDayofWeek(&$timesheetRow = [], &$days = [], &$row = [], &$fname, &$lname, &$start) {
        for ($x = $start; $x <= 7; $x++) { // loop 7 times because 7 days in a week
            // Week starts on Monday and ends on Sunday
            if(date('N', strtotime($timesheetRow['checkin'])) == $x) {
                // if checkin day is equal to the day represented by $x
                if (!in_array($x, $days)) {
                    $row[] = $timesheetRow; // push existng timesheet record into array
                    $start++;
                    array_push($days, $x);
                }
            } else if (date('N', strtotime($timesheetRow['checkin'])) > $x) { // if there were days before checkin date, where user did not checkin
                if (!in_array($x, $days)) {
                    $row[] = [
                        'fname' => $fname,
                        'lname' => $lname,
                        'department_name' => $timesheetRow['department_name'],
                    ]; // if no checkin for that day, then push dummy record that has no timesheet values
                    $start++;
                    array_push($days, $x);
                }
            } else if ($timesheetRow['checkin'] == null) { // No checkin at all for that day
                if (!in_array($x, $days)) {
                    $row[] = [
                        'fname' => $fname,
                        'lname' => $lname,
                        'department_name' => $timesheetRow['department_name'],
                    ]; // if no checkin for that day, then push dummy record that has no timesheet values
                    $start++;
                    array_push($days, $x);
                }
            }
        } // for ($x = $start
    }

    private function adjustUserTimesheetIfUserWasAbsentOnLastDayOfWeek(&$timesheetRow = [], &$days = [], &$row = [], &$fname, &$lname) {
        for ($y = 7; $y >= 1; $y--) { // loop 7 times because 7 days in a week
            // if the last checkin day of the week for user was NOT a Sunday,
            // then starting from Sunday, loop backwards and check which days user did not checkin

            if (in_array($y, $days)) {
                // exit loop early, once this loop reaches a day user has checked-in
                break;
            }

            // insert dummy records for those days user did not checkin
            if (!in_array($y, $days)) {
                $row[] = [
                    'fname' => $fname,
                    'lname' => $lname,
                    'department_name' => $timesheetRow['department_name'],
                ]; // if no checkin for that day, then push dummy record that has no timesheet values
                array_push($days, $y);
            }    
        }
    }

    private function calculateNormalHoursForAllUsers($timesheets = []) {
        $timesheet2 = [];
        $row = [];
        foreach ($timesheets as $key=>$user) {
            $weekNormalHours = 0;
            $maxHours = 0;
            $normalhours = 0;
            foreach ($user as $t) {
                if (array_key_exists('checkin' , $t) && $t['checkin'] != null) {
                    $checkin = new DateTime($t['checkin']);
                    $checkout = new DateTime($t['checkout']);
                    $workStartTime = new DateTime($t['work_starttime']);
                    $workEndTime = new DateTime($t['work_endtime']);

                    $normalhours = $checkin->diff($checkout);
                    $hours =  $normalhours->format('%h');
                    $minutes = $normalhours->format('%i')/60;
                    $seconds = $normalhours->format('%s')/3600;
                    $hoursPerDay = $hours + $minutes + $seconds;

                    $maxHours = $workStartTime->diff($workEndTime);
                    $hours =  $maxHours->format('%h');
                    $minutes = $maxHours->format('%i')/60;
                    $seconds = $maxHours->format('%s')/3600;
                    $maxHoursPerDay = $hours + $minutes + $seconds;

                    if ($hoursPerDay > $maxHoursPerDay) {
                        $hoursPerDay = $maxHoursPerDay;
                    }

                    $weekNormalHours+= $hoursPerDay;
                }
            }
            $row['timesheet'] = $user;
            $row['weeklynormalhours'] = round($weekNormalHours,2);
      
            array_push($timesheet2, $row);
            $row = [];
        }
        return $timesheet2;
    }

    private function userRecordsHtml($postdata = array()) {
        $tz = new Timezone;
        $url = env("LOGIN_API_URL") . "/api/timesheet/lastweek"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),      
        ];
        $postdata['timezone_utc_offset'] = $tz->timezoneUTCoffset(Session::get('company_details')['timezone']);

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $header, $postdata);

        $ids = json_encode(array_column($data, 'id'));

        $html =  view('timesheet/partial/timesheet_userrecords', [
            'timesheet' => $data, 
            'timesheetIds' => $ids,
            'timezone' => Session::get('company_details')['timezone'],
        ])->render();

        return $html;
    }

    public function timesheetEdit(Request $request) {
        try {
            $this->validate($request, [
                'id' => 'required',
                'checkin' => 'required',
                'checkout' => 'required',
            ]);

            $url = env("LOGIN_API_URL") . "/api/timesheet/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
            
            $postdata = $request->except('_token');
            
            date_default_timezone_set(env('APP_TIMEZONE'));
            $timezone = Session::get('company_details')['timezone'];
            
            $checkin = strtotime($postdata['checkin']  ." ". $timezone);
            $postdata['checkin'] = date("Y-m-d H:i:s", $checkin);

            $checkout = strtotime($postdata['checkout']  ." ". $timezone);
            $postdata['checkout'] = date("Y-m-d H:i:s", $checkout);

            if ($checkin > time() ) {
                // if checkin time is in the future
                throw new \Exception("Checkin time cannot be a time in the future"); 
            }

            if ($checkout > time() ) {
                // if checkout time is in the future
                throw new \Exception("Checkout time cannot be a time in the future"); 
            }

            if ($checkin > $checkout ) {
                // if checkin time is a time after checkout time
                throw new \Exception("Checkin time cannot be a time after checkout"); 
            } 

            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return json_encode([
                    'success' => true,
                    'message' => $this->userRecordsHtml($postdata),
                ]);
            }
    
            return json_encode($data);
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]); 
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]); 
        }
    }

    public function approveAll(Request $request) {
        $url = env("LOGIN_API_URL") . "/api/timesheet/approve/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = $request->except('_token');
       
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url, $header, $postdata);

        if ($data['success'] == true) {
            return json_encode([
                'success' => true,
                'message' => $this->userRecordsHtml($postdata),
            ]);
        }

        return json_encode($data);
    }
}