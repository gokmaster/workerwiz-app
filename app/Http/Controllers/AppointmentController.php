<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Illuminate\Filesystem\Filesystem;
use Session;
use PDF;
use Mail;
use App\MailConfig;

class AppointmentController extends Controller { 
   public function appointmentCreateForm(){
      return view('appointment/appointment_create', [
         'reps' => $this->repFetch(),
      ]);
   }

   public function appointmentEditForm($apptId){
      return view('appointment/appointment_edit', [
         'reps' => $this->repFetch(),
         'appt' => $this->appointmentFetch($apptId),
      ]);
   }

   public function blockTimeSlotsForm() {
      return view('appointment/appointment_block_timeslot', [
         'reps' => $this->repFetch(),
      ]);
   }

   public function appointmentView($apptId){
      $appt = $this->appointmentFetch($apptId);

      return view('appointment/appointment_view', [
         'reps' => $this->repFetch(),
         'appt' => $appt,
         'userIsApprover' => $this->userIsApprover(),
         'reps_to_email' => $this->fetchRepByIdAndProjectManager($appt['rep_id']),
      ]);
   }

   public function appointmentConfirmForm(){
      return view('appointment/appointment_confirm', [
         'appointments' => $this->appointmentsForConfirmersFetch(),
      ]);
   }

   public function appointmentCreate(Request $request){
      try {
         $this->validate($request, [
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'fname' => 'required',
            'lname' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['appointment_date'] = date("Y-m-d", strtotime($postdata['appointment_date']) ); // format date to yyyy-mm-dd
         $postdata['appointment_time'] = date("H:i:s", strtotime($postdata['appointment_time']) );
         $postdata['user_id'] = Session::get('user_details')['id'];
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                 
         $url = env("LOGIN_API_URL") . "/api/appointment/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         //When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function appointmentEdit(Request $request){
      try {
         $this->validate($request, [
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'fname' => 'required',
            'lname' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['appointment_date'] = date("Y-m-d", strtotime($postdata['appointment_date']) ); // format date to yyyy-mm-dd
         $postdata['appointment_time'] = date("H:i:s", strtotime($postdata['appointment_time']) );
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                         
         $url = env("LOGIN_API_URL") . "/api/appointment/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function appointmentEditJson(Request $request){
      try {
         $this->validate($request, [
            'id' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                          
         $url = env("LOGIN_API_URL") . "/api/appointment/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return json_encode([
                'success' => true,
                'message' => $data['message'],
            ]);
         }
         return json_encode($data);
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => $e->errors(),
         ]);  
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);  
      }
   }

   public function blockTimeslot(Request $request){
      try {
         $this->validate($request, [
            'appointment_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'rep_id' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['appointment_date'] = date("Y-m-d", strtotime($postdata['appointment_date']) ); // format date to yyyy-mm-dd
         $postdata['start_time'] = date("H:i", strtotime($postdata['start_time']) );
         $postdata['end_time'] = date("H:i", strtotime($postdata['end_time']) );
         $postdata['status'] = 'blocked';
         $postdata['company_id'] = Session::get('user_details')['company_id'];

         $startTimeInt = (int) date('H', strtotime($postdata['start_time']));
         $endTimeInt = (int) date('H', strtotime($postdata['end_time']));

         if ($endTimeInt <= $startTimeInt) {
            throw new \Exception("End-time should be a time after start-time"); 
         }
                 
         $url = env("LOGIN_API_URL") . "/api/appointment/blocked/timeslots/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         //When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function blockedTimeslotRemove(Request $request){
      try {
         $this->validate($request, [
            'id' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                          
         $url = env("LOGIN_API_URL") . "/api/appointment/blocked/timeslot/remove"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return json_encode([
                'success' => true,
                'message' => $data['message'],
            ]);
         }
         return json_encode($data);
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => $e->errors(),
         ]);
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);  
      }
   }

   public function appointmentConfirm(Request $request){
      try {
         $this->validate($request, [
            'id' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['status'] = "confirmed";
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                          
         $url = env("LOGIN_API_URL") . "/api/appointment/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return json_encode([
                'success' => true,
                'message' => "Successfully confirmed appointment",
                'html' => view('appointment/partial/appointment_confirm_table', ['appointments' => $this->appointmentsForConfirmersFetch()])->render(),
            ]);
         }
         return json_encode($data);
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => $e->errors(),
         ]);  
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);  
      }
   }

   public function appointmentDrop(Request $request){
      try {
         $this->validate($request, [
            'id' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['status'] = "dropped";
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                          
         $url = env("LOGIN_API_URL") . "/api/appointment/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return json_encode([
                'success' => true,
                'message' => "Successfully dropped appointment",
                'html' => view('appointment/partial/appointment_confirm_table', ['appointments' => $this->appointmentsForConfirmersFetch()])->render(),
            ]);
         }
         return json_encode($data);
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => $e->errors(),
         ]);  
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);  
      }
   }

   public function appointmentReschedule(Request $request){
      try {
         $this->validate($request, [
            'id' => 'required',
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'status' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['appointment_date'] = date("Y-m-d", strtotime($postdata['appointment_date']) ); // format date to yyyy-mm-dd
         $postdata['appointment_time'] = date("H:i", strtotime($postdata['appointment_time']) );
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                                  
         $url = env("LOGIN_API_URL") . "/api/appointment/reschedule"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return json_encode([
                'success' => true,
                'message' => $data['message'],
            ]);
         }
         return json_encode($data);
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => $e->errors(),
         ]);  
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);  
      }
   }

   public function appointmentsLiveDiary() {
      $appointments = $this->appointmentsByRepFetch();
      $calendar = $this->weeksAndMonthsArray();
      //dd($this->createAppointmentsByRepArray($appointments, $calendar['weeks']));  
      return view('appointment/appointment_live_diary', [
         'currentMonday' => date( 'Y-m-d', strtotime('monday this week') ),
         'currentMonth' => date('M-y'),
         'months' => $calendar['months'],
         'weeks' => $this->createAppointmentsByRepArray($appointments, $calendar['weeks']),
         'confirmers' => $this->confirmerAll(),
         'userIsApprover' => $this->userIsApprover(),
      ]);
   }

   public function appointmentsRepTableHtml() {
      $appointments = $this->appointmentsByRepFetch();
      $calendar = $this->weeksAndMonthsArray();
          
      return view('appointment/partial/appointment_rep_table', [
         'currentMonday' => date( 'Y-m-d', strtotime( 'monday this week' ) ),
         'weeks' => $this->createAppointmentsByRepArray($appointments, $calendar['weeks']),
         'userIsApprover' => $this->userIsApprover(),
      ])->render();
   }

   public function appointmentsIcedFetch() {
      $url = env("LOGIN_API_URL") . "/api/appointment/fetch/iced"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];
      
      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      $data = $HttpReq->post($url , $header, $postdata);

      return view('appointment/appointment_iced', [
         'appointments' => $data,
      ]);
   }

   public function appointmentsDroppedFetch() {
      $url = env("LOGIN_API_URL") . "/api/appointment/fetch/dropped"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      $data = $HttpReq->post($url , $header, $postdata);

      return view('appointment/appointment_dropped', [
         'appointments' => $data,
      ]);
   }

   public function appointmentsArchivedFetch() {
      $url = env("LOGIN_API_URL") . "/api/appointment/fetch/archived"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      $data = $HttpReq->post($url , $header, $postdata);

      return view('appointment/appointment_archived', [
         'appointments' => $data,
      ]);
   }

   public function sendLeadPdf(Request $request) {
      try {
         $this->validate($request, [
            'id' => 'required',
            'emails' => 'required',
         ]);
            
         $postdata = $request->except('_token');
         $appointment = $this->appointmentFetch($postdata['id']);
         $rep = $this->repfetchProjectManager();

         $appointment['projectmanager'] = $rep['fname'] . " " . $rep['lname'];

         $emails = json_decode($postdata['emails']);

         $filename = time(). mt_rand() . ".pdf";
         $pdf = PDF::loadView('appointment/partial/lead_pdf_template', $appointment);
         
         $filepath = storage_path('appointment/leadpdf/'. $filename);
         $pdf->save($filepath);

         foreach ($emails as $email) {
            $this->sendLeadPdfEmail($email, $filepath, $appointment);
         }
         
         return json_encode([
            'success' => true,
            'message' => "Successfully sent lead-pdf",
        ]);
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => $e->errors(),
         ]);  
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);  
      }
   }

   public function confirmersAddForm() {
      $url = env("LOGIN_API_URL") . "/api/confirmers/with/users"; 
            
      $header = [
          'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],        
      ];

      $HttpReq = new HttpRequest;
      $data = $HttpReq->post($url , $header, $postdata);

      $userIds = [];
      $currentConfirmers = [];
      $i = 0;

      foreach ($data as $d) {
          if (in_array($d['user_id'], $userIds)) {
              unset($data[$i]); // remove duplicate user_id from array
          }

          array_push($userIds, $d['user_id']);

          // current team members
          if ($d['confirmer_id'] != null) {
              array_push($currentConfirmers, $d);
          }
          $i++;
      }
     
      return view('appointment/confirmers_add', [
          'users' => $data,
          'currentMembers' => $currentConfirmers,
      ]);
   }

   public function confirmersAdd(Request $request) {
      try {
         $this->validate($request, [
             'selected_members' => 'required',
         ]);

         $url = env("LOGIN_API_URL") . "/api/confirmers/add"; 
            
         $header = [
             'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $selectedMembers = $request->input('selected_members');
        
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, [
            'selected_members' => $selectedMembers,
            'company_id' => Session::get('user_details')['company_id']
         ]); 
         
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }


   private function appointmentsByRepFetch() {
      $url = env("LOGIN_API_URL") . "/api/appointment/fetch/byrep"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],        
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }

   private function fetchRepByIdAndProjectManager($repId) {
      $url = env("LOGIN_API_URL") . "/api/rep/fetch/byid/projectmanager"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'id' => $repId,
         'company_id' => Session::get('user_details')['company_id'],    
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }

   private function appointmentsForConfirmersFetch() {
      $url = env("LOGIN_API_URL") . "/api/appointment/fetch/forconfirmer"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'user_id' => Session::get('user_details')['id'],
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }

   private function weeksAndMonthsArray() {
      $startdate =  strtotime(date('d-m-Y', strtotime("-26 week"))); // go 26 weeks back
      $dayOfWeek = date('w', $startdate); // integer indicating day of week; 0=Sunday, 6=Saturday 
      $subtractDays = $dayOfWeek-1;

      $monday = strtotime(date("Y-m-d",$startdate)." -" .$subtractDays. " days");
      $startdate = date("Y-m-d", $monday);

      $tue = strtotime(date("Y-m-d",$monday)." +1 days");
      $tue2 = date("Y-m-d", $tue);

      $wed = strtotime(date("Y-m-d",$monday)." +2 days");
      $wed2 = date("Y-m-d", $wed); 

      $thur = strtotime(date("Y-m-d",$monday)." +3 days");
      $thur2 = date("Y-m-d", $thur); 

      $fri = strtotime(date("Y-m-d",$monday)." +4 days");
      $fri2 = date("Y-m-d", $fri); 

      $sat = strtotime(date("Y-m-d",$monday)." +5 days");
      $sat2 = date("Y-m-d", $sat); 

      $enddate = strtotime(date("Y-m-d", $monday)." +6 days");
      $enddate2 = date("Y-m-d", $enddate);

      $weeks = [
         0 => [
            'monMonth' => date('M-y', $monday),
            'sunMonth' => date('M-y', $enddate),
            'monday' => $startdate,
            'tuesday' => $tue2,
            'wednesday' => $wed2,
            'thursday' => $thur2,
            'friday' => $fri2,
            'saturday' => $sat2,
            'sunday' =>  $enddate2,
         ]
      ];
      
      for ($i=1; $i<52; $i++) {
         $row = $this->calculateStartAndEndDateOfWeek($startdate);
         $weeks[] = $row;
         $startdate = $row['monday'];
      }

      $months = [
         0 => date('M-y', strtotime($weeks[0]['monday'])),
      ];
      
      for ($a=1; $a<13; $a++) {
         $months[] =  date('M-y', strtotime($weeks[0]['monday']."+$a month"));
      }

      return [
         'weeks' => $weeks,
         'months' => $months,
      ];
   }

   private function createAppointmentsByRepArray($appointments, $weeks) {
      foreach ($weeks as $key=>$week) {
         $weekstart = strtotime($week['monday']);
         $weekend = strtotime($week['sunday']);
         $repId;

         foreach ($appointments as $i=>$appointment) {            
            $appointmentDate = strtotime($appointment['appointment_date']);

            if ($appointmentDate >= $weekstart && $appointmentDate <= $weekend) {
               $repName = $appointment['rep_fname'] . " " . $appointment['rep_lname']."->".$appointment['rep_zipcode'];
               $weeks[$key]['appointments'][$repName][] = $appointment;
               unset($appointments[$i]);
            }
         }
      }
     
      foreach ($weeks as $wknum=>$week) {
         if (array_key_exists("appointments", $week)) {
            foreach ($week['appointments'] as $repkey=>$reps) {
               $days = ['mon'=> [], 'tue'=> [], 'wed'=> [], 'thu'=> [], 'fri'=> [], 'sat'=> [], 'sun'=> [] ];

               foreach ($reps as $key=>$repAppt) {
                  // group aapointments for each rep into days
                  $day = date('w', strtotime($repAppt['appointment_date'])); // integer indicating day of week; 0=Sunday, 6=Saturday 
  
                  if ($day == 1) {
                    $days['mon'][] = $repAppt;
                  } else if ($day == 2) {
                    $days['tue'][] = $repAppt;
                  } else if ($day == 3) {
                    $days['wed'][] = $repAppt;
                  } else if ($day == 4) {
                    $days['thu'][] = $repAppt;
                  } else if ($day == 5) {
                    $days['fri'][] = $repAppt;
                  }  else if ($day == 6) {
                     $days['sat'][] = $repAppt;
                  }  else if ($day == 0) {
                     $days['sun'][] = $repAppt;
                  }
               } // foreach
  
               $times = [9, 10, 11, 12, 13, 14, 15, 16];
               $timeStr = ['9:00:00', '10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00'];
               
               foreach ($days as $i=>$day) {
                  $bookedTimes = array_column($day, 'appointment_time_dec');
            
                  foreach ($times as $t=>$time) {
                     if (!in_array($time, $bookedTimes)) {
                        $appt = [
                        'appointment_time' =>  $timeStr[$t],
                        'appointment_time_dec' => $this->timeStringToFloat($timeStr[$t])
                        ];
                        $days[$i][] = $appt;
                     } 
                  } // foreach
  
                  array_multisort( array_column($days[$i], "appointment_time_dec"), SORT_ASC, $days[$i]);
               } // foreach

               $weeks[$wknum]['appointments'][$repkey] = $days;
            }
         }
      }

      return $weeks;
   }

   private function confirmerAll() {
      $url = env("LOGIN_API_URL") . "/api/confirmer/all"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }

   private function timeStringToFloat($timeString) {
      $timeArr = explode(":", $timeString);
      $floatnum = (float) $timeArr[0]+ (float)($timeArr[1]/60);
      return $floatnum;
   }

   private function calculateStartAndEndDateOfWeek($startdate) {
      $startdate = strtotime($startdate);
      $startdate = strtotime(date("Y-m-d",$startdate)." +7 days");
      
      $tue = strtotime(date("Y-m-d",$startdate)." +1 day");
      $wed = strtotime(date("Y-m-d",$startdate)." +2 days");
      $thur = strtotime(date("Y-m-d",$startdate)." +3 days");
      $fri = strtotime(date("Y-m-d",$startdate)." +4 days");
      $sat = strtotime(date("Y-m-d",$startdate)." +5 days");
      $enddate = strtotime(date("Y-m-d",$startdate)." +6 days");

      return [
         'monMonth' => date('M-y', $startdate),
         'sunMonth' => date('M-y', $enddate),
         'monday' => date("Y-m-d", $startdate),
         'tuesday' => date("Y-m-d", $tue),
         'wednesday' => date("Y-m-d", $wed),
         'thursday' => date("Y-m-d", $thur),
         'friday' => date("Y-m-d", $fri),
         'saturday' => date("Y-m-d", $sat),
         'sunday' => date("Y-m-d", $enddate),
      ];
   }

   private function repFetch() {
      $url = env("LOGIN_API_URL") . "/api/rep/all"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];
      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }

   private function repfetchProjectManager() {
      $url = env("LOGIN_API_URL") . "/api/rep/fetch/projectmanager"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];
      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , $header, $postdata);
   }

   private function appointmentFetch($appointmentId) {
      $url = env("LOGIN_API_URL") . "/api/appointment/fetch"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'id' => $appointmentId,
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, $postdata);
   }

   private function userIsApprover() {
      // check if the logged-in user is an approver
      // returns 1, if user is an approver, else 0
      $userId = Session::get('user_details')['id'];

      $url = env("LOGIN_API_URL") . "/api/confirmer/isapprover"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $HttpReq = new HttpRequest;
      return $HttpReq->post($url , $header, ['user_id' => $userId]);
   }

   private function sendLeadPdfEmail($mailTo, $attachmentFilepath, $data = [])
   {   
      $mailConfModel = new MailConfig;
      $mailConfig = $mailConfModel->fetch("default");
      $mailFrom = $mailConfig['from_address'];

      $subject = $data['rep_fname'] . ": " . $data['business_name'] . " " . date("j-M-Y", strtotime($data['appointment_date'])) . " Appointment @ " . date("g:i A", strtotime($data['appointment_time']));  
      Mail::send('mail/appointment_lead', $data, function($message) use($mailTo, $mailFrom, $attachmentFilepath, $subject) {
         $message->to($mailTo, 'Appointment')->subject($subject);
         $message->attach($attachmentFilepath);
         $message->from($mailFrom,'noreply');
      }); 
   }
}