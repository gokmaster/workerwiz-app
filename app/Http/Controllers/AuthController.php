<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;

class AuthController extends Controller {
    public function __construct() {
        //$this->middleware(Authenticate::class); // is user logged-in?
    }
    
    // Receive Access-token from Login App
    public function receiveAccessToken(Request $request) {
        $token = $request->input('access_token');
        $redirectUrl = $request->input('redirect_url');

        Session::put('access_token', $token); 
        return redirect($redirectUrl);
    }
}