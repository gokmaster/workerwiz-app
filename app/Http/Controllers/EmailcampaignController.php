<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
use Mail;
use App\EmailQue;
use App\MailConfig;

class EmailcampaignController extends Controller {
   public function createForm() {     
      return view('email_campaign/emailcampaign_create');
   }

   public function create(Request $request) {     
      try {
         $this->validate($request, [
            'campaign_name' => 'required',
            'content_type' => 'required',
            'email_subject' => 'required',
         ]);
          
         $postdata = $request->except('_token');
         $postdata['company_id'] = Session::get('user_details')['company_id'];

         if (isset($postdata['content'])) {
            $postdata['content'] = base64_encode($postdata['content']);
         }
                                    
         $url = env("LOGIN_API_URL") . "/api/emailcampaign/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];

         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);

         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', json_encode($data['message']));
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
          // When there is any invalid input            
          return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
          return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function sendCampaignForm() {
      return view('email_campaign/emailcampaign_send',[
         'campaigns' => $this->fetchAllCampaigns(),
         'mailinglistgroups' => $this->fetchAllMailingListGroups(),
         'timezone' => Session::get('company_details')['timezone'],
      ]);
   }

   public function sendCampaign(Request $request) {     
      try {
         $this->validate($request, [
            'mailinggroup_id' => 'required',
            'email_campaign_id' => 'required',
         ]);
          
         $postdata = $request->except('_token');
         $emailCampaign = $this->fetchEmailCampaign($postdata['email_campaign_id']);
         $mailingLists = $this->fetchMailingListOfMailingGroup($postdata['mailinggroup_id']);

         if (count($emailCampaign)) {
            if (count($mailingLists)) {
               $EmailQueData = [];
               $row = [];
               $response = "";
             
               $model = new EmailQue;

               ini_set('max_execution_time', 300); //300 seconds = 5 minutes
               set_time_limit(300);

               $mailConfModel = new MailConfig;
               $mailConfig = $mailConfModel->fetchInUse();

               foreach ($mailingLists as $count=>$ml) {
                  if ($count < 2) { // send email
                     $this->sendEmailCampaign($ml['email'], $emailCampaign);
                  } else { // add to que if sending more than 20 emails
                     $row['subject'] = $emailCampaign['email_subject'];
                     $row['mail_to'] = $ml['email'];
                     $row['mail_from'] = $mailConfig['from_address'];
                     $row['email_campaign_id'] = $postdata['email_campaign_id'];
                     $row['company_id'] = Session::get('user_details')['company_id'];
                     
                     array_push($EmailQueData, $row);

                     // insert email-ques into DB in batches of 50
                     if (count($EmailQueData) > 50) {
                        $response = $model->addEmailToQue($EmailQueData);
                        $EmailQueData = []; // reset array
                     } else if (count($mailingLists)==$count+1 && count($EmailQueData) > 0) { // if looped till last element of array and $EmailQueData > 0
                        $response = $model->addEmailToQue($EmailQueData); // last batch that has less than 50 records
                     }
                  } // if ($count < 50)
               }
              
               if($response != "") {
                  return json_encode($response);
               }
            } // if (count($mailingLists))

            return json_encode([
               'success' => true,
               'message' => 'Successfully sent email campaign',
            ]);
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
          // When there is any invalid input  
         return json_encode([
            'success' => false,
            'message' => json_encode($e->errors()),
         ]);          
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   private function sendEmailCampaign($mailTo, $data){
      $data['content'] = base64_decode($data['content']);
      $subject = $data['email_subject'];

      $mailConfModel = new MailConfig;
      $mailConfig = $mailConfModel->fetchInUse();
      $mailFrom = $mailConfig['from_address'];

      Mail::send('mail/email_campaign', $data, function($message) use($mailTo, $mailFrom, $subject) {
         $message->to($mailTo, '')->subject($subject);
        
         $message->from($mailFrom,'noreply');
      }); 
   }

   private function fetchEmailCampaign($emailCampaignId) {
      $url = env("LOGIN_API_URL") . "/api/emailcampaign/fetch"; 
         
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'email_campaign_id' => $emailCampaignId,
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , $header, $postdata);
   }

   private function fetchMailingListOfMailingGroup($mailingGrpId) {
      $url = env("LOGIN_API_URL") . "/api/mailinglist/ofmailinggroup/fetch/all"; 
         
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'mailinggroup_id' => $mailingGrpId,
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , $header, $postdata);
   }

   private function fetchAllCampaigns() {
      $url = env("LOGIN_API_URL") . "/api/emailcampaign/ofcompany/fetch/all"; 
         
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , $header, $postdata);
   }

   private function fetchAllMailingListGroups() {
      $url = env("LOGIN_API_URL") . "/api/mailinglistgroup/ofcompany/fetch/all"; 
         
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $postdata = [
         'company_id' => Session::get('user_details')['company_id'],
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , $header, $postdata);
   }
}