<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;

class AttachmentController extends Controller {    
    public function profileAttachmentAdd(Request $request) {
        try {
            $this->validate($request, [
                'user_id' => 'required',
                'date_applied' => 'required',
                'attachment_type_codename' => 'required',
                'file' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['date_applied'] = date("Y-m-d", strtotime($postdata['date_applied']) ); // format date to yyyy-mm-dd
            $postdata['company_id'] = Session::get('user_details')['company_id'];
            
            if($request->hasFile('file')) {
                // file upload
                request()->validate([
                    'file' => 'required|mimes:jpeg,png,jpg,pdf,doc,docx,xls,xlsx|max:9048',
                ]);
                $fileName = $postdata['user_id'] .'_'. mt_rand(1,9898899) . time(). '.'.request()->file->getClientOriginalExtension();
                request()->file->move(storage_path('attachment/profile'), $fileName);
                $postdata['file'] = $fileName;
            }
                      
            $url = env("LOGIN_API_URL") . "/api/attachment/profile/add"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect()->back()->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function profileAttachmentEdit(Request $request) {
        try {
            $this->validate($request, [
                'id' => 'required',
                'date_applied' => 'required',
                'attachment_type_codename' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['date_applied'] = date("Y-m-d", strtotime($postdata['date_applied']) ); // format date to yyyy-mm-dd
            $postdata['company_id'] = Session::get('user_details')['company_id'];
            
            if($request->hasFile('file')) {
                // file upload
                request()->validate([
                    'file' => 'required|mimes:jpeg,png,jpg,pdf,doc,docx,xls,xlsx|max:9048',
                ]);
                $fileName = $postdata['user_id'] .'_'. mt_rand(1,9898899) . time(). '.'.request()->file->getClientOriginalExtension();
                request()->file->move(storage_path('attachment/profile'), $fileName);
                $postdata['file'] = $fileName;
            }
            
            $url = env("LOGIN_API_URL") . "/api/attachment/profile/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect()->back()->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function profileAttachmentDelete(Request $request) {
        try {
            $this->validate($request, [
                'id' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            
            $url = env("LOGIN_API_URL") . "/api/attachment/profile/delete"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect()->back()->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function profileAttachmentDownload($file) {     
        return response()->download(
            storage_path('attachment/profile/'.hex2bin($file)) // filepath has been hex encoded so that it is compatible in url so we now need to convert it back to string
        );
    }
}