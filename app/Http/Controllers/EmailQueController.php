<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
use Mail;
use App\EmailQue;

class EmailQueController extends Controller {
   public function queuedEmailsSend(Request $request) {
      $secretkey = $request->input('secretkey');

      if ($secretkey != env("API_CALL_KEY")) {
         return "Permission denied";
      }

      $quedEmails = $this->fetchUnsentBatch($secretkey);

      foreach ($quedEmails as $quedEmail) {
         $this->sendEmail($quedEmail);
         $this->markAsSent($secretkey, $quedEmail['id']);
      }
      return "Successfully sent emails";
   }

   private function sendEmail($data) {
      $data['content'] = base64_decode($data['content']);
      $subject = $data['subject'];
      $mailTo = $data['mail_to'];
      $mailFrom = $data['mail_from'];

      Mail::send('mail/email_campaign', $data, function($message) use($mailTo,$mailFrom,$subject) {
         $message->to($mailTo, '')->subject($subject);
         
         $message->from($mailFrom,'noreply');
      }); 
   }

   private function fetchUnsentBatch($secretkey) {
      $url = env("LOGIN_API_URL") . "/api/emailque/unsent/fetch"; 
         
      $postdata = [
         'secretkey' => $secretkey,
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , [], $postdata);
   }

   private function markAsSent($secretkey, $emailQueId) {
      $url = env("LOGIN_API_URL") . "/api/emailque/marksent"; 
         
      $postdata = [
         'secretkey' => $secretkey,
         'emailque_id' => $emailQueId,
      ];

      $HttpReq = new HttpRequest;
      return $data = $HttpReq->post($url , [], $postdata);
   }
}