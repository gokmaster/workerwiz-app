<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller {
    public function permissionDenied() {
        return view('error/permission_denied');  
    }
}