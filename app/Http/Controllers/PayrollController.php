<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Illuminate\Filesystem\Filesystem;
use Session;
use Excel;
use PDF;
use Mail;
use App\MailConfig;

class PayrollController extends Controller { 
    public function payslipGenerateForm(){
        return view('payroll/payslip_generate', [
            'uploads' => $this->excelsUploadedFetch(), 
            ]
        );
    }

    public function excelMasterSheetUpload(Request $request) {
        try {
            if($request->hasFile('file')) {
                request()->validate([
                    'file' => 'required|mimes:xls,xlsx|max:9048',
                ]);
                
                $postdata =  $request->except('_token');
                $startdate =   date("Y-m-d", strtotime($postdata['payperiod_start']) ); // format date to yyyy-mm-dd
                $enddate =   date("Y-m-d", strtotime($postdata['payperiod_end']) ); // format date to yyyy-mm-dd

                $path = $request->file->getRealPath();
                $exceldata = Excel::load($path, function($reader) {})->get();

                // if excel file has data
                if(!empty($exceldata) && $exceldata->count()) {
                    $companyId = Session::get('company_details')['id'];
                    $filename = $companyId.'_'.time(). mt_rand() . '.'.request()->file->getClientOriginalExtension();
                    request()->file->move(storage_path('payroll/excelmaster'), $filename);
                    
                    $data = [
                        'filename' => $filename,
                        'payperiod_start' => $startdate,
                        'payperiod_end' => $enddate,
                        'company_id' => $companyId,
                    ];
                    
                    $uploadResponse = $this->excelUploadAdd($data);
                    $exceluploadId = $uploadResponse['payroll_excelupload_id'];
                    $insert = $this->excelMasterSheetDataToDB($exceldata, $startdate, $enddate, $exceluploadId);
                    
                    if ($insert['success'] == false) {
                        return json_encode([
                            'success' => false,
                            'message' => $insert['message']
                        ]);
                    }

                    $excelsUploadedHtml =  view('payroll/partial/excelsuploaded_table', [
                        'uploads' => $this->excelsUploadedFetch(), 
                    ])->render();

                    return json_encode([
                        'success' => true,
                        'message' => "Upload successful",
                        'html' => $excelsUploadedHtml,
                    ]);
                } else {
                    return json_encode([
                        'success' => false,
                        'message' => "The excel file has no data"
                    ]);
                }  
            } // if($request->hasFile('file')
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }  
    }

    public function generatePayslips(Request $request) {
        try {
            $postdata = $request->except('_token');
            $companyId = Session::get('company_details')['id'];
            $postdata['company_id'] = $companyId;

            $url = env("LOGIN_API_URL") . "/api/payroll/records/fetch"; 
                
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $employees = $HttpReq->post($url , $header, $postdata);

            $file = new Filesystem();
            $directory = '/payroll/payslip/'.$companyId.'/'.date("Y-m-d");

            if (!$file->isDirectory(storage_path($directory)) ) { // create directory if not exist
                $file->makeDirectory(storage_path($directory), 755, true, true); 
            }

            $payslipData = [];
                       
            foreach($employees as $emp) {
                $emp['company_name'] = Session::get('company_details')['company_name'];
                $filename = str_replace(' ', '_', $emp['emp_name']). "_" . time(). mt_rand() . ".pdf";
                $pdf = PDF::loadView('payroll/payslip_template', $emp);
                
                $filepath = storage_path($directory .'/'. $filename);
                $pdf->save($filepath);
                $payslipData[] = [
                    'user_id' => $emp['user_id'],
                    'filepath' => $directory .'/'. $filename,
                    'payperiod_start' => $emp['payperiod_start'],
                    'payperiod_end' => $emp['payperiod_end'],
                ];
                //$this->sendPayslipEmail($emp['email'], $filepath);
            }

            $payslipResponse = $this->payslipAddToDB($payslipData);

            if ($payslipResponse['success'] == true) {
                $this->markPayslipsGeneratedForUploadedExcel($postdata['payroll_excel_upload_id']);
            }
            
            return json_encode([
                'success' => true,
                'message' => "Successfully generated payslips",
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }  
    }

    public function payslipOfUser() {        
        $userId = Session::get('user_details')['id'];

        return view('payroll/payslip_ofuser', [
            'payslips' => $this->fetchUserPayslips($userId),
        ]);
    }

    public function payslipAllUsers() {
        return view('payroll/payslip_users', [
            'users' => $this->usersIdsAll(),
        ]);
    }

    public function payslipOfUserTableHtml(Request $request) {
        $userId = $request->input('user_id');
        return view('payroll/partial/user_payslip_table', [
            'payslips' => $this->fetchUserPayslips($userId),
        ])->render();
    }

    public function uploadedExcelDownload($filename) {     
        return response()->download(
            storage_path('payroll/excelmaster/'. hex2bin($filename)) // filename has been hex encoded so that it is compatible in url so we now need to convert it back to string
        );
    }

    public function payslipDownload($filepath) {     
        return response()->download(
            storage_path(hex2bin($filepath)) // filepath has been hex encoded so that it is compatible in url so we now need to convert it back to string
        );
    }

    public function excelTemplateForPayslipGeneratorDownload() {
        $data = [];
        $users = $this->usersIdsAll();

        $header = [
            'emp_id',
            'emp_name',
            'total_hours_worked',
            'hourly_rate',
            'gross_wages',
            'superannuation_deduction',
            'tax_deduction',
            'total_payable'
        ];

        array_push($data, $header);

        foreach ($users as $user) {
            $row['emp_id'] = $user['emp_id'];
            $row['emp_name'] = ucwords(strtolower($user['fname'])) . " " . ucwords(strtolower($user['lname']));
            $row['total_hours_worked'] = "";
            $row['hourly_rate'] = "";
            $row['gross_wages'] = "";
            $row['superannuation_deduction'] = "";
            $row['tax_deduction'] = "";
            $row['total_payable'] = "";
            array_push($data, $row);
        }
      
        $filename = "payroll-excel-template_".date("Y-m-d_his");
        $filenameWithExt = $filename.".xlsx";
                        
        Excel::create($filename, function($excel) use ($data) {
            $excel->setTitle("Payroll records for upload to Payslip Generator");
                
            $sheetname = "Payroll records";
        
            $excel->sheet($sheetname , function($sheet) use ($data) {
                $from = "A1"; // or any value
                $to = "H1"; // or any value
                $sheet->getStyle("$from:$to")->getFont()->setBold(true); 
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    private function fetchUserPayslips($userId) {
        $url = env("LOGIN_API_URL") . "/api/payslip/ofuser"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata['user_id'] = $userId;

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $postdata);
    }

    private function excelsUploadedFetch() {
        $url = env("LOGIN_API_URL") . "/api/payroll/excelsuploaded/fetch"; 
                
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $postdata);
    }
    
    // Record uploaded excel mastersheet in DB
    private function excelUploadAdd($data = []) {
        $url = env("LOGIN_API_URL") . "/api/payroll/excelupload/create"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $data);
    }

    // Insert excel mastersheet data to database
    private function excelMasterSheetDataToDB($exceldata = [], $payperiodStart, $payperiodEnd, $exceluploadId) {
        try {
            $url = env("LOGIN_API_URL") . "/api/payroll/records/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $employees = [];
            foreach ($exceldata as $key => $value) {
                if ($value->emp_id != null && $value->emp_id != "") {
                    $employees[$key] = [
                        'emp_id' => $value->emp_id,
                        'emp_name' => $value->emp_name,
                        'total_hours_worked' => $value->total_hours_worked,
                        'hourly_rate' => $value->hourly_rate,
                        'gross_wages' => $value->gross_wages,
                        'superannuation_deduction' => $value->superannuation_deduction,
                        'tax_deduction' => $value->tax_deduction,
                        'total_payable' => $value->total_payable,
                        'payperiod_start' => $payperiodStart,
                        'payperiod_end' => $payperiodEnd,
                        'payroll_excel_upload_id' => $exceluploadId,
                        'company_id' => Session::get('user_details')['company_id'],
                    ];

                    if (isset($value->adjustments)) {
                        $employees[$key]['customfields']['payroll_adjustments'] = $value->adjustments;
                    }
                    if (isset($value->confirmers_com)) {
                        $employees[$key]['customfields']['payroll_confirmers_com'] = $value->confirmers_com;
                    }
                    if (isset($value->commission)) {
                        $employees[$key]['customfields']['payroll_commission'] = $value->commission;
                    }
                    if (isset($value->n_confirmed_bookings)) {
                        $employees[$key]['customfields']['payroll_n_confirmed_bookings'] = $value->n_confirmed_bookings;
                    }
                    if (isset($value->target)) {
                        $employees[$key]['customfields']['payroll_target'] = $value->target;
                    }
                    if (isset($value->sales_kw)) {
                        $employees[$key]['customfields']['payroll_sales_kw'] = $value->sales_kw;
                    }
                    if (isset($value->top_agent)) {
                        $employees[$key]['customfields']['payroll_top_agent'] = $value->top_agent;
                    }
                    if (isset($value->meal_allowance)) {
                        $employees[$key]['customfields']['payroll_meal_allowance'] = $value->meal_allowance;
                    }
                    if (isset($value->travel_allowance)) {
                        $employees[$key]['customfields']['payroll_travel_allowance'] = $value->travel_allowance;
                    }
                }
            } // foreach
           
            $postdata = [
                'employees' => json_encode($employees),
                'company_id' => Session::get('user_details')['company_id']
            ];

            $HttpReq = new HttpRequest;
            return $HttpReq->post($url , $header, $postdata);
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    private function sendPayslipEmail($mailTo, $attachmentFilepath)
    {
        $subjects = "Payslip";

        $mailConfModel = new MailConfig;
        $mailConfig = $mailConfModel->fetch("default");
        $mailFrom = $mailConfig['from_address'];
        
        $data = [];
        Mail::send('mail/payslip', $data, function($message) use($mailTo, $mailFrom, $attachmentFilepath) {
            $message->to($mailTo, 'Playslip')->subject('Payslip');
            $message->attach($attachmentFilepath);
            $message->from($mailFrom,'noreply');
        }); 
    }

    private function payslipAddToDB($data = []) {
        $url = env("LOGIN_API_URL") . "/api/payslip/create"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $data);
    }

    private function markPayslipsGeneratedForUploadedExcel($uploadedExcelId) {
        // mark as 'Payslip Generated' for the uploaded-excel
        $url = env("LOGIN_API_URL") . "/api/payroll/excelupload/edit"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $data = [
            'id' => $uploadedExcelId,
            'payslip_generated' => 1,
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $data);
    }

    private function usersIdsAll() {
        $url = env("LOGIN_API_URL") . "/api/users/ids"; 
                
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }
}