<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use App\Lib\verifyEmail;
use App\MailConfig;
use Session;
use Excel;

class MailinglistController extends Controller {
   public function createForm(Request $request) {     
      return view('mailinglist/mailinglist_create');
   }

   public function create(Request $request) {
      set_time_limit(600); //60 seconds = 1 minute
      try {
         $this->validate($request, [
             'title' => 'required',
             'file' => 'required|mimes:xls,xlsx,csv,txt|max:9048',
         ]);
             
         $postdata = $request->except('_token');
                
            // file upload
         $path = $request->file->getRealPath();
         $exceldata = Excel::load($path, function($reader) {})->get();
         
         // if excel file has no data
         if (empty($exceldata) || $exceldata->count() < 1) {
            throw new \Exception("The excel file should have at least 1 record");                                           
         } 

         if ($exceldata->count() > 351) {
            throw new \Exception("The excel file should NOT contain more than 350 records");                                           
         } 

         $mailinglist = [];

         $mailConfModel = new MailConfig;
         $mailConfig = $mailConfModel->fetchInUse();
         $mailFrom = $mailConfig['from_address'];

         // Verify email address is valid and exist
         $vmail = new verifyEmail();
         $vmail->setStreamTimeoutWait(20);
         $vmail->Debug= TRUE;
         $vmail->Debugoutput= 'html';
   
         $vmail->setEmailFrom($mailFrom);

         foreach ($exceldata as $key=>$value) {      
            if ($vmail->check($value->email)) { // if email is valid and exist
               $mailinglist[] = [
                  'email' => $value->email,
               ];
            }
         } // foreach
         
         $postdata2 = [
            'mailinglist' => json_encode($mailinglist),
            'title' => $postdata['title'],
            'company_id' => Session::get('user_details')['company_id'],
         ];
                  
         $url = env("LOGIN_API_URL") . "/api/mailinglist/create"; 
         
         $header = [
             'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata2);
 
         if ($data['success'] == true) {
             return redirect()->back()->with('success_message', $data['message']);
         } else {
             return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      } catch(\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }
}