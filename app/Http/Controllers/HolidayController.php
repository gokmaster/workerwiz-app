<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;

class HolidayController extends Controller { 
   public function holidayCreateForm(){
      return view('holiday/holiday_create',[
         'holidays' => $this->fetch(),
      ]);
   }

   public function holidayCreate(Request $request){
      try {
         $this->validate($request, [
            'holiday_name' => 'required',
            'holiday_date' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['holiday_date'] = date("Y-m-d", strtotime($postdata['holiday_date']) ); // format date to yyyy-mm-dd
         $postdata['company_id'] = Session::get('user_details')['company_id'];
                 
         $url = env("LOGIN_API_URL") . "/api/holiday/create"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            return redirect()->back()->with('success_message', $data['message']);
         } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
      } catch(\Exception $e) {
         return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
      }
   }

   public function holidayEdit(Request $request){
      try {
         $this->validate($request, [
             'holiday_name' => 'required',
             'holiday_date' => 'required',
         ]);
             
         $postdata = $request->except('_token');
         $postdata['holiday_date'] = date("Y-m-d", strtotime($postdata['holiday_date']) ); // format date to yyyy-mm-dd
                 
         $url = env("LOGIN_API_URL") . "/api/holiday/edit"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('holiday/partial/holiday_table',[
                        'holidays' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      }  catch (\Illuminate\Validation\ValidationException $e ) {
         // When there is any invalid input
         return json_encode([
            'success' => false,
            'message' => json_encode($e->errors()),
         ]);
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   public function holidayDelete(Request $request) {
      try {
         $postdata = $request->except('_token');
                       
         $url = env("LOGIN_API_URL") . "/api/holiday/delete"; 
         
         $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
         ];
 
         $HttpReq = new HttpRequest;
         $data = $HttpReq->post($url , $header, $postdata);
 
         if ($data['success'] == true) {
            $html =  view('holiday/partial/holiday_table',[
                        'holidays' => $this->fetch(),
                     ])->render();

            return json_encode([
               'success' => true,
               'message' => $data['message'],
               'html' => $html,
            ]);
         } else {
            return json_encode([
               'success' => false,
               'message' => $data['message'],
            ]);
         }
      } catch(\Exception $e) {
         return json_encode([
            'success' => false,
            'message' => $e->getMessage(),
         ]);
      }
   }

   private function fetch() {
      $url = env("LOGIN_API_URL") . "/api/holiday/fetch"; 
            
      $header = [
         'Authorization' => 'Bearer ' . Session::get('access_token'),        
      ];

      $HttpReq = new HttpRequest;
      $postdata = [
         'company_id' => Session::get('user_details')['company_id']
      ];
      return $HttpReq->post($url , $header, $postdata);
   }
}