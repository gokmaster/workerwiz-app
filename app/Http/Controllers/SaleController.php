<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use App\Country;
use App\Campaign;
use App\Customfield;
use App\Role;
use Config;
use Session;
use DateTime;
use Crypt;
use Input;

class SaleController extends Controller {    
    public function saleAddForm(Request $request) {  
        $model = new Country;  
        
        $cpModel = new Campaign;
        $campaigns = $cpModel->campaignsOfCompanyFetch();
        
        // if campaigns exist AND $_GET does NOT have 'campaign' parameter
        if ($campaigns && !$request->exists('campaign')) {
            return view('sale/sale_choose_campaign',[
                'campaigns' => $campaigns,
            ]);
        }

        $fieldModel = new Customfield;
        $campaignId = 0;
        $campaign = "";

        if ($request->exists('campaignid')) {
            $campaignId = hex2bin($_GET['campaignid']);
        }

        if ($request->exists('campaign')) {
            $campaign = hex2bin($_GET['campaign']);
        }
        
        return view('sale/sale_add',[
            'countries' => $model->countriesList(),
            'customfields' => $fieldModel->fieldsGroupedByFormSection('sale', $campaignId),
            'campaign_id' => $campaignId,
            'campaign' => $campaign,
        ]);
    }

    public function saleEditForm($saleId) {  
        $model = new Country;  
        $fieldModel = new Customfield;

        $data = $this->saleDataFetch($saleId);

        if (isset($data['creditcard_number'])) {
            if ($data['creditcard_number'] != null) {
                $data['creditcard_number'] = Crypt::decrypt($data['creditcard_number']);
            }
        }

        $priceComfortMin = 100;
        $priceComfortMax = 300;
        if (isset($data['price_comfort'])) {
            if ($data['price_comfort'] != null) {
                $priceComfort = explode(" - ",$data['price_comfort']);
                $priceComfortMin = trim($priceComfort[0],"$");
                $priceComfortMax = trim($priceComfort[1],"$");
            }
        }
        
        return view('sale/sale_edit',[
            'countries' => $model->countriesList(),
            'sale' => $data,
            'customfields' => $fieldModel->fieldsGroupedByFormSection('sale', $data['campaign_id']),
            'customfieldValues' => $fieldModel->fieldValuesOfLinkedTableRow('sale', $data['id']),
            'priceComfortMin' => $priceComfortMin,
            'priceComfortMax' => $priceComfortMax,
        ]);
    }

    public function saleAdd(Request $request) {     
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'phone' => 'required',
                'sku' => 'required',
                'quantity' => 'required|numeric|min:1',
                'saleprice' => 'required|numeric|min:0',
            ]);
            
            $postdata = $request->except('_token');
            $postdata['sale_agent'] = Session::get('user_details')['id'];
            $postdata['company_id'] = Session::get('user_details')['company_id'];

            if (isset($postdata['creditcard_number'])) {
                $postdata['creditcard_number'] = Crypt::encrypt($postdata['creditcard_number']);
            }

            if (isset($postdata['card_expiry_date'])) {
                $expDate = '01/'.$postdata['card_expiry_date']; // format from mm/yyyy to dd/mm/yyyy
                $expDate = date_format(date_create_from_format('d/m/Y', $expDate), 'Y-m-d'); // format to yyyy-mm-dd
                $d = new DateTime($expDate);  
                $postdata['card_expiry_date'] = $d->format('Y-m-t'); // get last day of month
            }
                                      
            $url = env("LOGIN_API_URL") . "/api/sale/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return redirect()->back()->with('success_message', json_encode($data['message']));
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input            
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function saleEdit(Request $request) {     
        try {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'phone' => 'required',
                'sku' => 'required',
                'quantity' => 'required|numeric|min:1',
                'saleprice' => 'required|numeric|min:0',
            ]);
            
            $postdata = $request->except('_token');
            $postdata['company_id'] = Session::get('user_details')['company_id'];

            if (isset($postdata['creditcard_number'])) {
                $postdata['creditcard_number'] = Crypt::encrypt($postdata['creditcard_number']);
            }

            if (isset($postdata['card_expiry_date'])) {
                $expDate = '01/'.$postdata['card_expiry_date']; // format from mm/yyyy to dd/mm/yyyy
                $expDate = date_format(date_create_from_format('d/m/Y', $expDate), 'Y-m-d'); // format to yyyy-mm-dd
                $d = new DateTime($expDate);  
                $postdata['card_expiry_date'] = $d->format('Y-m-t'); // get last day of month
            }

            $fieldModel = new Customfield;
            $customfieldValues = $fieldModel->fieldValuesOfLinkedTableRow('sale', $postdata['sale_id']);

            if ($customfieldValues) {
                $inputCodenames = array_column($customfieldValues, 'input_codename');

                foreach ($inputCodenames as $ic) {
                    if (!isset($postdata[$ic])) {
                        $postdata[$ic] = ""; // assign blank-value if this input has no value but had a value previously
                    }
                }
            }            
                                      
            $url = env("LOGIN_API_URL") . "/api/sale/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return redirect(route('sale-my'))->with('success_message', json_encode($data['message']));
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input            
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function salesOfAgent(Request $request) {
        $year = date("Y");

        if ($request->exists('year')) {
            $year = $_GET['year'];
        }

        $userId = Session::get('user_details')['id'];
        $model = new Role;
       
        return view('sale/sales_my',[
            'sales' => $this->salesOfAgentForYearFetch($userId,$year), // fetch sales for current year
            'yearViewed' => $year,
            'thisyear' => date('Y'),
            'thisyearLess1' => date("Y",strtotime("-1 year")),
            'timezone' => Session::get('company_details')['timezone'],
            'viewerPermissions' => $model->userPermissions($userId),
        ]);
    }

    public function salesAll(Request $request) {
        $year = date("Y");

        if ($request->exists('year')) {
            $year = $_GET['year'];
        }

        $model = new Role;

        return view('sale/sales_all',[
            'sales' => $this->salesOfYearFetch($year), // fetch sales for current year
            'yearViewed' => $year,
            'thisyear' => date('Y'),
            'thisyearLess1' => date("Y",strtotime("-1 year")),
            'timezone' => Session::get('company_details')['timezone'],
            'viewerPermissions' => $model->userPermissions(Session::get('user_details')['id']),
        ]);
    }

    public function saleDetails($saleId) {
        $data = $this->saleDataFetch($saleId);

        if ($data['creditcard_number'] != null) {
            $data['creditcard_number'] = Crypt::decrypt($data['creditcard_number']);
        }

        $model = new Role;
       
        return view('sale/sale_details',[
           'sale' => $data,
           'viewerPermissions' => $model->userPermissions(Session::get('user_details')['id']),
        ]);
    }

    private function salesOfYearFetch($year) {
        $url = env("LOGIN_API_URL") . "/api/sales/ofyear"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = [
            'year' => $year,
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        $sales = $HttpReq->post($url , $header, $postdata);

        if (count($sales)) {
            foreach ($sales as $key=>$sale) {
                if ($sale['creditcard_number'] != null) {
                    $sales[$key]['creditcard_number'] = Crypt::decrypt($sale['creditcard_number']);
                }
            }  
        }
        return $sales;
    }

    private function salesOfAgentForYearFetch($userId, $year) {
        $url = env("LOGIN_API_URL") . "/api/sales/ofagent/foryear"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = [
            'user_id' => $userId,
            'year' => $year,
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        $sales = $HttpReq->post($url , $header, $postdata);

        if (count($sales)) {
            foreach ($sales as $key=>$sale) {
                if ($sale['creditcard_number'] != null) {
                    $sales[$key]['creditcard_number'] = Crypt::decrypt($sale['creditcard_number']);
                }
            }  
        }
        return $sales;
    }

    private function saleDataFetch($saleId) {
        $url = env("LOGIN_API_URL") . "/api/sale/fetch"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = [
            'sale_id' => $saleId,
            'company_id' => Session::get('user_details')['company_id'],
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url , $header, $postdata);
    }
}