<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;

class KpiController extends Controller {
    public function kpiCreateForm() {
        return view('kpi/kpicreate', [
            'kpigroups' => $this->kpiGroups(),
            'kpidetails' => $this->kpiDetails(),
        ]);
    }

    public function kpiGroupCreate(Request $request) {
        try {
            $this->validate($request, [
                'description' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['company_id'] = Session::get('user_details')['company_id'];
                        
            $url = env("LOGIN_API_URL") . "/api/kpi/group/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $kpigroupshtml =  view('kpi/partial/kpigroup_table', ['kpigroups' => $this->kpiGroups()])->render();
                return json_encode([
                    'success' => true,
                    'message' => $kpigroupshtml,
                ]);
            }
            return json_encode($data);
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]); 
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);  
        }
    }

    public function kpiDetailCreate(Request $request) {
        try {
            $this->validate($request, [
                'description' => 'required',
                'target' => 'required',
                'target_type' => 'required',
            ]);
                
            $postdata = $request->except('_token');

            $inputname = str_replace(' ', '_', $postdata['description']); // replace spaces with underscore
            $postdata['input_name'] = preg_replace('/[^A-Za-z0-9\-]/', '', $inputname); // Removes special chars
            $postdata['company_id'] = Session::get('user_details')['company_id'];
                        
            $url = env("LOGIN_API_URL") . "/api/kpi/detail/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                $kpiHtml =  view('kpi/partial/kpidetail_table', ['kpidetails' => $this->kpiDetails()])->render();
                return json_encode([
                    'success' => true,
                    'message' => $kpiHtml,
                ]);
            }
            return json_encode($data);
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]); 
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);  
        }
    }

    public function kpiAssignToGroupHtml(Request $request) {
        $url = env("LOGIN_API_URL") . "/api/kpi/ofgroup"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $postdata = $request->except('_token');

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);

        $groupKpis = array_column($data, 'kpi_detail_id');

        $kpiHtml =  view('kpi/partial/kpi_assign', [
            'kpidetails' => $this->kpiDetails(), 
            'kpigroupId' => $postdata['kpi_group_id'], 
            'groupKpis' => $groupKpis,
        ])->render();

        return json_encode([
            'success' => true,
            'message' => $kpiHtml,
        ]);  
    }

    public function kpiAssignToGroup(Request $request) {
        try {
            $this->validate($request, [
                'kpi_group_id'  => 'required',
                'kpi_detail_ids' => 'required',
            ]);

            $url = env("LOGIN_API_URL") . "/api/kpi/assign/togroup"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];

            $postdata = $request->except('_token');

            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
                
            if ($data['success'] == true) {
                return json_encode([
                    'success' => true,
                    'message' => "Successfully assigned KPIs to KPI-group",
                ]); 
            }
            return json_encode($data);
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]); 
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);  
        }
    }

    public function kpiAssignToUserForm() {
        $url = env("LOGIN_API_URL") . "/api/team/members/ofuser"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = [
            'user_id' => Session::get('user_details')['id'],
            'company_id' => Session::get('user_details')['company_id']
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);
        
        return view('kpi/kpi_user_assign', [
            'users' => $data,
        ]);
    }

    public function kpiAssignToUser(Request $request) {
        try {
            $this->validate($request, [
                'kpi_group_id'  => 'required',
                'kpi_startdate' => 'required',
                'user_id' => 'required',
            ]);

            $postdata = $request->except('_token');
            $postdata['kpi_startdate'] = date("Y-m-d", strtotime($postdata['kpi_startdate']) ); // format date to yyyy-mm-dd

            if(date('w', strtotime($postdata['kpi_startdate'])) !== '1') {
                throw new \Exception( "Start date should be a Monday." ); 
            }
          
            $url = env("LOGIN_API_URL") . "/api/kpi/assign/touser"; 
                
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
        
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return json_encode([
                    'success' => true,
                    'message' => "Successfully assigned KPI to user",
                ]); 
            }
            return json_encode($data);
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]); 
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]);  
        }
    }

    public function kpiAssignToUserHtml(Request $request) {
        $userId = $request->input('user_id');

        $kpiHtml =  view('kpi/partial/assignkpi_user', [
            'kpigroups' => $this->kpiGroups(), 
            'userId' => $userId,
        ])->render();

        return json_encode([
            'success' => true,
            'message' => $kpiHtml,
        ]);  
    }

    public function userKpiEntryForm() {
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $url = env("LOGIN_API_URL") . "/api/team/members/ofuser";          
        $postdata = [
            'user_id' => Session::get('user_details')['id'],
            'company_id' => Session::get('user_details')['company_id']
        ];
        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);
        

        $url2 = env("LOGIN_API_URL") . "/api/rep/all";          
        $HttpReq = new HttpRequest;
        $data2 = $HttpReq->get($url2 , $header, []);
    
        return view('kpi/userkpi_entry', [
            'users' => $data,
            'reps' => $data2,
        ]);
    }

    public function userKpisForEntryHtml(Request $request) {
        $url = env("LOGIN_API_URL") . "/api/kpi/userkpis"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = $request->except('_token');

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);

        $kpiHtml =  view('kpi/partial/userkpi_entry_table', [
            'userKpis' => $data['userKpis'], 
            'dateHeader' => $data['dateHeader'],
        ])->render();

        return json_encode([
            'success' => true,
            'message' => $kpiHtml,
        ]);  
    }

    public function userKpiUpdate(Request $request) {
        try {
            $this->validate($request, [
                'datajson' => 'required',
            ]);

            $url = env("LOGIN_API_URL") . "/api/kpi/userkpi/update"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
            $postdata = $request->except('_token');

            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);

            if ($data['success'] == true) {
                return json_encode([
                    'success' => true,
                    'message' => "Successfully updated User-KPIs",
                ]); 
            }
            return json_encode($data);
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return json_encode([
                'success' => false,
                'message' => $e->errors(),
            ]); 
        } catch(\Exception $e) {
            return json_encode([
                'success' => false,
                'message' => $e->getMessage(),
            ]); 
        }
    }

    public function kpiReport() {
        return view('kpi/kpi_report', [
            'kpiGroups' => $this->kpiGroups()
        ]);
    }

    public function kpiReportHtml(Request $request) {
        $url = env("LOGIN_API_URL") . "/api/kpi/report"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];
        
        $postdata = $request->except('_token');
        $postdata['start_date'] = date("Y-m-d", strtotime($postdata['start_date']) ); // format date to yyyy-mm-dd
        $postdata['end_date'] = date("Y-m-d", strtotime($postdata['end_date']) ); // format date to yyyy-mm-dd

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, $postdata);

        $kpiHtml =  view('kpi/partial/kpi_report_table', [
            'userKpis' => $data['userKpis'], 
            'kpiDescripHeader' => $data['kpiDescripHeader'],
        ])->render();

        return json_encode([
            'success' => true,
            'message' => $kpiHtml,
        ]);  
    }

    private function kpiGroups() {
        $url = env("LOGIN_API_URL") . "/api/kpi/group/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

    private function kpiDetails() {
        $url = env("LOGIN_API_URL") . "/api/kpi/detail/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        return $HttpReq->post($url , $header, $postdata);
    }

}