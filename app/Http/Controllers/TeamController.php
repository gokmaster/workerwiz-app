<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;

class TeamController extends Controller {    
    public function teamCreateForm() {
        $url = env("LOGIN_API_URL") . "/api/users"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        $data = $HttpReq->post($url , $header, $postdata);

        return view('team/teamcreate',[
            'users' => $data
        ]);
    }

    public function teamCreate(Request $request) {
        try {
            $this->validate($request, [
                'team_name' => 'required',
                'team_leader' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['member_type'] = 'leader';
            
            $url = env("LOGIN_API_URL") . "/api/team/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect(route('team-all'))->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        }
    }

    public function teamAll() {
        $url = env("LOGIN_API_URL") . "/api/team/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        $data = $HttpReq->post($url , $header, $postdata);
       
        return view('team/teamall', [
            'teams' => $data
        ]);
    }

    public function membersEditForm($teamId, $encodedTeamName) {
        $url = env("LOGIN_API_URL") . "/api/team/users"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'team_id' => $teamId,
            'company_id' => Session::get('user_details')['company_id'] 
        ];
        $data = $HttpReq->post($url , $header, $postdata);

        if (isset($data['success'])) {
            if($data['success'] == false) {
                return $data['message']; // stop here if there is error
            }
        }

        $userIds = [];
        $currentMembers = [];
        $i = 0;

        foreach ($data as $d) {
            if (in_array($d['user_id'], $userIds)) {
                unset($data[$i]); // remove duplicate user_id from array
            }

            array_push($userIds, $d['user_id']);

            // current team members
            if ($d['team_id'] == $teamId) {
                array_push($currentMembers, $d);
            }
            $i++;
        }
       
        return view('team/members_edit', [
            'teamId' => $teamId,
            'teamName' => hex2bin($encodedTeamName),
            'users' => $data,
            'currentMembers' => $currentMembers,
        ]);
    }

    public function membersEdit(Request $request) {
        $url = env("LOGIN_API_URL") . "/api/team/members/edit"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $selectedMembers = $request->input('selected_members');

        $members = json_decode($selectedMembers, true);
        $teamId = $members[0]['team_id'];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, [
            'selected_members' => $selectedMembers
        ]);
        
        if ($data['success'] == true) {
            return redirect(route('team', ['team_id' => $teamId]) )->with('success_message', $data['message']);
        } else {
            return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
        }
    }

    public function team($teamId) {
        $url = env("LOGIN_API_URL") . "/api/team/members"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->post($url , $header, [
            'team_id' => $teamId,
            'company_id' => Session::get('user_details')['company_id']
        ]);

        return view('team/members', [
            'members' => $data,
        ]);
    }
}