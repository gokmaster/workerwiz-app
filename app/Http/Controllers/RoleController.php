<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Config;
use Session;

class RoleController extends Controller {    
    public function roleCreateForm() {  
        $url = env("LOGIN_API_URL") . "/api/task/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $data = $HttpReq->get($url , $header, []);
       
        return view('role/rolecreate', [
            'tasks' => $data,
            'companyPrivilegeLevel' => Session::get('company_details')['privilege_level'],
        ]);         
    }

    public function roleCreate(Request $request) {   
        try {
            $this->validate($request, [
                'role_name' => 'required',
                'tasks' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['company_id'] = Session::get('user_details')['company_id'];
                      
            $url = env("LOGIN_API_URL") . "/api/role/create"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect(route('role-all'))->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        } 
    }

    public function roleEditForm($roleId) {              
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $url2 = env("LOGIN_API_URL") . "/api/role/fetch"; 
        $HttpReq = new HttpRequest;
        $postdata = [
            'role_id' => $roleId, 
            'company_id' => Session::get('user_details')['company_id']    
        ];
        $role = $HttpReq->post($url2 , $header, $postdata);

        if (isset($role['success'])) {
            if($role['success'] == false) {
                return $role['message'];
            }
        }

        $url = env("LOGIN_API_URL") . "/api/task/all"; 
        $HttpReq = new HttpRequest;
        $tasks = $HttpReq->get($url , $header, []);
              
        return view('role/role_edit', [
            'tasks' => $tasks,
            'roleTasks' => $role['roletasks'],
            'role' => $role['role'],
            'roleId' => $roleId,
            'companyPrivilegeLevel' => Session::get('company_details')['privilege_level'],
        ]);         
    }

    public function roleEdit(Request $request) {   
        try {
            $this->validate($request, [
                'role_id' => 'required',
                'role_name' => 'required',
                'tasks' => 'required',
            ]);
                
            $postdata = $request->except('_token');
            $postdata['company_id'] = Session::get('user_details')['company_id'];
                      
            $url = env("LOGIN_API_URL") . "/api/role/edit"; 
            
            $header = [
                'Authorization' => 'Bearer ' . Session::get('access_token'),        
            ];
    
            $HttpReq = new HttpRequest;
            $data = $HttpReq->post($url , $header, $postdata);
    
            if ($data['success'] == true) {
                return redirect(route('role-all'))->with('success_message', $data['message']);
            } else {
                return redirect()->back()->with('fail_message', json_encode($data['message']))->withInput(); 
            }
        }  catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            return redirect()->back()->with('fail_message', json_encode($e->errors()))->withInput(); 
        } catch(\Exception $e) {
            return redirect()->back()->with('fail_message', json_encode($e->getMessage()))->withInput(); 
        } 
    }

    public function roleAll(Request $request) {   
        $url = env("LOGIN_API_URL") . "/api/role/all"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        $postdata = [
            'company_id' => Session::get('user_details')['company_id']
        ];
        $data = $HttpReq->post($url , $header, $postdata);
        return view('role/role_all', [
            'roles' => $data
        ]);
    }
}