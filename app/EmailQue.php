<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lib\HttpRequest;
use Session;
  
class EmailQue extends Model
{
    public function addEmailToQue($postdata){
        $url = env("LOGIN_API_URL") . "/api/emailque/create"; 
            
        $header = [
            'Authorization' => 'Bearer ' . Session::get('access_token'),        
        ];

        $HttpReq = new HttpRequest;
        return $HttpReq->post($url, $header, $postdata);
    }
}
?>