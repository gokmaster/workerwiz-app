<?php

namespace App\Lib;

use GuzzleHttp\Client;

class HttpRequest
{
    public function get($url, $header, $form_param) {
        $client = new Client;
      
        $response = $client->request('GET', $url , [
            'headers' => $header,
            'form_params' => $form_param,
        ]);

        return json_decode($response->getBody(), true);
    }

    public function post($url , $header, $form_param) {
        $client = new Client;
       
        $response = $client->request('POST', $url , [
            'headers' => $header,
            'form_params' => $form_param,
        ]);
   
        return json_decode($response->getBody(), true);
    }
  
}
