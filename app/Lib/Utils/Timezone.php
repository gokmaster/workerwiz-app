<?php

namespace App\Lib\Utils;

use DateTime;
use DateTimeZone;

class Timezone {

    public function timezoneListing() {
        $zones_array = array();
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $zones_array[$key]['zone'] = $zone;
            $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
        }
        return $zones_array;
    }

    // example use: utcToSpecificTimezone('2020-05-14 10:49:06', 'Pacific/Fiji', "D d M, Y - H:i")
    public static function utcToSpecificTimezone($utcDatetime, $timezoneConvertingTo, $format) {
        $date = new DateTime($utcDatetime.' +00');
        $date->setTimezone(new DateTimeZone($timezoneConvertingTo)); 
        
        return $date->format($format);
    }

     // example use: timezoneUTCoffset('Pacific/Fiji')
    public function timezoneUTCoffset($timezone) {
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            if (strtolower($zone) == strtolower($timezone)) {
                return date('P', $timestamp);
            }
        }
    }

}