<?php
namespace App\Lib;

class Url
{
    /**
 * Get the HTTP(S) URL of the current page.
 *
 * @param $server The $_SERVER superglobals array.
 * @return string The URL.
 */

    // use currentUrl($_SERVER) to call this function
    function currentUrl($server){
        //Figure out whether we are using http or https.
        $http = 'http';
        //If HTTPS is present in our $_SERVER array, the URL should
        //start with https:// instead of http://
        if(isset($server['HTTPS'])){
            $http = 'https';
        }
        //Get the HTTP_HOST.
        $host = $server['HTTP_HOST'];
        //Get the REQUEST_URI. i.e. The Uniform Resource Identifier.
        $requestUri = trim($server['REQUEST_URI'], '/'); // trim forward slashes
        //Finally, construct the full URL.
        //Use the function htmlentities to prevent XSS attacks.
        return $http . '://' . htmlentities($host) . '/'. htmlentities($requestUri);
    }

    // get root-domain of current website
    // use rootDomain($_SERVER) to call this function
    function rootDomain($server) {
        return (!empty($server['HTTPS']) ? 'https' : 'http') . '://' . $server['HTTP_HOST'] . '/';
    }

    // getRootDomainFromUrl("http://example.com/xyz"); // Gives example.com
    function getRootDomainFromUrl($url) { 
        $parseUrl = parse_url(trim($url)); 
        $rootDomain = trim($parseUrl['host'] ? $parseUrl['host'] : array_shift(explode('/', $parseUrl['path'], 2)));
        
        if (array_key_exists('port', $parseUrl)) {
            return $rootDomain . ":" . $parseUrl['port'];
        }
        
        return $rootDomain;
    }
}


