@extends('layouts.app')

@section('assets')

@endsection

@section('content')
<h2 class="pageheading">My Payslip</h2>
<div class="table-container">
    @include('payroll.partial.user_payslip_table')
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({
                
        });
    });
</script>
@endsection
