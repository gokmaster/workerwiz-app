@extends('layouts.app')

@section('assets')

@endsection

@section('content')
<h2 class="pageheading">All Users' Payslips</h2>

<select class="form-control shortwidth-select" id="user" name="user">
    <option disabled selected value> -- Select a user -- </option>
    @foreach($users as  $user)
    <option value="{{ $user['id'] }}">{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</option>
    @endforeach
</select>

<div id="payslip-table-div" class="table-container">
   
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
    $('#user').on('change', function() {
        $('#payslip-table-div').html('');
        var data = new FormData();
        data.append("user_id",this.value);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('payslip-ofuser-table-html') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                $('#payslip-table-div').html(response);

                $('.maintable').DataTable({});
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    });
});
</script>
@endsection
