<?php 
    $bonusTotal = 0;
    if (isset($customfields['Bonus Breakdown'])) {
        foreach ($customfields['Bonus Breakdown'] as $bonus) {
            if ($bonus != null) {
                $bonusTotal += $bonus;
            }
        }
    }
    
    // $ytdAnnualLeave = 0;
    
    // $now = time(); // or your date as well
    // $dateDiff = $now - strtotime($emp_startdate);
    // $daysSinceJoined = round($dateDiff / (60 * 60 * 24));

    // if ( $daysSinceJoined > 90) {
    //     // annual leave cannot be taken if employee has not completed 90-day probation period
    //     $ytdAnnualLeave = round(($days_earned/261) * ($weekly_normal_hours * 2),2);
    // }
?>

<html>
<head>
    <style>
        .wrapper {
            width: 19cm;
        }

        .left-div {
            width: 70%;
            display: inline-block;
        }

        .right-div {
            width: 30%;
            display: inline-block;
        }

        .logo {
            width: 90%;
        }

        table {
            border-collapse: collapse;
        }

        .payslip-table th, .payslip-table td,
        .bonus-table th, .bonus-table td {
            padding: 3px;
            border: solid 1px #000;
        }

        .payslip-table {
            width: 100%;
        }

        .payslip-table th, .payslip-table td {
            text-align: center;
        }

        .bonus-table {
            width: 35%;
        }

        .bonus-table td:last-child {
            text-align: right;
        }

        .empinfo-table {
            width: 100%;
            padding-bottom: 2px;
        }
    </style>
</head>

<body>
<div class="wrapper">
    <div>
        <div class="left-div">
            <h3>{{strtoupper($company_name)}}</h3>
            WAGES SLIP &nbsp;&nbsp;&nbsp; {{ date("d.m.y", strtotime($payperiod_start)) }}-{{ date("d.m.y", strtotime($payperiod_end)) }}
        </div>
        <div class="right-div">
            
        </div>
    </div>

    <table class="empinfo-table">
        <tr>
            <td><b>NAME:</b> &nbsp;&nbsp;&nbsp; {{ strtoupper($emp_name) }} </td>
            <td><b>DATE STARTED:</b>  &nbsp;&nbsp;&nbsp; {{ date("d M Y", strtotime($emp_startdate)) }} </td>
        </tr>
        <tr>
            <td><b>TAX IDENTIFICATION NO.:</b> &nbsp;&nbsp;&nbsp; {{ $tin_number }} </td>
            <td><b>SUPERANNUATION A/C NO.:</b> &nbsp;&nbsp;&nbsp; {{ $fnpf_number }} </td>
        </tr>
    </table>
    <hr>
    <table class="payslip-table">
        <tr>
            <th>Total Hours</th>
            <th>Rate</th>
            <th>BONUS</th>
            <th>Tax Deduction</th>
            <th>Gross Wages</th>
            <th>Superannuation Deduction</th>
            <th>Net Wages</th>
        </tr>
        <tr>
            <td style="background-color:#c9d1cb">{{ $total_hours_worked }}</td>
            <td>${{ number_format((float) $hourly_rate, 2, '.', '') }}</td>
            <td>${{ number_format((float) $bonusTotal, 2, '.', '') }}</td>
            <td>${{ number_format((float) $tax_deduction, 2, '.', '') }}</td>
            <td>${{ number_format((float) $gross_wages, 2, '.', '') }}</td>
            <td>${{ number_format((float) $superannuation_deduction, 2, '.', '') }}</td>
            <td>${{ number_format((float) $total_payable, 2, '.', '') }}</td>
        </tr>
    </table>
    <br>

    @if (isset($customfields))
        @foreach ($customfields as $sectionTitle=>$customfieldSection)
            @if (count($customfieldSection)>0)
                @if (empty(array_filter($customfieldSection, function ($a) { return $a !== null;})) == false) 
                <!-- if $customfieldSection does NOT contain only null -->
                    <b>{{$sectionTitle}}</b>
                    <table class="bonus-table">
                        @foreach ($customfieldSection as $fieldLabel=>$fieldVal)
                        @if ($fieldVal != null)
                            <tr>
                                <td>{{ucfirst($fieldLabel)}}</td>
                                <td>${{ number_format((float) $fieldVal, 2, '.', '') }}</td>
                            </tr>
                        @endif
                        @endforeach
                    </table>
                @endif
            @endif
        @endforeach 
    @endif
    
    <hr>
    <b>Year-to-date</b>
    <table class="bonus-table">
        <tr>
            <td>Superannuation Deduction</td>
            <td>${{ number_format((float) $superannuation_deduct_ytd, 2, '.', '') }}</td>
        </tr>
    </table>
</div>
</body>
</html>