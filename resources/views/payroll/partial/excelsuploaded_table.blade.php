<table class="maintable">
    <thead>
        <tr>
            <th>Excel Uploaded</th>
            <th>Start of Pay Period</th>
            <th>End of Pay Period</th>
            <th style="width:24%;"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($uploads as $u)        
        <tr>
            <td><a href="{{ route('payroll-mastersheet-download', ['filename' => bin2hex($u['filename']) ])}}">Download</a></td>
            <td>{{ date("D d M", strtotime($u['payperiod_start'])) }}</td>
            <td>{{ date("D d M", strtotime($u['payperiod_end'])) }}</td>
            <td id="tdGeneratePayslip-{{ $u['id'] }}">
                @if ($u['payslip_generated'] == 0)
                    <button class="btnGeneratePayslip" data-excelupload-id="{{ $u['id'] }}" data-payperiod-end="{{ $u['payperiod_end'] }}">Generate Payslip</button>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

