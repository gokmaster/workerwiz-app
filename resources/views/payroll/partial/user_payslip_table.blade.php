
<table class="display maintable" style="width:100%">
    <thead>
        <tr>
            <th>Payslip</th>
            <th>Start of Pay Period</th>
            <th>End of Pay Period</th>
            <th>Created on</th>
        </tr>
    </thead>
    <tbody>
    @foreach($payslips as $payslip)
    <tr>
        <td><a href="{{ route('payslip-download', ['filepath' => bin2hex($payslip['filepath']) ])}}">Download</a></td>
        <td>{{ date("d M, Y", strtotime($payslip['payperiod_start'])) }}</td>
        <td>{{ date("d M, Y", strtotime($payslip['payperiod_end'])) }}</td>
        <td>{{ date("d M, Y - H:i", strtotime($payslip['created_at'])) }}</td>
    </tr>
    @endforeach
    </tbody>
</table>

