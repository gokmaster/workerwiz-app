@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
  <h2 class="pageheading">Payslip Generator</h2>
  @if(session()->has('success_message'))
      <div class="alert alert-success">
          {{ session()->get('success_message') }}
      </div>
  @elseif(session()->has('fail_message'))
      <div class="alert alert-danger">
          {{ session()->get('fail_message') }}
      </div>
  @endif
  
  <p>
    <b>How to use this Payslip Generator?</b><br>
    Download this <a href="{{route('payslipgen-exceltemplate-download')}}">excel template</a> and modify it with the payroll data for your employees.
    Then upload the modified excel template in the upload-box below.
  </p>
  <input id="payperiod_start" name="payperiod_start" class="shorttextbox" type="text" placeholder="Start of pay period" autocomplete="off" required/>
  <input id="payperiod_end" name="payperiod_end" class="shorttextbox" type="text" placeholder="End of pay period" autocomplete="off" required/> 

  <form id="myform" action="{{ route('payroll-mastersheet-upload') }}" class="dropzone" enctype="multipart/form-data" method="post">
    @csrf
    <div class="fallback">
      <input type="file" name="file" />
    </div>
  </form> 

  <br>
  <p>
    <b>Note:</b><br>
    This Payslip Generator does not perform calculations of gross wages, superannuation deduction, tax deduction, nor total wages payable.
    <br>Therefore, please ensure you have inputed the correct figures into the excel template before uploading.
  </p>

  <br>
  <div id="uploaded-files-div">
    @if (count($uploads) > 0) 
      @include('payroll.partial.excelsuploaded_table')
    @endif
  </div>   
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
<script>
  Dropzone.autoDiscover = false;

  $(function () {
    $("#payperiod_start").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function(selected) {
            $("#payperiod_end").datepicker("option","minDate", selected)
        } // end date should not be less than start date
    });
    
    $("#payperiod_end").datepicker({
        dateFormat: "dd-mm-yy",
        onSelect: function(selected) {
            $("#payperiod_start").datepicker("option","maxDate", selected)
        } // start date should not be greater than end date
    });

    var myDropzone = new Dropzone(".dropzone", {
      dictDefaultMessage: "Drop excel file here to upload"
    }); 

    Dropzone.options.myDropzone = {
      maxFiles: 1,
    };

    // on successful upload
    myDropzone.on('success', function (file, res) {
        if (res.success == true) {
          swal("Success", res.message, "success");
          $('#uploaded-files-div').html(res.html); // show uploaded files in a table
        } else {
          $('.dz-preview').remove(); // remove the upload-preview icon
          $('.dropzone.dz-started .dz-message').show(); // show the "Drop file here to upload" message
          swal("Failed", res.message, "error");
        }
    });

    myDropzone.on('sending', function(data, xhr, formData){ 
      // Called just before each file is sent

      var startdate = $("#payperiod_start").val();
      var enddate = $("#payperiod_end").val();

      if (startdate != null && startdate != ""
        && enddate != null && enddate != "") {
        $('.dropzone.dz-started .dz-message').hide(); // hide the "Drop file here to upload" message
        formData.append('payperiod_start', startdate);
        formData.append('payperiod_end', enddate); 
      } else {
        $('.dz-preview').remove(); // remove the upload-preview icon
        $('.dropzone.dz-started .dz-message').show(); // show the "Drop file here to upload" message
        swal("Failed", "Start date and end date are required", "error");
      }
    });

    $(document).on("click",".btnGeneratePayslip", function(e) {
      var buttonJustClicked = e.target;
      var id = $(buttonJustClicked).data('excelupload-id');
      var payperiodEnd = $(buttonJustClicked).data('payperiod-end');

      $('#tdGeneratePayslip-'+id).html('Please wait...'); // spinner

      var data = new FormData();
      data.append("payroll_excel_upload_id", id);
      data.append("payperiod_end", payperiodEnd);

      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('payslips-generate') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          console.log(response);
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object
          if (jsonObj.success == true) {
            $('#tdGeneratePayslip-'+id).html(''); // remove spinner
            swal("Success", jsonObj.message, "success");
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
          swal("Failed", "Something went wrong", "error");
        }
      }); // $.ajax
    });
  });
</script>
@endsection
