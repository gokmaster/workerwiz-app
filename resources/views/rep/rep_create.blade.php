@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('rep-create') }}" enctype="multipart/form-data">
      @csrf
      <h2 class="pageheading">Add Rep</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="fname">First Name<span>*</span></label>
            <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required/>
          </div>
          <div class="item">
            <label for="lname">Last Name<span>*</span></label>
            <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required/>
          </div>
          <div class="item">
            <label for="email">Email<span>*</span></label>
            <input id="email" type="text" name="email" value="{{ old('email') }}" required/>
          </div>
          <div class="item">
            <label for="zipcode">Post Code<span>*</span></label>
            <input id="zipcode" type="text" name="zipcode" value="{{ old('zipcode') }}" required/>
          </div>
          <div class="item">
            <label for="country">Country</label>
            <select class="form-control" id="country" name="country">
              <option disabled selected value> -- Select a country -- </option>
              <option value="Australia">Australia</option>
              <option value="New Zealand">New Zealand</option>				
            </select>
          </div>
          <div class="item">
            <label for="type">Type</label>
            <select class="form-control" id="type" name="type">
              <option value="consultant">Consultant</option>
              <option value="project-manager">Project Manager</option>				
            </select>
          </div>
		    </div>
      </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
      <br>
     
      <div id="table-div">
        @include('rep.partial.rep_table')
      </div>
</form>    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $("#myform").validate();

    $(document).on("click",".btnEdit", function(e){
        var buttonJustClicked = e.target;
        var repId = $(buttonJustClicked).data("rep-id"); 
        
        $('#row-'+ repId).hide();
        $('#editable-row-'+ repId).show();
    });

    $(document).on("click",".btnSave", function(e){
      var buttonJustClicked = e.target;
      var repId = $(buttonJustClicked).data("rep-id"); 
      var fname = $('#fname-' + repId).val();
      var lname = $('#lname-' + repId).val();
      var email = $('#email-' + repId).val();
      var zipcode = $('#zipcode-' + repId).val();
      var country = $('#country-' + repId).val();
      var type = $('#type-' + repId).val();
      
      var data = new FormData();
      data.append("id", repId);
      data.append("fname", fname);
      data.append("lname", lname);
      data.append("email", email);
      data.append("zipcode", zipcode);
      data.append("country", country);
      data.append("type", type);
      
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('rep-edit') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object

          if (jsonObj.success == true) {
            $('#table-div').html(jsonObj.html);
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
        }
      }); // $.ajax
    }); 

    $(document).on("click",".btnDelete", function(e){
      var buttonJustClicked = e.target;
      var repId = $(buttonJustClicked).data("rep-id"); 
      
      var data = new FormData();
      data.append("id",repId);
          
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('rep-delete') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object

          if (jsonObj.success == true) {
            $('#table-div').html(jsonObj.html);
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
        }
      }); // $.ajax
    });
  });
</script>
@endsection
