@if (count($reps))
    <table class="maintable">
        <thead>
        <tr>
            <th>
                Name
            </th>
            <th>
                Email
            </th>
            <th>
                Post Code
            </th>
            <th>
                Country
            </th>
            <th>
                Type
            </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($reps as $r)
            <tr id="row-{{$r['id']}}">
                <td>
                    {{ ucfirst($r['fname']) }} {{ ucfirst($r['lname']) }}
                </td>
                <td>
                    {{ $r['email'] }}
                </td>
                <td>
                    {{ ucfirst($r['zipcode']) }}
                </td>
                <td>
                    {{ ucfirst($r['country']) }}
                </td>
                <td>
                    {{ ucfirst($r['type']) }}
                </td>
                <td>
                    <button type="button" class="btnEdit iconbutton"><i class="fa fa-edit" data-rep-id="{{$r['id']}}"></i></button>
                    <button type="button" class="btnDelete iconbutton"><i class="fa fa-trash" data-rep-id="{{$r['id']}}"></i></button>
                </td>
            </tr>
            <tr id="editable-row-{{$r['id']}}" style="display:none;">
                <td>
                    <input id="fname-{{$r['id']}}" type="text" required value="{{ $r['fname'] }}" placeholder="First Name"/>
                    <input id="lname-{{$r['id']}}" type="text" required value="{{ $r['lname'] }}" placeholder="Last Name"/>
                </td>
                <td>
                    <input id="email-{{$r['id']}}" type="text" required value="{{ $r['email'] }}" placeholder="Email"/>
                </td>
                <td>
                    <input id="zipcode-{{$r['id']}}" type="text" required value="{{ $r['zipcode'] }}" placeholder="Post Code"/>
                </td>
                <td>
                    <select class="form-control" id="country-{{$r['id']}}">
                        <option disabled selected value> -- Select a country -- </option>
                        <option value="Australia" @if ($r['country'] == "Australia") selected @endif>Australia</option>
                        <option value="New Zealand" @if ($r['country'] == "New Zealand") selected @endif>New Zealand</option>				
                    </select>
                </td>
                <td>
                    <select class="form-control" id="type-{{$r['id']}}">
                        <option disabled selected value> -- Select a type -- </option>
                        <option value="consultant" @if ($r['type'] == "consultant") selected @endif>Consultant</option>
                        <option value="project-manager" @if ($r['type'] == "project-manager") selected @endif>Project Manager</option>				
                    </select>
                </td>
                <td><button type="button" class="btnSave smallbutton" data-rep-id="{{$r['id']}}">Save</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif