@if (count($departments))
    <table class="maintable">
        <thead>
        <tr>
            <th>
                Department Name
            </th>
            <th>
                Created on
            </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($departments as $d)
            <tr id="row-{{$d['id']}}">
                <td>
                    {{ ucfirst($d['department_name']) }}
                </td>
                <td>
                    {{ date("d M, Y", strtotime($d['created_at'])) }}
                </td>
                <td>
                    <button type="button" class="btnEdit iconbutton"><i class="fa fa-edit" data-department-id="{{$d['id']}}"></i></button>
                    <button type="button" class="btnDelete iconbutton"><i class="fa fa-trash" data-department-id="{{$d['id']}}"></i></button>
                </td>
            </tr>
            <tr id="editable-row-{{$d['id']}}" style="display:none;">
                <td><input id="department-name-{{$d['id']}}" type="text" required value="{{ $d['department_name'] }}"/></td>
                <td>{{ date("d M, Y", strtotime($d['created_at'])) }}</td>
                <td><button type="button" class="btnSave smallbutton" data-department-id="{{$d['id']}}">Save</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif