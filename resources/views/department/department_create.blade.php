@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('department-create') }}" enctype="multipart/form-data">
      @csrf
      <h2 class="pageheading">Add Department</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="department_name">Department Name<span>*</span></label>
            <input id="department_name" type="text" name="department_name" value="{{ old('department_name') }}" required/>
          </div>
		</div>
    </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
      <br>
     
      <div id="table-div">
        @include('department.partial.department_table')
      </div>
</form>    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $("#myform").validate();

    $(document).on("click",".btnEdit", function(e){
        var buttonJustClicked = e.target;
        var departmentId = $(buttonJustClicked).data("department-id"); 
        
        $('#row-'+ departmentId).hide();
        $('#editable-row-'+ departmentId).show();
    });

    $(document).on("click",".btnSave", function(e){
      var buttonJustClicked = e.target;
      var departmentId = $(buttonJustClicked).data("department-id"); 
      var departmentName = $('#department-name-' + departmentId).val();
           
      var data = new FormData();
      data.append("id", departmentId);
      data.append("department_name", departmentName);
           
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('department-edit') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object

          if (jsonObj.success == true) {
            $('#table-div').html(jsonObj.html);
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
        }
      }); // $.ajax
    }); 

    $(document).on("click",".btnDelete", function(e){
      var buttonJustClicked = e.target;
      var departmentId = $(buttonJustClicked).data("department-id"); 
      
      var data = new FormData();
      data.append("id", departmentId);
          
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('department-delete') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object

          if (jsonObj.success == true) {
            $('#table-div').html(jsonObj.html);
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
        }
      }); // $.ajax
    });
  });
</script>
@endsection
