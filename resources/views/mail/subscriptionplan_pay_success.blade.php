
Hello {{ucfirst($fname)}} {{ucfirst($lname)}},
<br><br>
We have received your payment for a {{env('APP_NAME')}} {{ucfirst($subscription_plan)}} subscription-plan.
<br><br>
The subscription-plan details are as follows:<br>
<b>{{ucfirst($subscription_plan)}} Subscription</b>: {{$user_quantity}} users for {{$subscription_period_months}} months
<br>
<b>Payment Total</b>: ${{ number_format((float) $payment_amount, 2, '.', '') }}

<br><br>
Kind Regards,<br>
The Development Team
<br>

<img width="190px" src="{{env('SITE_LOGO_URL')}}"/>

