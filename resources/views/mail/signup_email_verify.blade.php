
Hello {{ ucfirst($name) }},
<br><br>
Welcome to {{env('APP_NAME')}}.
<br><br>
Please click <a href="{{route('signup-email-verify', ['verification_code' => $verificationCode])}}">here</a> to activate your account.
<br><br><br>
Kind Regards,<br>
The Development Team
<br>

<img width="190px" src="{{env('SITE_LOGO_URL')}}"/>