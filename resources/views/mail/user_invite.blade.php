
Hello {{ ucfirst($name) }},
<br><br>
{{ ucfirst($company) }} has invited you to create a {{env('APP_NAME')}} User Account.
<br><br>
Please click <a href="{{route('invited-user-create-form', ['verification_code' => $verificationCode])}}">here</a> 
to create your {{env('APP_NAME')}} user account which will be associated with {{ ucfirst($company) }}.
<br><br><br>
Kind Regards,<br>
The Development Team
<br>

<img width="190px" src="{{env('SITE_LOGO_URL')}}"/>