
Hello {{ $name }},
<br><br>
Your {{env('APP_NAME')}} account has been created.
<br><br>
Use the following password to login to your {{env('APP_NAME')}} account:
{{ $password }}
<br><br>
You can change your password after logging in.
<br><br><br>
Kind Regards,<br>
The Development Team
<br>

<img width="190px" src="{{env('SITE_LOGO_URL')}}"/>
