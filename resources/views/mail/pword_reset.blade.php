
Your {{env('APP_NAME')}} account password has been resetted.
<br><br>
Your new password is:
<b>{{ $password }}</b>
<br><br><br>
Kind Regards,<br>
The Development Team
<br>

<img width="190px" src="{{env('SITE_LOGO_URL')}}"/>

