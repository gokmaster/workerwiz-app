@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           <input id="access_token" type="hidden" value="{{ $access_token }}"/>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    var access_token = $('#access_token').val();
    var login_url = "{{ env('LOGIN_URL') }}";

    if (access_token !== null && access_token !== "") {
        localStorage.setItem("access_token", access_token );
    } else {
        window.location.href = login_url + '/accesstoken/get';
    }
}); //document.ready
</script>
@endsection
