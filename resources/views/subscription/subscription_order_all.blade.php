@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <h2 class="pageheading">Subscription Plan Orders</h2>
    
    <div class="table-container">    
        <table class="display maintable" style="width:100%">
            <thead>
                <tr>  
                    <th>Order Date</th>  
                    <th>Company Name</th>
                    <th>Subscription Plan</th>
                    <th>Period</th>
                    <th>Number Of Users</th>
                    <th>Total Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order['created_at']}}</td>
                    <td>{{ ucwords(strtolower($order['company_name']))}}</td>
                    <td>{{ ucwords(strtolower($order['subscription_plan']))}}</td>
                    <td>{{ $order['subscription_period_months']}} months</td>
                    <td>{{ $order['user_quantity']}}</td>
                    <td>${{ $order['payment_amount']}}</td>
                    <td>
                        <button class="btnShowPayReceivedModal smallbutton" type="button" 
                            data-id="{{$order['id']}}" data-company="{{ucwords(strtolower($order['company_name']))}}" data-payment="{{$order['payment_amount']}}">Paid</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

      <!-- Modal -->
    <div id="paidModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Receipt of Payment</h4>
                </div>
                <form id="myform" method="post" action="{{ route('company-subscription-create') }}" enctype="multipart/form-data">
                    @csrf
                    <div id="paidBody" class="modal-body">
                        <input type="hidden" id="subscription_plan_order_id" name="subscription_plan_order_id" value=""/>
                        Are you sure you have received payment of $<span id="spn-payment"></span> from <span class="spn-company"></span>?
                        <br><br>
                        An email will be sent to <span class="spn-company"></span> confirming receipt of payment.
                    </div>
                    <div class="modal-footer">
                        <div class="btn-block">
                            <button type="submit">Confirm</button>
                        </div>  
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({   
        });

        $('[data-toggle="tooltip"]').tooltip(); 

        $(document).on("click",".btnShowPayReceivedModal", function(e){
            var orderId = $(e.target).data('id');
            var company = $(e.target).data('company');
            var payment = $(e.target).data('payment');

            $('#subscription_plan_order_id').val(orderId);
            $('#spn-payment').text(payment);
            $('.spn-company').text(company);

            $('#paidModal').modal('show');
        }); 
    });
</script>
@endsection
