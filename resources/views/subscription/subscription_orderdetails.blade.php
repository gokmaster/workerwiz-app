@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/multistepform.css') }}?rand={{mt_rand()}}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="container">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
	<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line1 active-bgcolor"></div>
                <div class="connecting-line2"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1">
                            <span class="round-tab">
                                <i class="fas fa-hand-pointer"></i>
                            </span>
                        </a>
                        <span class="step-label active-color">Subscription Details</span>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2">
                            <span class="round-tab">
                                <i class="fas fa-credit-card"></i>
                            </span>
                        </a>
                        <span class="step-label">Payment</span>
                    </li>
                    
                    <li role="presentation" class="disabled">
                        <a href="#complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                        <span class="step-label">Email Confirmation</span>
                    </li>
                </ul>
            </div>

            <form id="myform" method="get" action="{{ route('subscription-payment-form') }}">
                @csrf
                <input id="subscription_plan_order_id" name="subscription_plan_order_id" type="hidden" value="{{$order['subscription_plan_order_id']}}">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <p>
                            <h4>Order Details</h4>
                            <table class="ordertable">
                                <tr>
                                    <td style="width:87%;">
                                        <b>{{ucfirst($order['plan_title'])}} Subscription</b>: {{$order['user_quantity']}} users for {{$order['subscription_period_months']}} months
                                    </td>
                                    <td>
                                        <b>${{ number_format((float) $order['payment_amount'], 2, '.', '') }}</b>
                                    </td>
                                </tr>
                            </table>
                        </p>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Back</button></li>
                            <li><button type="submit" class="btn next-step">Proceed to Payment</button></li>
                        </ul>
                    </div>
                                       
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>
@endsection

@section('scripts')

@endsection
