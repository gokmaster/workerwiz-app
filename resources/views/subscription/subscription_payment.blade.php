@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/multistepform.css') }}?rand={{mt_rand()}}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="container">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
	<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line1 active-bgcolor"></div>
                <div class="connecting-line2"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1">
                            <span class="round-tab">
                                <i class="fas fa-hand-pointer"></i>
                            </span>
                        </a>
                        <span class="step-label active-color">Subscription Details</span>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2">
                            <span class="round-tab">
                                <i class="fas fa-credit-card"></i>
                            </span>
                        </a>
                        <span class="step-label">Payment</span>
                    </li>
                    
                    <li role="presentation" class="disabled">
                        <a href="#complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                        <span class="step-label">Email Confirmation</span>
                    </li>
                </ul>
            </div>

            <form id="myform" method="post" action="{{ route('subscription-payment') }}">
                @csrf
                <input id="subscription_plan_order_id" name="subscription_plan_order_id" type="hidden" value="{{$subscription_plan_order_id}}">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <fieldset>
                            <legend>Credit Card Details</legend>
                            <div class="colums">
                            <div class="item">
                                <label for="cardholder_name">Cardholder Name</label>
                                <input id="cardholder_name" type="text" name="cardholder_name" value="{{ old('cardholder_name') }}" required/>
                            </div>
                            <div class="item">
                                <label for="creditcard_number">Credit Card Number</label>
                                <input id="creditcard_number" type="text" name="creditcard_number" value="{{ old('creditcard_number') }}"  maxlength="19" required/>
                            </div>
                            <div class="item">
                                <label for="card_expiry_date">Expiry Date</label>
                                <input id="card_expiry_date" type="text" name="card_expiry_date" value="{{ old('card_expiry_date') }}" placeholder="MM/YYYY" minlength="7" maxlength="7" required/>
                            </div>
                            <div class="item">
                                <label for="cvv">CVV</label>
                                <input id="cvv" type="text" name="cvv" value="{{ old('cvv') }}" minlength="3" maxlength="4" required/>
                            </div>
                            </div>      
                        </fieldset>	
                        
                        <ul class="list-inline pull-right">
                            <li><button type="submit" class="btn next-step">Pay</button></li>
                        </ul>
                    </div>
                                       
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
          rules: {
            creditcard_number: {
              digits: true,
            },
            cvv: {
              digits: true,
            }
          } // rules
        });

        $('#card_expiry_date').MonthPicker({ Button: false }); // KidSysco-month-picker
        $("#card_expiry_date").keydown(false);
    });
</script>
@endsection
