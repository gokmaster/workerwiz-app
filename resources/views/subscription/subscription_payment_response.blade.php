@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/multistepform.css') }}?rand={{mt_rand()}}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="container">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
	<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line1 active-bgcolor"></div>
                <div class="connecting-line2 active-bgcolor"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1">
                            <span class="round-tab">
                                <i class="fas fa-hand-pointer"></i>
                            </span>
                        </a>
                        <span class="step-label active-color">Subscription Details</span>
                    </li>

                    <li role="presentation" class="active">
                        <a href="#step2">
                            <span class="round-tab">
                                <i class="fas fa-credit-card"></i>
                            </span>
                        </a>
                        <span class="step-label active-color">Payment</span>
                    </li>
                    
                    <li role="presentation" class="disabled">
                        <a href="#complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                        <span class="step-label">Email Confirmation</span>
                    </li>
                </ul>
            </div>

            <p style="padding:10px;">
                <h4>Payment Awaiting Confirmation</h4>
                Once your payment has been confirmed by one of our representatives, we will send you a confirmation email.
                This might take up to 1 business day.
            </p>
            <br><br>
        </div>
    </section>
   </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
          rules: {
            creditcard_number: {
              digits: true,
            },
            cvv: {
              digits: true,
            }
          } // rules
        });

        $('#card_expiry_date').MonthPicker({ Button: false }); // KidSysco-month-picker
        $("#card_expiry_date").keydown(false);
    });
</script>
@endsection
