@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/multistepform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="container">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <div style="text-align:center;">
        <div class="sub-upgrade-alertbox">
            @if ($expired == 0)
                This feature requires a Premium Subscription.<br>
                <b>UPGRADE SUBSCRIPTION</b> below
            @else
                Your Premium Subscription has expired.<br>
                <b>EXTEND SUBSCRIPTION</b> below
            @endif
        </div>
        <br>
        <i class="fa fa-arrow-down" style="font-size:10rem;color:#327d46;" aria-hidden="true"></i>
    </div>

	<div class="row">
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line1"></div>
                <div class="connecting-line2"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="disabled">
                        <a href="#step1">
                            <span class="round-tab">
                                <i class="fas fa-hand-pointer"></i>
                            </span>
                        </a>
                        <span class="step-label">Subscription Details</span>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2">
                            <span class="round-tab">
                                <i class="fas fa-credit-card"></i>
                            </span>
                        </a>
                        <span class="step-label">Payment</span>
                    </li>
                    
                    <li role="presentation" class="disabled">
                        <a href="#complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                        <span class="step-label">Email Confirmation</span>
                    </li>
                </ul>
            </div>

            <form id="myform" method="post" action="{{ route('subscription-order') }}">
                @csrf
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <p>
                            <div class="formsection">
                                <h4>Subscription Plan</h4>
                                <input id="subscription_plan_no" name="subscription_plan_no" type="hidden" value="5">
                                <div class="radiobutton-outerdiv">
                                    <div class="radiobutton-div selected" data-subscription_plan_no="5"></div>
                                    <div class="info-div">
                                        <div class="subscription-title">{{ ucfirst($plan5['title']) }}</div>
                                        <div class="price">${{$plan5['price']}}</div>
                                        <div class="price-info">/user per month</div>
                                    </div>
                                </div>
                            </div>
                            <div class="formsection">
                                <h4>Subscription Period</h4>
                                <label class="radio-inline"><input type="radio" name="subscription_period" value="3" required>3 months</label>
                                <label class="radio-inline"><input type="radio" name="subscription_period" value="6" checked>6 months</label>
                                <label class="radio-inline"><input type="radio" name="subscription_period" value="12">12 months</label>
                            </div>
                            <div class="formsection">
                                <h4>Number of Users</h4>
                                <input id="user_quantity" class="short-textbox" name="user_quantity" type="number" min="1" value="1" required>
                            </div>
                        </p>
                        <ul class="list-inline pull-right">
                            <li><button type="submit" class="btn next-step">Next</button></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
          rules: {
			user_quantity: {
				digits: true,
			}
          }
        });

        $(".radiobutton-div").click(function(e){
            var justClicked = e.target;
            $(".radiobutton-div").removeClass("selected");
            $(justClicked).addClass("selected");

            var planNo = $(justClicked).data("subscription_plan_no");
            $("#subscription_plan_no").val(planNo);
        });

    });
</script>
@endsection
