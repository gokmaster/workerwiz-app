@if (count($jobtitles))
    <table class="maintable">
        <thead>
        <tr>
            <th>
                Job Title
            </th>
            <th>
                Created on
            </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($jobtitles as $j)
            <tr id="row-{{$j['id']}}">
                <td>
                    {{ ucfirst($j['job_title']) }}
                </td>
                <td>
                    {{ date("d M, Y", strtotime($j['created_at'])) }}
                </td>
                <td>
                    <button type="button" class="btnEdit iconbutton"><i class="fa fa-edit" data-jobtitle-id="{{$j['id']}}"></i></button>
                    <button type="button" class="btnDelete iconbutton"><i class="fa fa-trash" data-jobtitle-id="{{$j['id']}}"></i></button>
                </td>
            </tr>
            <tr id="editable-row-{{$j['id']}}" style="display:none;">
                <td><input id="jobtitle-{{$j['id']}}" type="text" required value="{{ $j['job_title'] }}"/></td>
                <td>{{ date("d M, Y", strtotime($j['created_at'])) }}</td>
                <td><button type="button" class="btnSave smallbutton" data-jobtitle-id="{{$j['id']}}">Save</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif