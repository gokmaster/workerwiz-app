@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    @if(count($members))
        <h2 class="pageheading">Members of Team "{{ ucfirst($members[0]['team_name']) }}"</h2>
        <div class="table-container">
            <table class="display maintable" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Member Type</th>
                    </tr>
                </thead>
                <tbody>        
                    @foreach($members as $member)
                    <tr>
                        <td><a href="{{ route('user-profile', ['user_id' => $member['user_id'] ]) }}">{{ ucfirst($member['fname']) }} {{ ucfirst($member['lname']) }}</a></td>
                        <td>{{ ucfirst($member['member_type']) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({
                
        });
    });
</script>
@endsection
