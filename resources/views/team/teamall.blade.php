@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
<h2 class="pageheading">Teams</h2>
<div class="table-container">
    <table class="display maintable" style="width:100%">
        <thead>
            <tr>
                <th>Team Name</th>
                <th>Team Leader</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($teams as $team)
        <tr>
            <td><a href="{{ route('team', ['team_id' => $team['id']]) }}">{{ ucfirst($team['team_name']) }}</a></td>
            <td>{{ ucfirst($team['leader_fname']) }} {{ ucfirst($team['leader_lname']) }}</td>
            <?php $encodedTeamName = bin2hex($team['team_name']) ?>
            <td>
                <a href="{{ route('team-members-edit-form', ['team_id' => $team['id'], 'encoded_team_name' => $encodedTeamName]) }}" class="btnEdit iconbutton" data-toggle="tooltip" data-placement="left" title="Edit"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
<a class="buttonlink" href="{{ route('teamcreate-form') }}">Create Team</a>  
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({  
        });

        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
