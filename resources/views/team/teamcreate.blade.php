@extends('layouts.app')

@section('assets')

<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('team-create') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Create Team</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="team_name">Team Name<span>*</span></label>
            <input id="team_name" type="text" name="team_name" value="{{ old('team_name') }}" required/>
          </div>
          <div class="item">
            <label for="team_leader">Team Leader<span>*</span></label>
            <select class="form-control" id="team_leader" name="team_leader" required>
              <option disabled selected value> -- Select a user -- </option>
              @foreach($users as  $user)
              <option value="{{ $user['id'] }}"
                  @if ( strcmp($user['id'], old('team_leader') ) === 0 )
                      selected="selected"
                  @endif
              >{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</option>
              @endforeach
          </select>
          </div>
		</div>
    </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
	</div>
  </form>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
@endsection
