@extends('layouts.app')

@section('assets')

<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
<link rel="stylesheet" href="{{ asset('css/membersedit.css') }}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('team-members-edit') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Edit Members for Team "{{ $teamName }}"</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif

      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="colDiv">
              <h3 class="smallheading">Select Members from here<h3>
              @foreach ($users as $user)
                    <a id="btnEachUser-{{ $user['user_id'] }}" class="btnEachUser" href="#" data-user-id="{{$user['user_id']}}" 
                        data-user-name="{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}"
                        @if ($user['team_id'] == $teamId)
                           style='display:none;' 
                        @endif
                        > 
                        {{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}
                    </a>
              @endforeach
            </div>
          </div>
          <div class="col-sm-6">
            <div id="chosenMembersDiv" class="colDiv">
              <h3 class="smallheading">Members of "{{ $teamName }}"</h3>
              @foreach ($currentMembers as $cm)
                <div id="selected-member-{{ $cm['user_id'] }}"> 
                    <div class='selecteduser-name'>{{ ucfirst($cm['fname']) }} {{ ucfirst($cm['lname'])}}</div>
                    <select id="membertype-{{ $cm['user_id'] }}" class="membertype">
                        <option @if ($cm['member_type'] == 'normal') selected @endif value="normal">Normal</option>
                        <option @if ($cm['member_type'] == 'senior agent') selected @endif value="senior agent">Senior Agent</option>
                        <option @if ($cm['member_type'] == 'leader') selected @endif value="leader">Leader</option>
                    </select>
                    <a class="btnRemoveMember" href="#" data-user-id="{{$cm['user_id']}}">
                        <i class="fa fa-remove" data-user-id="{{$cm['user_id']}}"></i> 
                    </a>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      
      <input id="selected_members" name="selected_members" type="hidden" value=""/>
    
      <div class="btn-block">
        <button id="btnSave" type="button">Save</button>
      </div>
    </form>  
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate();

        $(document).on("click",".btnEachUser",function(e) {
            var buttonJustClicked = e.target;
            $(buttonJustClicked).hide();
            
            var userId = $(buttonJustClicked).data("user-id"); 
            var name = $(buttonJustClicked).data("user-name"); 
           
            var eachSelectedUserHTML = "<div id='selected-member-" +  userId + "'>" +
                "<div class='selecteduser-name'>" + name + "</div> " +
                "<select class='membertype' id='membertype-" + userId + "'>" +
                    "<option selected value='normal'>Normal</option>" +
                    "<option value='senior agent'>Senior Agent</option>" +
                    "<option value='leader'>Leader</option>" +
                "</select>" +
                " <a class='btnRemoveMember' href='#' data-user-id='" + userId + "'>" +
                    "<i class='fa fa-remove' data-user-id='" + userId + "'></i>" +
                "</a>" +
            "</div>";

            $('#chosenMembersDiv').append(eachSelectedUserHTML);
        });

        $(document).on("click","#btnSave",function(e) {
            var selectedMembers = [];
            var selectedMembersJson = "";
            var i = 0;
            var leaderCount = 0;

            $(".btnRemoveMember").each(function() {  
                var userId = $(this).data('user-id');

                var memberType = $('#membertype-'+ userId).val();

                if (memberType == 'leader') {
                    leaderCount++;
                }
                
                selectedMembers[i] = {
                    user_id: userId,
                    member_type: memberType,
                    team_id: {{ $teamId }}
                };

                selectedMembersJson = JSON.stringify(selectedMembers);

                i++;                
            }); // $(".btnRemoveMember").each

            if (leaderCount == 0) {
                alert('Please assign one of the members as leader');
            } else if (leaderCount > 1) {
                alert('A group can only have 1 leader.');
            } else {
                $('#selected_members').val(selectedMembersJson);
                $('#myform').submit();
            }
        });

        $(document).on("click",".btnRemoveMember",function(e) {
            var buttonJustClicked = e.target;
            var userId = $(buttonJustClicked).data("user-id"); 

            $('#selected-member-' + userId).remove();
            $('#btnEachUser-' + userId).show();
        });
    });
</script>
@endsection
