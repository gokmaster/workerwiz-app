@extends('layouts.app')

@section('assets')

@endsection

@section('content')
  <h2 class="pageheading">Assign KPIs to User</h2>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif 

  <div class="table-container">
    <table class="display maintable" style="width:100%">
        <thead>
            <tr>
                <th>User</th>
                <th></th>
        
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
          <td><a href="">{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</a></td>        
          <td>
              <a href="#" class="btnAssignKpi" data-user-id="{{ $user['user_id'] }}" 
                data-user-name="{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}">Assign KPI</a>
          </td>
        </tr>
        @endforeach
        </tbody>
    </table>
  </div>

  <!-- Modal -->
<div id="kpiAssignModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Assign KPI to "<span id="spnKpiGroupName"></span>"</h4>
          </div>
          <div id="kpiAssignModalBody" class="modal-body">
              
          </div>
          <div class="modal-footer">
              <div class="btn-block">
                  <button id="btnSaveKpiAssign" type="button">Save</button>
              </div>  
          </div>
      </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        //$("#myform").validate();

        $('.maintable').DataTable({
        });

        $(document).on("click",".btnAssignKpi", function(e){
            var buttonJustClicked = e.target;
            var userId = $(buttonJustClicked).data('user-id');
            var userName = $(buttonJustClicked).data('user-name');
        
            $('#spnKpiGroupName').html(userName);
        
            var data = new FormData();
            data.append("user_id", userId);
      
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('kpi-user-assign-html') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object
            
                    if (jsonObj.success == true) {
                        $('#kpiAssignModalBody').html(jsonObj.message);
                        $('#kpiAssignModal').modal('show');
                    } 

                    $( "#kpi_startdate" ).datepicker({
                        dateFormat: "dd-mm-yy",
                        onSelect: function(selected) {  
                            var startdate = selected.split("-").reverse().join("-"); 
                            startdate = new Date(startdate);
           
                            if(!isNaN(startdate.getTime())){
                                startdate.setDate(startdate.getDate()+4);
                         
                                $("#kpi_enddate").val($.datepicker.formatDate('dd-mm-yy', startdate));
                            }
                        } // end date should not be less than start date
                    });
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
        }); //  btnAssignKpi


        $(document).on("click","#btnSaveKpiAssign", function(e) {
            var kpiGroupId = $('#kpi_group_id').val();
            var kpiStartdate = $('#kpi_startdate').val();
            var userId = $('#user_id').val();

            if ($("#kpi_group_id option:selected").index() == 0) {
                swal("Failed", "Please select a KPI Group", "error");
            } else if (kpiStartdate == "" || kpiStartdate == null) {
                swal("Failed", "Please enter a Start Date", "error");
            } else {
                var data = new FormData();
                data.append("kpi_group_id", kpiGroupId);
                data.append("kpi_startdate", kpiStartdate);
                data.append("user_id", userId);
        
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "{{ route('kpi-user-assign') }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 6000000000,
                    success: function (response) {
                        var jsonObj = JSON.parse(response); //convert JSON string to JSON object
                        
                        if (jsonObj.success == true) {
                            $('#kpiAssignModal').modal('hide');
                            swal("Success", jsonObj.message, "success");
                        } else {
                            swal("Failed", jsonObj.message, "error");
                        }      
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                }); // $.ajax
            }
        }); //  btnSaveKpiAssign

    });
</script>
@endsection
