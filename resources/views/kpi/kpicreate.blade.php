@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
<h2 class="pageheading">Create KPIs</h2>

<a href="#" id="btnShowKpiGrp" class="btnTab btnTabSelected">KPI Groups</a>
<a href="#" id="btnShowKpi" class="btnTab">KPI</a>

<div id="kpiGrpContainer" class="table-container">
    <h3 class='tableheading'>KPI Groups</h3>
    <label for="kpigroup_descrip">Group Description: </label>
    <input id="kpigroup_descrip" name="kpigroup_descrip" class="shorttextbox" type="text" required/>
    <button id="btnAddGrp" type="button" class="smallbutton">Add</button>

    <div id="kpigroup-div">
      @include('kpi/partial/kpigroup_table')
    </div>
</div>

<div id="kpiDetailContainer" class="table-container container" style="display:none;">
    <h3 class='tableheading'>KPI</h3>
    
    <div class="inputrow">
      <label for="kpi_descrip">KPI Description: </label>
      <input id="kpi_descrip" name="kpi_descrip" class="longtextbox" type="text" required/>
    </div>
    <div class="inputrow">
      <label for="target">Target: </label>
      <input id="target" name="target" class="longtextbox" type="number" required/>
    </div>
    <div class="inputrow">
      <label for="target_type">Target Type: </label>
      <select class="longtextbox" id="target_type" name="target_type" required>
        <option disabled selected value> -- Select target type -- </option>
        <option value="%">%</option>
        <option value="$">$</option>
        <option value="value">value</option>
      </select>
    </div>
    <br/>
    <div class="btn-block">
      <button id="btnAddKpiDetail" type="button" class="smallbutton">Add</button>
    </div>

    <div id="kpidetail-div">
      @include('kpi/partial/kpidetail_table')
    </div>
</div>

<!-- Modal -->
<div id="kpiAssignModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Assign KPI to "<span id="spnKpiGroupName"></span>"</h4>
            </div>
            <div id="kpiAssignModalBody" class="modal-body">
                
            </div>
            <div class="modal-footer">
                <div class="btn-block">
                    <button id="btnSaveKpiAssign" type="button">Save</button>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#kpigrp-table').DataTable({});
        $('#kpidetail-table').DataTable({});

        $(document).on("click","#btnAddGrp", function(e){
            var kpigroup_descrip = $('#kpigroup_descrip').val();
        
            var data = new FormData();
            data.append("description", kpigroup_descrip);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('kpigroup-create') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                    if (jsonObj.success == true) {
                        $('#kpigroup-div').html(jsonObj.message);
                        $('#kpigrp-table').DataTable({});
                        swal("Success", "", "success");
                    } else {
                        swal("Failed", jsonObj.message, "error");
                    }      
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
        }); // btnAddGrp

        $(document).on("click","#btnAddKpiDetail", function(e){
            var kpiDescrip = $('#kpi_descrip').val();
            var target = $('#target').val();
            var targettype = $('#target_type').val();
        
            var data = new FormData();
            data.append("description", kpiDescrip);
            data.append("target", target);
            data.append("target_type", targettype);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('kpi-detail-create') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                    if (jsonObj.success == true) {
                        $('#kpidetail-div').html(jsonObj.message);
                        $('#kpidetail-table').DataTable({});
                        swal("Success", "", "success");
                    } else {
                        swal("Failed", jsonObj.message, "error");
                    }     
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
        }); // btnAddKpiDetail

        $(document).on("click","#btnShowKpiGrp", function(e){
            $('.table-container').hide();
            $('#kpiGrpContainer').show();

            $('.btnTab').removeClass('btnTabSelected');
            $('#btnShowKpiGrp').addClass('btnTabSelected');
        }); // btnShowKpiGrp

        $(document).on("click","#btnShowKpi", function(e){
            $('.table-container').hide();
            $('#kpiDetailContainer').show();

            $('.btnTab').removeClass('btnTabSelected');
            $('#btnShowKpi').addClass('btnTabSelected');
        }); // btnShowKpiGrp

        $(document).on("click",".btnAssignKpi", function(e){
            var buttonJustClicked = e.target;
            var kpiGrpId = $(buttonJustClicked).data('kpigroup-id');
            var kpiGrpName = $(buttonJustClicked).data('kpigroup-descrip');

            $('#spnKpiGroupName').html(kpiGrpName);
          
            var data = new FormData();
            data.append("kpi_group_id", kpiGrpId);
         
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('kpi-group-assign-html') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                    if (jsonObj.success == true) {
                        $('#kpiAssignModalBody').html(jsonObj.message);
                        $('#kpiAssignModal').modal('show');
                    } 
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
        }); //  btnAssignKpi

        $(document).on("click","#btnSaveKpiAssign", function(e){
            var kpiDetailIds = [];

            $.each($(".kpi_detail_ids:checked"), function(){
                kpiDetailIds.push($(this).val());
            });
            kpiDetailIds = JSON.stringify(kpiDetailIds);

            var kpiGrpId = $('#kpigroup-id').val();

            var data = new FormData();
            data.append("kpi_group_id", kpiGrpId);
            data.append("kpi_detail_ids", kpiDetailIds);
         
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('kpi-group-assign') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                    if (jsonObj.success == true) {
                        $('#kpiAssignModal').modal('hide');
                        swal("Success", jsonObj.message, "success");
                    } else {
                        swal("Failed", jsonObj.message, "error");
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
        }); //  btnAssignKpi
    });
</script>
@endsection
