@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" enctype="multipart/form-data">
    @csrf
    <h2 class="pageheading">User KPI Entry</h2>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

    <select class="form-control shortwidth-select" id="user" name="user">
        <option disabled selected value> -- Select a user -- </option>
        @foreach($users as  $user)
        <option value="{{ $user['user_id'] }}">{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</option>
        @endforeach
    </select>

    <b>No Users in Dropdown List?</b><br>
    <a href="{{route('teamcreate-form')}}">Create a team</a> and add yourself and other users you want to the team. The above dropdown list will only list members of your team.
    
    <div id="user-records"></div> 
</form>

<!-- Modal -->
<div id="addRepModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Rep</h4>
            </div>
            <div id="addRepModalBody" class="modal-body">
                <div class="item">
                    <label>Rep</label>
                    <select class="form-control" id="rep_id" name="rep_id" required>
                        <option disabled selected value> -- Select a rep -- </option>
                        @foreach($reps as  $rep)
                        <option value="{{ $rep['id'] }}">{{ ucfirst($rep['fname']) }} {{ ucfirst($rep['lname']) }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="item">
                    <label for="kpi_value">KPI Value</label>
                    <input id="kpi_value" type="number" name="kpi_value" required/>
                </div>

                <input type="hidden" id="selected_id" value=""/>

                <div id="selected_reps"></div>
            </div>
            <div class="modal-footer">
                <div class="btn-block">
                    <button id="btnAddRep" type="button">Add</button>
                </div>  
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    function selectedRepsHTML(repObj) {
        var html = "";
        $.each(repObj, function(key, r) {
            html += "<div class='each-selected-rep'>" + r.rep_name + ": " + r.kpi_value + 
                        "<button class='btnRemoveRep' href='#' data-rep-id='" + r.rep_id + "'>" +
                            " <i class='fa fa-remove' data-rep-id='" + r.rep_id + "'></i>" +
                        "</button>" +
                    "</div>"; 
        });
        return html;
    }

    $("#myform").validate({
    });

    $('#user').on('change', function() {
        $('#user-records').html("");
        var data = new FormData();
        data.append("user_id",this.value);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('user-kpi-entry-html') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                var jsonObj = JSON.parse(response); //convert JSON string to JSON object
                $('#user-records').html(jsonObj.message);
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    }); // #user

    $(document).on("click",".btnShowAddRepPopup", function(e) {
        var buttonJustClicked = e.target;
        var id = $(buttonJustClicked).data("id");
        var inputElement = document.getElementById('txt-result-' + id);
        var repData = inputElement.getAttribute('data-rep');

         // reset values
        $('#rep_id option:first').prop('selected',true);
        $('#kpi_value').val("");
        $('#selected_id').val(id);
        $('#selected_reps').html("");

        $('#addRepModal').modal('show'); 

        if (repData != null && repData != "" && repData != undefined) {
            repObj = JSON.parse(repData); 
            
            $('#selected_reps').html(selectedRepsHTML(repObj));
        }           
    }); //  btnAssignKpi

    $(document).on("click","#btnAddRep", function(e) {
        var row = {};
        row.rep_id = $('#rep_id').val();
        row.rep_name = $("#rep_id option:selected").text();
        row.kpi_value = $('#kpi_value').val();

        if (row.kpi_value == null || row.kpi_value == "") {
            swal("Failed", "Please enter a KPI value", "error");
        } else if ($("#rep_id option:selected").index() == 0) {
            swal("Failed", "Please select a rep", "error");
        } else {
            var id = $('#selected_id').val();
            var inputElement = document.getElementById('txt-result-' + id);
        
            var repData = inputElement.getAttribute('data-rep');
            var repObj = [];
        
            if (repData != null && repData != "" && repData != undefined) {
                repObj = JSON.parse(repData); 
            } 

            var rep_exist = 0;
            
            // if repObj is not empty
            if (repObj.length != 0) {
                $.each(repObj, function(key, r) {
                    if (r.rep_id == row.rep_id) {
                        rep_exist = 1; // this rep has already been added
                    }
                });
            }

            if (rep_exist == 0) {
                repObj.push(row);
            }

            // update kpi value in textbox
            var totalKpiValue = 0;
            $.each(repObj, function(key, r) {
                totalKpiValue = +totalKpiValue + +r.kpi_value
            });
            $('#txt-result-' + id).val(totalKpiValue);

            $('#selected_reps').html(selectedRepsHTML(repObj));
                    
            repData = JSON.stringify(repObj);
            
            inputElement.setAttribute('data-rep',  repData); 
        } // else
    }); //  btnAssignKpi

    $(document).on("click",".btnRemoveRep", function(e) {
        var buttonJustClicked = e.target;
        $(buttonJustClicked).attr("disabled", true);

        var repId = $(buttonJustClicked).data("repId");
        var id = $('#selected_id').val();
        var inputElement = document.getElementById('txt-result-' + id);
      
        var repData = inputElement.getAttribute('data-rep');
       
        var repObj = [];
    
        if (repData != null && repData != "" && repData != undefined) {
            repObj = JSON.parse(repData); 
        } 
        
        var i = 0;
        var totalKpiValue = 0;
        
         // update kpi value in textbox
        $.each(repObj, function(key, r) {
            if (r.rep_id == repId) {
                repObj.splice(i, 1); // remove item from array 
            } else {
                totalKpiValue = +totalKpiValue + +repObj[i].kpi_value;
            }
            i++;
        });
        $('#txt-result-' + id).val(totalKpiValue);

        $('#selected_reps').html(selectedRepsHTML(repObj)); 
        
        repData = JSON.stringify(repObj);
        
        inputElement.setAttribute('data-rep',  repData); 
    }); //  btnAssignKpi

    $(document).on("click","#btn-save", function(e) {
        var data = [];

        $('.txt-result').each(function(i, el) {
            var row = {};
            row.id = el.getAttribute('data-id');
            row.result = $(el).val();
            row.rep = el.getAttribute('data-rep');

            data.push(row);
        });
      
        var formdata = new FormData();
        formdata.append("datajson", JSON.stringify(data));
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('user-kpi-update') }}",
            data: formdata,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                var jsonObj = JSON.parse(response); //convert JSON string to JSON object
                if (jsonObj.success == true) {
                    $('#kpiAssignModal').modal('hide');
                    swal("Success", jsonObj.message, "success");
                } else {
                    swal("Failed", jsonObj.message, "error");
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    }); //  btnAssignKpi
}); // document.ready

</script>
@endsection
