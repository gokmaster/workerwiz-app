<table id="kpigrp-table" class="display maintable" style="width:100%">
    <thead>
        <tr>
        <th>Description</th>
        <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($kpigroups as $k)
    <tr>
    <td><a href="">{{ ucfirst($k['description']) }}</a></td>
    <td>
        <a class="btnAssignKpi" href="#" data-toggle="modal" data-target="kpiAssignModal" data-kpigroup-id="{{ $k['id'] }}" data-kpigroup-descrip="{{ $k['description'] }}">Assign KPI</a>
    </td>
    </tr>
    @endforeach
    </tbody>
</table>