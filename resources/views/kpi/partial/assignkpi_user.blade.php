
<label>KPI Group</label>
<select class="form-control" id="kpi_group_id" name="kpi_group_id" required>
    <option disabled selected value> -- Select KPI Group -- </option>
    @foreach ($kpigroups as $k)
    <option value="{{ $k['id'] }}">
        {{ ucfirst($k['description']) }}
    </option>
    @endforeach
</select>
<br>
<div class="inputrow">
    <label for="kpi_startdate">Start Date</label>
    <input id="kpi_startdate" class="longtextbox" type="text" name="kpi_startdate" autocomplete="off" required />
</div>

<div class="inputrow">
    <label for="kpi_enddate">End Date</label>
    <input id="kpi_enddate" class="longtextbox" type="text" name="kpi_enddate" readonly/>
</div>

<input id="user_id" name="user_id" type="hidden" value="{{ $userId }}"/>