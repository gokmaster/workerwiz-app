@foreach ($kpidetails as $k)
<label>
    <input class="kpi_detail_ids" type="checkbox" name="kpi_detail_ids[]" value="{{$k['id']}}" required
        @if (in_array($k['id'] , $groupKpis) )
            checked
        @endif
    >
    {{ ucfirst($k['description']) }}
</label><br/>
@endforeach
<input id="kpigroup-id" type="hidden" value="{{ $kpigroupId }}"/>