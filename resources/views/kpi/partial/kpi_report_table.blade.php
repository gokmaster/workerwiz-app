<table class="maintable">
    <thead>
        <tr>
            <th>User</th>
            @foreach($kpiDescripHeader as $descrip)
                <th>{{ ucfirst($descrip) }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
    @foreach($userKpis as $kpi)        
        <tr>
            <td>{{  ucfirst($kpi[0]['user_name']) }}</td>
            @foreach($kpi as $k)
                <td>
                    {{ $k['result'] }} 
                    @if ( $k['target_type'] != 'value')
                        ( {{ $k['target_type'] }} )
                    @endif

                    @if (!empty($k['rep']))
                        <a class="btnReps" href="#">Show Reps</a>
                        <ul class="repList" style="display:none;">
                            @foreach($k['rep'] as $repName=>$repKpiValue) 
                                <li>
                                    {{ $repName }} : {{ $repKpiValue }}
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>

