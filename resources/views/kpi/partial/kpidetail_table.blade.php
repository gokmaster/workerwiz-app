<table id="kpidetail-table" class="display maintable" style="width:100%">
    <thead>
        <tr>
        <th>Description</th>
        <th>Target</th>
        <th>Target Type</th>
        </tr>
    </thead>
    <tbody>
    @foreach($kpidetails as $k)
    <tr>
    <td>{{ ucfirst($k['description']) }}</td>
    <td>{{ ucfirst($k['target']) }}</td>
    <td>{{ ucfirst($k['target_type']) }}</td>
    </tr>
    @endforeach
    </tbody>
</table>