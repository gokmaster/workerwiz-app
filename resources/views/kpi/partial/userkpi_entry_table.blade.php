<table class="maintable">
    <thead>
        <tr>
            <th>KPIs</th>
            @foreach($dateHeader as $d)
                <th>{{ date("D d M", strtotime($d)) }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
    @foreach($userKpis as $kpi)        
        <tr>
            <td>{{ $kpi[0]['kpi_description'] }}</td>
            @foreach($kpi as $kpidate)
                <td>
                    <input id="txt-result-{{ $kpidate['id'] }}" class="txt-result" data-id="{{ $kpidate['id'] }}" data-rep="{{ $kpidate['rep'] }}" type='text' value="{{ $kpidate['result'] }}"/>
                    <a href="#" class="btnShowAddRepPopup" data-id="{{ $kpidate['id'] }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Rep</a>
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
<br>
<div class="btn-block">
    <button id="btn-save" class="largebutton" type="button">Save</button>
</div>

