@extends('layouts.app')

@section('assets')

@endsection

@section('content')
   
<h2 class="pageheading">KPI Report</h2>

<select class="shorttextbox" id="kpi_group_id" name="kpi_group_id">
    <option disabled selected value> -- Select a KPI Group -- </option>
    @foreach($kpiGroups as $k)
    <option value="{{ $k['id'] }}">{{ ucfirst($k['description']) }}</option>
    @endforeach
</select>
<input id="start_date" name="start_date" class="shorttextbox" type="text" placeholder="Start date" autocomplete="off" required/>
<input id="end_date" name="end_date" class="shorttextbox" type="text" placeholder="End date" autocomplete="off" required/> 
<button id="btnFetchKpiReport" type="button" class="smallbutton">Fetch</button>

<div id="kpi-report-div" class="table-container"></div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $( "#start_date" ).datepicker({
            dateFormat: "dd-mm-yy",
            onSelect: function(selected) {
                $("#end_date").datepicker("option","minDate", selected)
            } // end date should not be less than start date
        });
        
        $( "#end_date" ).datepicker({
            dateFormat: "dd-mm-yy",
            onSelect: function(selected) {
                $("#start_date").datepicker("option","maxDate", selected)
            } // start date should not be greater than end date
        });

        $(document).on("click","#btnFetchKpiReport", function(e){
            var kpiGrpId = $('#kpi_group_id').val();
            var startdate = $('#start_date').val();
            var enddate = $('#end_date').val();

            if (kpiGrpId == null || kpiGrpId == "") {
                swal("Failed", "Please select a KPI Group", "error");
            } else if (startdate == null || startdate == "") {
                swal("Failed", "Please enter a start date", "error");
            } else if (enddate == null || enddate == "") {
                swal("Failed", "Please enter a end date", "error");
            } else {
                var data = new FormData();
                data.append("kpi_group_id", kpiGrpId);
                data.append("start_date", startdate);
                data.append("end_date", enddate);
            
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "{{ route('kpi-report-html') }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 6000000000,
                    success: function (response) {
                        var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                        if (jsonObj.success == true) {
                            $('#kpi-report-div').html(jsonObj.message);
                            $('.maintable').DataTable();
                        } else {
                            swal("Failed", jsonObj.message, "error");
                        }
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                }); // $.ajax
            }
        }); //  btnFetchKpiReport

        $(document).on("click",".btnReps", function(e){
            $(this).parent().find('.repList').slideToggle();
        }); //
    });
</script>
@endsection
