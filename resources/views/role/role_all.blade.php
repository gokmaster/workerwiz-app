@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
<h2 class="pageheading">Departments</h2>
<div class="table-container">
    <table class="display maintable" style="width:100%">
        <thead>
            <tr>
                <th>Department Name</th>
                <th>Set Permissions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($roles as $role)
        <tr>
            <td>{{ ucfirst($role['role_name']) }}</td>        
            <td>
                <a href="{{ route('roleedit-form', ['role_id' => $role['id'] ]) }}" class="iconbutton" data-toggle="tooltip" data-placement="left" title="Edit Department Permissions"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
<a class="buttonlink" href="{{ route('rolecreate-form') }}">Create Department</a>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({     
        });

        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
