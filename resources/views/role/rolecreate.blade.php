@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('role-create') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Create Department</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
    <div class="item">
        <label for="role_name">Department Name<span>*</span></label>
        <input id="role_name" type="text" name="role_name" value="{{ old('role_name') }}" placeholder="E.g System admin" required/>
    </div>
    <h3>Select the permissions for this department</h3>
  
    @foreach($tasks as $label)
        <div class="box" style="padding:5px;">
            <h4>{{ strtoupper( $label[0]['label'] ) }}</h4>
            @foreach($label as $task)
                @if ($companyPrivilegeLevel >= $task['privilege_level'])
                    <?php
                        $input_id = str_replace(' ', '_', $task['task_name'] );
                    ?>
                    <label class="chkbox_container" >
                        <input id="{{$input_id}}" type="checkbox" name="tasks[]" value="{{$task['task_id']}}" required
                            @if(is_array( old('tasks') ) && in_array( $task['task_id'], old('tasks') )) 
                                checked 
                            @endif
                        >
                        &nbsp {{$task['task_name']}}
                        <span class="checkmark"></span>
                    </label>&nbsp &nbsp &nbsp
                @endif
            @endforeach
        </div>
    @endforeach

    <div class="btn-block">
      <button type="submit">Save</button>
    </div>  
</form>
                   
                

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#myform").validate();
        });
    </script>
@endsection
