<!DOCTYPE html>
<?php 
  $user = Session::get('user_details'); 
  $company = Session::get('company_details');
?>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <title>{{env('APP_NAME')}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- jQuery 3 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Bootstrap 3.3.7 -->
 <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}"> 
  <!-- Bootstrap 4.4.1 -->
   <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">-->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/skin-green.min.css') }}">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!--Datatables-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

  <!-- timepicker -->
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

  <!--KidSysco-month-picker -->
  <link rel="stylesheet" href="{{ asset('kidsysco-month-picker/src/monthpicker.css') }}">

  <link rel="stylesheet" href="{{ asset('css/main.css') }}?rand={{mt_rand()}}">

  @yield('assets')
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
   
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{ asset('images/icon/logo.png') }}" width="130px"/></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
    
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs">{{ ucfirst($user['fname']) }}</span>
                <img class="profile-img-icon" src="{{ asset('images/'. $user['profilepic']) }}"/>
              </a>
              <ul class="dropdown-menu" style="width:140px;">
                <!-- Menu Footer-->
                <li class="user-footer">
                  <a href="{{ route('user-profile', ['user_id' => $user['id'] ]) }}">
                    <div>{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</div>
                  </a>
                </li>
                <li class="user-footer">
                  <a href="{{ route('passwordchange-form') }}">
                    <div>Change Password</div>
                  </a>
                </li>
                <li class="user-footer">
                  <a href="{{ env('LOGIN_URL') }}/logout">
                    <div>Sign out</div>
                  </a>
                </li>
              </ul>
            </li><!--user account menu-->
        </ul>
      </div>
    </nav>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
                <li class="treeview">
                  <a href="#">
                    <span><b>{{ strtoupper($company['company_name']) }}</b></span>
                  </a>
                </li>
        @if ( isset($spData['menu'])  && $spData['menu'] !== null )
          @foreach ($spData['menu'] as $key=>$menuItem)
              @if (strcmp( $menuItem['has_submenu'], 0) === 0 )
                <!-- Has no sub-menu -->
                <li><a href="{{route( $menuItem['route_name'] )}}">
                  <span>{{ $menuItem['menu_label'] }}</span></a>
                </li>
              @else
                <!-- Has sub-menu -->
                <li class="treeview">
                    <a href="#">
                      <span>{{ $key }}</span>
                      <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      @foreach ($menuItem['submenu'] as $submenu)
                        <li><a href="{{ route($submenu['route_name']) }}">{{ $submenu['menu_label'] }}</a></li>
                      @endforeach
                    </ul>
                </li>
              @endif
          @endforeach
        @endif
                <li class="treeview">
                  <a href="#">
                    <span>Payroll <div class="premium-plan-icon">Premium</div></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('payslip-ofuser') }}">My Payslip</a></li>
                    <li><a href="{{ route('payslip-generate-form') }}">Payslip Generator</a></li>
                    <li><a href="{{ route('payslip-users-all') }}">All Users' Payslips</a></li>
                  </ul>
                </li>

                <li class="treeview">
                  <a href="#">
                    <span>Sale Tracker <div class="premium-plan-icon">Premium</div></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('sale-add-form') }}">Add Sale</a></li>
                    <li><a href="{{ route('sale-my') }}">My Sales</a></li>
                    <li><a href="{{ route('sale-all') }}">Sales</a></li>
                  </ul>
                </li>

                <li class="treeview">
                  <a href="#">
                    <span>Customer Accounts <div class="premium-plan-icon">Premium</div></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('customer-create-form') }}">Create Customer Account</a></li>
                    <li><a href="{{ route('customer-all') }}">Customers</a></li>
                  </ul>
                </li>

                <li class="treeview">
                  <a href="#">
                    <span>Email Campaign <div class="premium-plan-icon">Premium</div></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('emailcampaign-create-form') }}">Create Email Campaign</a></li>
                    <li><a href="{{ route('mailinglist-create-form') }}">Create Mailing List</a></li>
                    <li><a href="{{ route('emailcampaign-send-form') }}">Send Email Campaign</a></li>
                  </ul>
                </li>

                <li class="treeview">
                  <a href="#">
                    <span>Appointment <div class="premium-plan-icon">Premium</div></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('appointment-rep') }}">Live Diary</a></li>
                    <li><a href="{{ route('appointment-create-form') }}">Create</a></li>
                    <li><a href="{{ route('appointment-confirm-form') }}">Confirm</a></li>
                    <li><a href="{{ route('appointment-iced') }}">For Review</a></li>
                    <li><a href="{{ route('appointment-dropped') }}">Cancelled</a></li>
                    <li><a href="{{ route('appointment-archived') }}">Archived</a></li>
                    <li><a href="{{ route('appointment-block-timeslot-form') }}">Block Times</a></li>
                    <li><a href="{{ route('confirmers-add-form') }}">Add Confirmers</a></li>
                  </ul>
                </li>

                <li class="treeview">
                  <a href="#">
                    <span>KPI <div class="premium-plan-icon">Premium</div></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('kpi-create-form') }}">Create KPI</a></li>
                    <li><a href="{{ route('kpi-user-assign-form') }}">Assign KPI to User</a></li>
                    <li><a href="{{ route('user-kpi-entry-form') }}">Enter User KPI Results</a></li>
                    <li><a href="{{ route('kpi-report') }}">KPI Report</a></li>
                  </ul>
                </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       @yield('header')
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script> 

  <!-- Popper JS - Needed for Bootstrap 4 -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <!-- Bootstrap 4.4.1 -->
  <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>-->

  <!-- AdminLTE App -->
  <script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <!--jQuery Validation Plugin -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>

  <!--Scripts need for Datatables -->
  <script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script> 

  <!--Needed for Datatables Date Sorting-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.12/sorting/datetime-moment.js"></script>

  <!--Timepicker -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

  <!--KidSysco-month-picker -->
  <script src="{{ asset('kidsysco-month-picker/src/monthpicker.js') }}"></script>

  <!--Sweet Alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <!--Fontawesome 5-->
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>

  <script>
    // Prevent Backspace key from navigating back
    $(document).on("keydown", function (e) {
      if (e.which === 8 && !$(e.target).is("input:not([readonly]):not([type=radio]):not([type=checkbox]), textarea, [contentEditable], [contentEditable=true]")) {
          e.preventDefault();
      }
    });
  </script>

  @yield('scripts')
</body>
</html>