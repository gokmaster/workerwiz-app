@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('customer-edit') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Edit Customer</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Customer Details</legend>
        <div class="colums">
          <div class="item">
            <input id="customer_id" name="customer_id" type="hidden" value="{{ $customer['id'] }}"/>
            <label for="fname">First Name<span>*</span></label>
            <input id="fname" type="text" name="fname" value="{{ old('fname', $customer['fname']) }}" required/>
          </div>
          <div class="item">
            <label for="lname">Last Name<span>*</span></label>
            <input id="lname" type="text" name="lname" value="{{ old('lname', $customer['lname']) }}" required/>
          </div>
          <div class="item">
            <label for="phone">Phone</label>
            <input id="phone" type="text" name="phone" value="{{ old('phone', $customer['phone']) }}" maxlength="16"/>
          </div>
          <div class="item">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ old('email', $customer['email']) }}" />
          </div>
          <div class="item">
            <label for="preference">Preference</label>
            <input id="preference" type="text" name="preference" value="{{ old('preference', $customer['preference']) }}" />
          </div>
          <div class="item">
            <label for="price_comfort">Price Comfort</label>
            <input type="text" id="price_comfort" name="price_comfort" readonly>
            <div id="slider-range" style="width:98.5%;"></div>
          </div>
        </div>

        <h4>Physical Address</h4>
        <div class="colums">
          <div class="item">
            <label for="address1">Address Line 1</label>
            <input id="address1" type="text" name="address1" value="{{ old('address1', $customer['address1']) }}"/>
          </div>
          <div class="item">
            <label for="address2">Address Line 2</label>
            <input id="address2" type="text" name="address2" value="{{ old('address2', $customer['address2']) }}" />
          </div>
          <div class="item">
            <label for="city">City</label>
            <input id="city" type="text" name="city" value="{{ old('city', $customer['city']) }}"/>
          </div>
          <div class="item">
            <label for="state">State</label>
            <input id="state" type="text" name="state" value="{{ old('state', $customer['state']) }}" />
          </div>
          <div class="item">
            <label for="zipcode">Zip/Postal Code</label>
            <input id="zipcode" type="text" name="zipcode" value="{{ old('zipcode', $customer['zipcode']) }}" maxlength="12"/>
          </div>
          <div class="item">
            <label for="country">Country</label>
            <select class="form-control" id="country" name="country">
                <option disabled selected value> -- Select a country -- </option>
                @foreach($countries as  $country)
                <option value="{{ $country }}" @if ($country == $customer['country']) selected @endif>{{ ucfirst($country) }}</option>
                @endforeach
            </select>
          </div>
        </div>

        <h4>Postal Address</h4>
        <div class="colums">
        <div class="item">
            <label for="postal_address1">Address Line 1</label>
            <input id="postal_address1" type="text" name="postal_address1" value="{{ old('postal_address1', $customer['postal_address1']) }}"/>
          </div>
          <div class="item">
            <label for="postal_address2">Address Line 2</label>
            <input id="postal_address2" type="text" name="postal_address2" value="{{ old('postal_address2', $customer['postal_address2']) }}" />
          </div>
          <div class="item">
            <label for="postal_city">City</label>
            <input id="postal_city" type="text" name="postal_city" value="{{ old('postal_city', $customer['postal_city']) }}"/>
          </div>
          <div class="item">
            <label for="postal_state">State</label>
            <input id="postal_state" type="text" name="postal_state" value="{{ old('postal_state', $customer['postal_state']) }}" />
          </div>
          <div class="item">
            <label for="postal_zipcode">Zip/Postal Code</label>
            <input id="postal_zipcode" type="text" name="postal_zipcode" value="{{ old('postal_zipcode', $customer['postal_zipcode']) }}" maxlength="12"/>
          </div>
          <div class="item">
            <label for="postal_country">Country</label>
            <select class="form-control" id="postal_country" name="postal_country">
                <option disabled selected value> -- Select a country -- </option>
                @foreach($countries as  $country)
                <option value="{{ $country }}" @if ($country == $customer['country']) selected @endif>{{ ucfirst($country) }}</option>
                @endforeach
            </select>
          </div>
        </div>
      </fieldset>	
      <br>
      <fieldset>
        <legend>Credit Card Details</legend>
        <div class="colums">
          <div class="item">
            <label for="cardholder_name">Cardholder Name</label>
            <input id="cardholder_name" type="text" name="cardholder_name" value="{{ old('cardholder_name', $customer['cardholder_name']) }}"/>
          </div>
          <div class="item">
            <label for="creditcard_number">Credit Card Number</label>
            <input id="creditcard_number" type="text" name="creditcard_number" value="{{ old('creditcard_number', $customer['creditcard_number']) }}" maxlength="19"/>
          </div>
          <div class="item">
            <?php
              $cardExpiryDate = "";
              if ($customer['card_expiry_date'] != null) {
                $cardExpiryDate = date('m/Y',strtotime($customer['card_expiry_date']));
              }
            ?>
            <label for="card_expiry_date">Expiry Date</label>
            <input id="card_expiry_date" type="text" name="card_expiry_date" placeholder="MM/YYYY" value="{{ old('card_expiry_date', $cardExpiryDate) }}" minlength="7" maxlength="7" />
          </div>
          <div class="item">
            <label for="cvv">CVV</label>
            <input id="cvv" type="text" name="cvv" value="{{ old('cvv', $customer['cvv']) }}" minlength="3" maxlength="4"/>
          </div>
        </div>      
      </fieldset>		

      @if($customfields)
        @foreach($customfields as $sectionTitle=>$formSections)
        <br>
        <fieldset>
          <legend>{{$sectionTitle}}</legend>
          <div class="colums">
            @foreach($formSections as $field)
              <?php $inputName = $field['input_codename']; ?>
              <div class="item">
                <label for="{{$inputName}}">
                  {{ ucfirst($field['field_label'])}}
                  @if($field['required']==1)<span>*</span> @endif
                  <span class="field-info">{{$field['field_info']}}</span>
                </label>
                <?php
                  $inputVal = "";
                  if ($customfieldValues) {
                    foreach ($customfieldValues as $val) {
                      if ($inputName == $val['input_codename']) {
                        $inputVal = $val['input_value'];
                        break;
                      }
                    } //
                  }
                ?>
                <input id="{{$inputName}}" type="{{$field['input_type']}}" name="{{$inputName}}" value="{{ old($inputName, $inputVal) }}" @if($field['required']==1) required @endif/>
              </div>
            @endforeach
          </div>      
        </fieldset>	
        @endforeach
      @endif

      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
    </form>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
          rules: {
            phone: {
              digits: true,
            },
            creditcard_number: {
              digits: true,
            },
            cvv: {
              digits: true,
            }
          } // rules
        });

        $( function() {
          $("#slider-range").slider({
            range: true,
            min: 0,
            max: 1000,
            values: [{{$priceComfortMin}}, {{$priceComfortMax}}],
            slide: function( event, ui ) {
              $("#price_comfort").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
          });
          $("#price_comfort").val("$" + $("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1) );
        });

        $('#card_expiry_date').MonthPicker({ Button: false }); // KidSysco-month-picker
        $("#card_expiry_date").keydown(false);
    }); // document.ready
</script>
@endsection
