@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <h2 class="pageheading">Customers</h2>
    
    <div class="table-container">    
        <table class="display maintable" style="width:100%">
            <thead>
                <tr>    
                    <th>Action</th>
                    <th>Campaign</th>
                    <th>Customer Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Preference</th>
                    <th>Price Comfort</th>
                    <th>Physical Address</th>
                    <th>Postal Address</th>
                    @if (in_array("customer_view_creditcard_data", $viewerPermissions))
                        <th>Cardholder Name</th>
                        <th>Creditcard No.</th>
                        <th>Expiry Date</th>
                        <th>CVV</th>
                    @endif
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($customers as $cust)
                <tr>
                    <td>
                        <a href="{{ route('customer-edit-form', ['customer_id' => $cust['id'] ]) }}" class="btnEdit iconbutton" data-toggle="tooltip" data-placement="left" title="Edit"><i class="fa fa-edit"></i></a>
                    </td>
                    <td>{{ ucfirst($cust['campaign_name']) }}</td>
                    <td>{{ ucfirst($cust['fname']) }} {{ ucfirst($cust['lname']) }}</td>
                    <td>{{ $cust['phone'] }}</td>
                    <td>{{ $cust['email'] }}</td>
                    <td>{{ $cust['preference'] }}</td>
                    <td>{{ $cust['price_comfort'] }}</td>
                    <td>
                        {{ $cust['address1'] }},
                        @if ($cust['address2'] != null) {{ $cust['address2'] }}, @endif
                        {{ $cust['city'] }},
                        @if ($cust['state'] != null) {{ $cust['state'] }}, @endif
                        @if ($cust['zipcode'] != null) {{ $cust['zipcode'] }}, @endif
                        {{ $cust['country'] }}
                    </td>
                    <td>
                        {{ $cust['postal_address1'] }},
                        @if ($cust['postal_address2'] != null) {{ $cust['postal_address2'] }}, @endif
                        {{ $cust['postal_city'] }},
                        @if ($cust['postal_state'] != null) {{ $cust['postal_state'] }}, @endif
                        @if ($cust['postal_zipcode'] != null) {{ $cust['postal_zipcode'] }}, @endif
                        {{ $cust['postal_country'] }}
                    </td>
                    @if (in_array("customer_view_creditcard_data", $viewerPermissions))
                        <td>{{ $cust['cardholder_name'] }}</td>
                        <td>{{ $cust['creditcard_number'] }}</td>
                        <td> @if (isset($cust['card_expiry_date']))
                                {{ date('m/Y', strtotime($cust['card_expiry_date'])) }}
                            @endif
                        </td>
                        <td>{{ $cust['cvv'] }}</td>
                    @endif
                    <td><a href="{{route('customer-profile', ['customer_id'=>$cust['id']] ) }}">See more</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({   
        });

        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
