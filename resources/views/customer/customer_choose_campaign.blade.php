@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <h2 class="pageheading">Select Campaign to Add Customer For:</h2>

    @foreach ($campaigns as $c)
        <a href="{{route('customer-create-form')}}?campaign={{bin2hex($c['campaign_name'])}}&campaignid={{bin2hex($c['id'])}}" class="btn-campaign">{{$c['campaign_name']}}</a>
    @endforeach
    

@endsection

@section('scripts')
<script>
   
</script>
@endsection
