@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/profile.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

<div class="profile-wrapper">	  
	<div class="additional-block">
		<h2>
			Customer Details
		</h2>
		<div class="address-details">
			<div class="item">
				<div class="label">
					First Name:
				</div>
				<div class="value">
					{{$customer['fname']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Last Name:
				</div>
				<div class="value">
					{{$customer['lname']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Phone:
				</div>
				<div class="value">
					{{$customer['phone']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Email:
				</div>
				<div class="value">
					{{$customer['email']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Preference:
				</div>
				<div class="value">
					{{$customer['preference']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Price Comfort:
				</div>
				<div class="value">
					{{$customer['price_comfort']}}
				</div>
			</div>
		</div> <!--address-details-->

		<h4>Physical Address</h4>
		<div class="address-details">
			<div class="item">
				<div class="label">
					Address Line 1:
				</div>
				<div class="value">
					{{$customer['address1']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Address Line 2:
				</div>
				<div class="value">
					{{$customer['address2']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					City:
				</div>
				<div class="value">
					{{$customer['city']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					State:
				</div>
				<div class="value">
					{{$customer['state']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Zipcode:
				</div>
				<div class="value">
					{{$customer['zipcode']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Country:
				</div>
				<div class="value">
					{{$customer['country']}}
				</div>
			</div>
		</div> <!--address-details-->

		<h4>Postal Address</h4>
		<div class="address-details">
			<div class="item">
				<div class="label">
					Address Line 1:
				</div>
				<div class="value">
					{{$customer['postal_address1']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Address Line 2:
				</div>
				<div class="value">
					{{$customer['postal_address2']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					City:
				</div>
				<div class="value">
					{{$customer['postal_city']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					State:
				</div>
				<div class="value">
					{{$customer['postal_state']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Zipcode:
				</div>
				<div class="value">
					{{$customer['postal_zipcode']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Country:
				</div>
				<div class="value">
					{{$customer['postal_country']}}
				</div>
			</div>
		</div> <!--address-details-->
	</div> <!--additional-block-->
	
	@if (in_array("customer_view_creditcard_data", $viewerPermissions))
		<div class="additional-block">
			<h2>
				Credit Card Details
			</h2>
			<div class="address-details">
				<div class="item">
					<div class="label">
						Credit Card No.:
					</div>
					<div class="value">
						{{$customer['creditcard_number']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Cardholder Name:
					</div>
					<div class="value">
						{{$customer['cardholder_name']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Expiry Date:
					</div>
					<div class="value">
						@if ($customer['card_expiry_date'] != null)
							{{date("M/Y", strtotime($customer['card_expiry_date']))}}
						@endif
					</div>
				</div>
				<div class="item">
					<div class="label">
						CVV:
					</div>
					<div class="value">
						{{$customer['cvv']}}
					</div>
				</div>
			</div> <!--address-details-->
		</div> <!--additional-block-->
	@endif

	@if (isset($customer['customfields']))
		@foreach($customer['customfields'] as $sectionTitle=>$sections)
		<div class="additional-block">
			<h2>
				{{ucwords($sectionTitle)}}
			</h2>
			<div class="address-details">
				@foreach ($sections as $fieldLabel=>$field)
					<div class="item">
						<div class="label">
							{{ ucwords($fieldLabel) }}:
						</div>
						<div class="value">
							{{ $field }}
						</div>
					</div>
				@endforeach
			</div> <!--address-details-->
		</div> <!--additional-block-->
		@endforeach
	@endif
</div>
@endsection

@section('scripts')
<script>

</script>
@endsection
