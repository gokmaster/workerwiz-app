@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('mailinglist-create') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Create Mailing List</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Mailing List Details</legend>
        <div class="colums">
          <div class="item">
            <label for="title">Mailing List Title<span>*</span></label>
            <input id="title" type="text" name="title" value="{{ old('title') }}" required/>
          </div>
          <div class="item">
            <label for="file">CSV/Excel file of email addresses<span>*</span> 
              <span class="field-info"><a href="{{ asset('files/email_addresses.csv') }}">Download sample CSV</a></span>
            </label>
            <input type="file" name="file" id="file" placeholder="Upload" required/>
          </div>
        </div>      
      </fieldset>
      
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
    </form>

    <br>
    <p>
      <b>Note:</b><br>
      The system will try to filter out any invalid or non-existent email addresses. <br>
      Sometimes, it is unable to correctly detect whether an email address exist due to the restrictions placed by some email providers.
    </p>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
        });
    }); // document.ready
</script>
@endsection
