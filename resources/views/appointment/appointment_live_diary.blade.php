@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
<link rel="stylesheet" href="{{ asset('css/appointment.css') }}">
@endsection

@section('content')
  @csrf
  <h2 class="pageheading">Appointments Live Diary</h2>

  <div class="month-container">
    @foreach ($months as $month)
      <button class="
          @if ($month == $currentMonth)
            selected
          @endif 
          btn-month" data-month="{{$month}}">{{ $month }}
      </button>
    @endforeach
  </div>

  <div class="week-container">
    @foreach ($weeks as $wknum=>$week)
      <button class="btn-week {{$week['monMonth']}} {{$week['sunMonth']}}
        @if ($currentMonday == $week['monday'])
          selected
        @endif
      "
        @if ($week['monMonth'] != $currentMonth || $week['sunMonth'] != $currentMonth)
          style="display:none;"
        @endif
        data-weeknum="{{$wknum}}">
          {{ date("d/M", strtotime($week['monday'])) }} - {{ date("d/M", strtotime($week['sunday'])) }}
      </button>
    @endforeach
  </div>
  
  <div id="appt-table-container">
    @include('appointment.partial.appointment_rep_table')
  </div>

  <div class="key-container">
    <div class="key-div"><div class="key-color-box booked-appt"></div>Appointment Set</div>
    <div class="key-div"><div class="key-color-box confirmed-appt"></div>Confirmed Appointment</div>
    <div class="key-div"><div class="key-color-box signup-appt"></div>Signup</div>
    <div class="key-div"><div class="key-color-box first-appt"></div>First Appointment</div>
    <div class="key-div"><div class="key-color-box revisit-appt"></div>Revisit Lead</div>
    <div class="key-div"><div class="key-color-box blocked-timeslot"></div>Blocked</div>
  </div>

  <!-- Modal -->
<div id="apptEditModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div id="apptEditModalBody" class="modal-body">
              <label>Confirmer</label>
              <select class="form-control" id="confirmer_user_id" name="confirmer_user_id"
                @if ($userIsApprover == 0)
                  disabled
                @endif
              >
                <option disabled selected value> -- Select Confirmer -- </option>
                @foreach ($confirmers as $c)
                <option value="{{ $c['user_id'] }}">
                    {{ ucfirst($c['fname']) }} {{ ucfirst($c['lname']) }}
                </option>
                @endforeach
              </select>
              <br>
              <label for='approver_notes'>Approvers Notes</label>
              <input class="form-control" id="approver_notes" type="text" name="approver_notes"
                @if ($userIsApprover == 0)
                  disabled
                @endif
              />
              <input id="appt-id" type="hidden" value=""/>

              @if ($userIsApprover == 1)
                <div class="btn-block">
                    <button id="btnSaveApproverEdit" type="button">Save</button>
                </div>  
              @endif
              <hr>
              <div id="confirmer-notes-div"></div>
            </div>
            <div class="modal-footer">
              <a id="btnViewAppointment" class="buttonlink" target="_blank" href="">View Appointment</a>
            </div>
        </div>
    </div>
</div>

 <!-- Modal -->
<div id="blockedTimeSlotEditModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Unblock Appointment Timeslot</h4>
            </div>
            <div id="blockedTimeSlotEditModalBody" class="modal-body">
              <input id="blocked-appt-id" type="hidden" value=""/>
              Are you sure you want to unblock timeslot?
            </div>
            <div class="modal-footer">
              <button type="button" id="btnUnblockTimeslot">Unblock</button>
              <button type="button" id="btnCancelUnblockTimeslot" class="btn-negative">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var currentBtnDay;

    function confirmerNotesDivHtml(userIsApprover, apptdata) {
        var html = '<b>Confirmer Notes</b><br>' + apptdata.confirmer_notes;
        var htmlForApprover = '<b>Confirmer Notes</b><br>' +
        apptdata.confirmer_notes +
        '<br><br><div>' +
            '<button id="btnRevisit" class="btn-schedule btn-small">Schedule Revisit</button> <button id="btnSignup" class="btn-schedule btn-small">Schedule Signup</button>'+
        '</div>' +
        '<div id="schedule-div" style="display:none">' +
            '<input id="status" type="hidden">' +
            '<div class="item">' +
                '<label for="appointment_date">Appointment Date<span>*</span></label>'+
                '<input id="appointment_date" type="text" name="appointment_date" placeholder="dd-mm-yyyy" autocomplete="off" required />'+
            '</div>'+
            '<div class="item">' +
                '<label for="appointment_time">Appointment Time<span>*</span></label>' +
                '<input id="appointment_time" type="text" name="appointment_time" autocomplete="off" required />' +
            '</div>' +
            '<div class="btn-block">' +
                '<button id="btnSaveScheduleAppt" type="button">Save</button>' +
            '</div>'
        '</div>';

        if (userIsApprover == 1) {
          return htmlForApprover;
        }

        return html;
    }

    function refreshAppointmentsTable(){
      
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          enctype: 'multipart/form-data',
          url: "{{ route('appointment-rep-table-html') }}",
          processData: false,
          contentType: false,
          cache: false,
          timeout: 6000000000,
          success: function (response) {
            $('#appt-table-container').html(response);
          },
          error: function (e) {
              console.log("ERROR : ", e);
          },
          complete: function(){
            $('.btn-week.selected').click();
            $('#'+currentBtnDay).click();
            console.log("refreshed");
          }
      }); // $.ajax
  }

$(document).ready(function(){
    
    setInterval(function(){
      refreshAppointmentsTable();
    },30000);

    $(document).on("click",".btn-month", function(e) {
      var buttonJustClicked = e.target;
      var btnweekForMonth = $(buttonJustClicked).data("month"); // get month in order to get btn-week buttons that belong to that month

      $('.btn-week').hide();
      $('.'+ btnweekForMonth).show();

      $(".btn-month").removeClass("selected");
      $(buttonJustClicked).addClass("selected");

      setTimeout(function(){
        $('.'+ btnweekForMonth +":first").click();
      },1);
    });

    $(document).on("click",".btn-week", function(e) {
      var buttonJustClicked = e.target;
      var weeknum = $(buttonJustClicked).data("weeknum"); 

      $('.weekdiv').hide();
      $('#weekdiv-'+weeknum).show();

      $(".btn-week").removeClass("selected");
      $(buttonJustClicked).addClass("selected");
    });

    $(document).on("click",".btn-day", function(e) {
      $(".btn-day").removeClass("current");
      $(e.target).addClass('current');
      currentBtnDay = $(e.target).attr('id');
    });

    $(document).on("click",".btn-mon", function(e) {
      // show monday column
      $('.day-col').hide();
      $('tr td:nth-child(3), tr th:nth-child(3)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-mon").addClass("selected");
    });

    $(document).on("click",".btn-tue", function(e) {
       // show tuesday column
      $('.day-col').hide();
      $('tr td:nth-child(4), tr th:nth-child(4)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-tue").addClass("selected");
    });

    $(document).on("click",".btn-wed", function(e) {
       // show wednesday column
      $('.day-col').hide();
      $('tr td:nth-child(5), tr th:nth-child(5)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-wed").addClass("selected");
    });

    $(document).on("click",".btn-thu", function(e) {
       // show thursday column
      $('.day-col').hide();
      $('tr td:nth-child(6), tr th:nth-child(6)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-thu").addClass("selected");
    });

    $(document).on("click",".btn-fri", function(e) {
       // show fri column
      $('.day-col').hide();
      $('tr td:nth-child(7), tr th:nth-child(7)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-fri").addClass("selected");
    });

    $(document).on("click",".btn-sat", function(e) {
       // show fri column
      $('.day-col').hide();
      $('tr td:nth-child(8), tr th:nth-child(8)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-sat").addClass("selected");
    });

    $(document).on("click",".btn-sun", function(e) {
       // show fri column
      $('.day-col').hide();
      $('tr td:nth-child(9), tr th:nth-child(9)').show();

      $(".btn-day").removeClass("selected");
      $(".btn-sun").addClass("selected");
    });

    $(document).on("click",".edit-appt", function(e) {
        var justclicked = e.target;
        var apptdata = $(justclicked).data('apptdata');
        var btnViewApptLink = "{{ route('appointment-view', ['appt_id' => ''])}}/" + apptdata.id;
      
        $('#apptEditModal .modal-title').html("Appointment for " + apptdata.rep_fname + " " + apptdata.rep_lname);

        // set values
        $('#appt-id').val(apptdata.id);
        $('#approver_notes').val(apptdata.approver_notes);
        $("#btnViewAppointment").prop("href", btnViewApptLink);
        $('#confirmer_user_id option:selected').removeAttr('selected');
        $("#confirmer_user_id").prop('selectedIndex', 0);
        $("#confirmer-notes-div").html('');

        setTimeout(async function(){ 
          if (apptdata.confirmer_user_id !== null) {
           await $('#confirmer_user_id option[value='+apptdata.confirmer_user_id+']').prop('selected','selected');
          }
        }, 1);
        
        var userIsApprover = {{ $userIsApprover }};
        var html = confirmerNotesDivHtml(userIsApprover, apptdata);

        if (apptdata.status == 'confirmed') {
            $('#confirmer-notes-div').html(html);
        }

        $(function() {
            $("#appointment_date").datepicker({
                dateFormat: "dd-mm-yy",
                minDate: 0,
            });
        });
        
        $(function() {
            $('#appointment_time').timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                minTime: '7:00',
                maxTime: '7:00pm',
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        });

        $('#apptEditModal').modal('show');
    });

    $(document).on("click","#btnRevisit", function(e) {
      $(".btn-schedule").removeClass("selected");
      $("#btnRevisit").addClass("selected");
      $("#status").val('revisit');
      $("#schedule-div").show();
    });

    $(document).on("click","#btnSignup", function(e) {
      $(".btn-schedule").removeClass("selected");
      $("#btnSignup").addClass("selected");
      $("#status").val('signup');
      $("#schedule-div").show();
    });

    $(document).on("click","#btnSaveScheduleAppt", function(e) {
      var apptdate = $('#appointment_date').val();
      var appttime = $('#appointment_time').val();
      
      var data = new FormData();
      data.append("id", $('#appt-id').val());
      data.append("appointment_date", apptdate);
      data.append("appointment_time", appttime);
      data.append("status", $('#status').val());
      
      if (apptdate == null || apptdate == "") {
        swal("Failed", "Please enter appointment date", "error");
      } else if (appttime == null || appttime == "") {
        swal("Failed", "Please enter appointment time", "error");
      } else {
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          enctype: 'multipart/form-data',
          url:  "@if ($userIsApprover == 1) {{ route('appointment-reschedule') }} @endif",
          data: data,
          processData: false,
          contentType: false,
          cache: false,
          timeout: 6000000000,
          success: function (response) {
              var jsonObj = JSON.parse(response); //convert JSON string to JSON object

              if (jsonObj.success == true) {
                refreshAppointmentsTable();
                swal("Success", jsonObj.message, "success");
              } else {
                swal("Failed", jsonObj.message, "error");
              }
          },
          error: function (e) {
              console.log("ERROR : ", e);
          }
        }); // $.ajax
      }
    });

    $(document).on("click","#btnSaveApproverEdit", function(e){
      var confirmerId = $('#confirmer_user_id').val();
      var data = new FormData();
      data.append("id", $('#appt-id').val());
      data.append("confirmer_user_id", confirmerId);
      data.append("approver_notes", $('#approver_notes').val());
      
      if (confirmerId == null || confirmerId == "") {
        swal("Failed", "Please select a confirmer", "error");
      } else {
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          enctype: 'multipart/form-data',
          url: "@if ($userIsApprover == 1) {{ route('appointment-edit-json') }} @endif",
          data: data,
          processData: false,
          contentType: false,
          cache: false,
          timeout: 6000000000,
          success: function (response) {
              var jsonObj = JSON.parse(response); //convert JSON string to JSON object

              if (jsonObj.success == true) {
                refreshAppointmentsTable();
                swal("Success", "Successfully assigned confirmer", "success");
              } else {
                swal("Failed", jsonObj.message, "error");
              }
          },
          error: function (e) {
              console.log("ERROR : ", e);
          }
        }); // $.ajax
      }
    }); 

    $(document).on("click",".edit-blocked-slot", function(e) {
      var justclicked = e.target;
      var apptdata = $(justclicked).data('apptdata');

      $('#blocked-appt-id').val(apptdata.id);

      $('#blockedTimeSlotEditModal').modal('show');
    });

    $(document).on("click","#btnCancelUnblockTimeslot", function(e) {
      $('#blockedTimeSlotEditModal').modal('hide');
    });

    $(document).on("click","#btnUnblockTimeslot", function(e){
      var data = new FormData();
      data.append("id", $('#blocked-appt-id').val());
           
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "@if ($userIsApprover == 1) {{ route('appointment-blocktimeslot-remove') }} @endif",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
            var jsonObj = JSON.parse(response); //convert JSON string to JSON object

            if (jsonObj.success == true) {
              $('#blockedTimeSlotEditModal').modal('hide');
              refreshAppointmentsTable();
              swal("Success", jsonObj.message, "success");
            } else {
              swal("Failed", jsonObj.message, "error");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
      }); // $.ajax
    }); 

});
</script>
@endsection
