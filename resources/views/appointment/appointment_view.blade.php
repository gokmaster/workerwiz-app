@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform">
      @csrf
      <h2 class="pageheading">Appointment</h2>
        
      <div style="text-align:right;">
      @if($appt['archived'] == 0)
        <a class="buttonlink" href="{{ route('appointment-edit-form', ['appt_id' => $appt['id']])}}">Edit Appointment</a>
        @if ($userIsApprover == 1)
          @if ($appt['status'] != 'iced')
            <a id="btnShowIceModal" class="buttonlink" href="#">Ice Appointment</a>
          @endif
          <a id="btnShowArchiveModal" class="buttonlink" href="#">Archive Appointment</a>
          <a id="btnShowLeadPdfModal" class="buttonlink" href="#">Send Lead-PDF</a>
        @endif
      @endif
      </div>
      <fieldset>
        <legend>Appointment Details</legend>
        <input id="id" type="hidden" name="id" value="{{ $appt['id'] }}"/>
        <div class="colums">
          <div class="item">
            <label for="appointment_date">Appointment Date</label>
            <input id="appointment_date" type="text" name="appointment_date" value="{{ old('appointment_date', date('d-m-Y', strtotime($appt['appointment_date']))) }}" disabled/>
          </div>
          <div class="item">
            <label for="appointment_time">Appointment Time</label>
            <input id="appointment_time" type="text" name="appointment_time" value="{{ old('appointment_time', date('g:i A', strtotime($appt['appointment_time']))) }}" autocomplete="off" disabled />
          </div>
          <div class="item">
            <label for="rep_id">Solar Consultant</label>
            <select class="form-control" id="rep_id" name="rep_id" disabled>
              <option disabled selected value> -- Select a Solar Consultant -- </option> 
              @foreach($reps as  $rep)
              <option value="{{ $rep['id'] }}"
                @if ($rep['id'] == $appt['rep_id'])
                  selected
                @endif
                >{{ ucfirst($rep['fname']) }} {{ ucfirst($rep['lname']) }}</option>
              @endforeach      
            </select>
          </div>
        </div>
      </fieldset>
      <fieldset>
        <legend>Client Details</legend>
        <div class="colums">
          <div class="item">
            <label for="business_name">Business Name</label>
            <input id="business_name" type="text" name="business_name" value="{{ old('business_name', $appt['business_name']) }}" disabled/>
          </div>
          <div class="item">
            <label for="client_type">Client Type</label>
            <select class="form-control" id="client_type" name="client_type" disabled>
                <option value="business"
                  @if ($appt['country'] == 'business')
                    selected
                  @endif
                >
                  Business
                </option>
                <option value="residential"
                  @if ($appt['country'] == 'residential')
                    selected
                  @endif
                >
                  Residential
                </option>           
            </select>
          </div>
          <div class="item">
            <label for="phone">Phone Number</label>
            <input id="phone" type="tel" name="phone" value="{{ old('phone', $appt['phone']) }}" disabled/>
          </div>
          <div class="item">
            <label for="mobile">Mobile Number</label>
            <input id="mobile" type="tel" name="mobile" value="{{ old('mobile', $appt['mobile']) }}" disabled/>
          </div>
          <div class="item">
            <label for="street_address">Street Address</label>
            <input id="street_address" type="text" name="street_address" value="{{ old('street_address', $appt['street_address']) }}" disabled/>
          </div>
          <div class="item">
            <label for="city">City</label>
            <input id="city" type="text" name="city" value="{{ old('city', $appt['city']) }}" disabled/>
          </div>
          <div class="item">
            <label for="zipcode">Postal/Zip Code </label>
            <input id="zipcode" type="text" name="zipcode" value="{{ old('zipcode', $appt['zipcode']) }}" disabled/>
          </div>
          <div class="item">
            <label for="country">Country</label>
            <select class="form-control" id="country" name="country" disabled>
              <option disabled selected value> -- Select a country -- </option>
              <option value="Australia"
                @if ($appt['country'] == 'Australia')
                  selected
                @endif
              >
                Australia
              </option>
              <option value="New Zealand"
                @if ($appt['country'] == 'New Zealand')
                  selected
                @endif
              >
                New Zealand
              </option>           
            </select>
          </div>
          <div class="item">
            <label for="fname">First Name</label>
            <input id="fname" type="text" name="fname" value="{{ old('fname', $appt['fname']) }}" required disabled/>
          </div>
          <div class="item">
            <label for="lname">Last Name</label>
            <input id="lname" type="text" name="lname" value="{{ old('lname', $appt['lname']) }}" required disabled/>
          </div>
          <div class="item">
            <label for="bill_amount">Bill Amount</label>
            <input id="bill_amount" type="number" name="bill_amount" value="{{ old('bill_amount', $appt['bill_amount']) }}" disabled/>
          </div>
          <div class="item">
            <label for="email">Email</label>
            <input type="email" name="email" value="{{ old('email', $appt['email']) }}" disabled/>
          </div>
        </div>
      </fieldset>
      <fieldset>
        <legend>Agent Details</legend>
        <div class="colums">
          <div class="item">
            <label for="agent_notes">Agent Notes</label>
            <input id="agent_notes" type="text" name="agent_notes" value="{{ old('agent_notes', $appt['agent_notes']) }}" disabled/>
          </div>
          <div class="item">
            <label>Agent</label>
            <input type="text" value="{{ ucfirst($appt['agent_fname'])}} {{ ucfirst($appt['agent_lname'])}}" disabled />
          </div>
        </div>
      </fieldset>
</form> 

  <!-- Modal -->
  <div id="apptIceModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Ice Appointment</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to put this appointment on ice?
            <br><br>
            <label for='ice_reason'>Reason for iceing appointment</label>
              <input class="form-control" id="ice_reason" type="text" name="ice_reason"/>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnIceAppointment">Ice</button>
            <button type="button" id="btnCancelIceAppointment" class="btn-negative">Cancel</button>
          </div>
      </div>
  </div>
</div>

  <!-- Modal -->
<div id="apptArchiveModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Archive Appointment</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to put this appointment into archive?
          </div>
          <div class="modal-footer">
            <button type="button" id="btnArchiveAppointment">Archive</button>
            <button type="button" id="btnCancelArchiveAppointment" class="btn-negative">Cancel</button>
          </div>
      </div>
  </div>
</div>

  <!-- Modal -->
<div id="leadPdfModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Send Lead-PDF</h4>
          </div>
          <div class="modal-body">
            <div id="email-to-div">
              <span id="email-to-label">To: </span>
              @foreach ($reps_to_email as $rep)
                <div class='email-address-tag'>
                  <span class='email-text'>{{$rep['email']}}</span>
                  <button type='button' class='close btnRemoveEmail' >&times;</button>
                </div>
              @endforeach
            </div>
            <input id="email" class="shorttextbox" type="email" placeholder="Email"/>
            <button type="button" id="btnAddEmail" class="smallbutton">Add</button>
            <div id="send-status" style="display:none;">Sending email. Please wait...</div>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSendLeadPdf">Send</button>
            <button type="button" id="btnCancelLeadPdfModal" class="btn-negative">Cancel</button>
          </div>
      </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  function emailAddressHtml(email) {
    html = "<div class='email-address-tag'><span class='email-text'>"+email+
          "</span><button type='button' class='close btnRemoveEmail' >&times;</button></div>";
    return html;
  }

  $(document).ready(function(){
    
    $(document).on("click","#btnShowArchiveModal", function(e) {
      $('#apptArchiveModal').modal('show');
    });

    $(document).on("click","#btnCancelArchiveAppointment", function(e) {
      $('#apptArchiveModal').modal('hide');
    });

    $(document).on("click","#btnShowIceModal", function(e) {
      $('#apptIceModal').modal('show');
    });

    $(document).on("click","#btnCancelIceAppointment", function(e) {
      $('#apptIceModal').modal('hide');
    });

    $(document).on("click","#btnShowLeadPdfModal", function(e) {
      $('#leadPdfModal').modal('show');
    });

    $(document).on("click","#btnCancelLeadPdfModal", function(e) {
      $('#leadPdfModal').modal('hide');
    });

    $(document).on("click","#btnArchiveAppointment", function(e){
      var data = new FormData();
      data.append("id", $('#id').val());
      data.append("archived", 1);
           
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('appointment-edit-json') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
            var jsonObj = JSON.parse(response); //convert JSON string to JSON object

            if (jsonObj.success == true) {
              swal("Success", "Successfully archived appointment", "success");

              setTimeout(function(){
                window.location.href = "{{ route('appointment-rep') }}"; 
              },3000);
              
            } else {
              swal("Failed", jsonObj.message, "error");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
      }); // $.ajax
    }); 

    $(document).on("click","#btnIceAppointment", function(e){
      var data = new FormData();
      data.append("id", $('#id').val());
      data.append("status", 'iced');
      data.append("ice_reason", $('#ice_reason').val());      
           
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('appointment-edit-json') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
            var jsonObj = JSON.parse(response); //convert JSON string to JSON object

            if (jsonObj.success == true) {
              $('#apptIceModal').modal('hide');
              swal("Success", "Successfully iced appointment", "success");              
            } else {
              swal("Failed", jsonObj.message, "error");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
      }); // $.ajax
    }); 

    $(document).on("click","#btnSendLeadPdf", function(e){
      // if email exists
      if ($(".email-text")[0]){
        $("#send-status").show();

        var emails = [];
        $.each($(".email-text"), function(){
          emails.push($(this).text());
        });
        emails = JSON.stringify(emails);

        var data = new FormData();
        data.append("id", $('#id').val());
        data.append("emails", emails);
                
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          enctype: 'multipart/form-data',
          url: "@if ($userIsApprover == 1) {{route('appointment-lead-pdf-send')}} @endif",
          data: data,
          processData: false,
          contentType: false,
          cache: false,
          timeout: 6000000000,
          success: function (response) {
              var jsonObj = JSON.parse(response); //convert JSON string to JSON object
              if (jsonObj.success == true) {
                $("#send-status").hide();
                $('#leadPdfModal').modal('hide');
                swal("Success", jsonObj.message, "success");              
              } else {
                swal("Failed", jsonObj.message, "error");
              }
          },
          error: function (e) {
              console.log("ERROR : ", e);
          }
        }); // $.ajax
      } else {
        swal("Failed", "Please add at least 1 recipient email address", "error");
      }
    });

    $(document).on("click","#btnAddEmail", function(e) {
      var emailTagHtml = emailAddressHtml($('#email').val());
      $('#email-to-div').append(emailTagHtml);
      $('#email').val('');
    });

    $(document).on("click",".btnRemoveEmail", function(e) {
      $(this).parent().remove();
    });
  });
</script>
@endsection
