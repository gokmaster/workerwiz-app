@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
@endsection

@section('content')
@csrf
<h2 class="pageheading">Dropped Appointments</h2>

<div id="appointment-table-container" class="table-container">
  @include('appointment.partial.appointment_dropped_table')
</div>

 <!-- Confirm Modal -->
<div id="apptRescheduleModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Reschedule Appointment</h4>
          </div>
          <div id="apptConfirmBody" class="modal-body">
            <input id="appt-id" type="hidden" value=""/>
            <div class="item">
              <label for="appointment_date">Appointment Date<span class="required">*</span></label>
              <input id="appointment_date" class="form-control" type="text" name="appointment_date" autocomplete="off"/>
            </div>
            <br>
            <div class="item">
              <label for="appointment_time">Appointment Time<span class="required">*</span></label>
              <input id="appointment_time" class="form-control" type="text" name="appointment_time" autocomplete="off"/>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnRescheduleAppointment">Reschedule</button>
            <button type="button" id="btnCancelRescheduleAppointment" class="btn-negative">Cancel</button>
          </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $('.maintable').DataTable({});

    $(function() {
        $("#appointment_date").datepicker({
            dateFormat: "dd-mm-yy",
            minDate: 0,
        });
    });
    
    $(function() {
        $('#appointment_time').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '7:00',
            maxTime: '7:00pm',
            startTime: '00:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    });

    $(document).on("click",".btnReschedule", function(e) {
      var buttonJustClicked = e.target;
      var apptId = $(buttonJustClicked).data("appt-id"); 
      $('#appt-id').val(apptId);
      $("#appointment_date").val('');
      $("#appointment_time").val('');
      $("#apptRescheduleModal").modal('show');
    });

    $(document).on("click","#btnCancelRescheduleAppointment", function(e) {
      $("#apptRescheduleModal").modal('hide');
    });

    $(document).on("click","#btnRescheduleAppointment", function(e) {
      var apptdate = $('#appointment_date').val();
      var appttime = $('#appointment_time').val();
      
      var data = new FormData();
      data.append("id", $('#appt-id').val());
      data.append("appointment_date", apptdate);
      data.append("appointment_time", appttime);
      data.append("status", 'booked');
      
      if (apptdate == null || apptdate == "") {
        swal("Failed", "Please enter appointment date", "error");
      } else if (appttime == null || appttime == "") {
        swal("Failed", "Please enter appointment time", "error");
      } else {
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: "POST",
          enctype: 'multipart/form-data',
          url: "{{route('appointment-reschedule')}}",
          data: data,
          processData: false,
          contentType: false,
          cache: false,
          timeout: 6000000000,
          success: function (response) {
              var jsonObj = JSON.parse(response); //convert JSON string to JSON object

              if (jsonObj.success == true) {
                swal("Success", jsonObj.message, "success");
              } else {
                swal("Failed", jsonObj.message, "error");
              }
          },
          error: function (e) {
              console.log("ERROR : ", e);
          }
        }); // $.ajax
      }
    });
  });
</script>
@endsection
