@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('appointment-block-timeslot') }}" enctype="multipart/form-data">
      @csrf
      <h2 class="pageheading">Block Appointment Timeslot</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="rep_id">Solar Consultant<span>*</span></label>
            <select class="form-control" id="rep_id" name="rep_id" required>
                <option disabled selected value> -- Select a Solar Consultant -- </option> 
                @foreach($reps as  $rep)
                <option value="{{ $rep['id'] }}">{{ ucfirst($rep['fname']) }} {{ ucfirst($rep['lname']) }}</option>
                @endforeach      
            </select>
          </div>
          <div class="item">
            <label for="appointment_date">Appointment Date<span>*</span></label>
            <input id="appointment_date" type="text" name="appointment_date" value="{{ old('appointment_date') }}" placeholder="dd-mm-yyyy" autocomplete="off" required />
          </div>
          <div class="item">
            <label for="start_time">Start Time<span>*</span></label>
            <input id="start_time" type="text" name="start_time" value="{{ old('start_time') }}" autocomplete="off" required onkeypress="return false;"/>
          </div>
          <div class="item">
            <label for="end_time">End Time<span>*</span></label>
            <input id="end_time" type="text" name="end_time" value="{{ old('end_time') }}" autocomplete="off" required onkeypress="return false;"/>
          </div>
        </div>
      </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
      <br>
</form>    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
      $("#myform").validate();

      $( function() {
        $("#appointment_date").datepicker({
              dateFormat: "dd-mm-yy",
              minDate: 0,
        });
      });

      $('#start_time').timepicker({
          timeFormat: 'h:mm p',
          interval: 60,
          minTime: '7:00',
          maxTime: '7:00pm',
          startTime: '00:00',
          dynamic: false,
          dropdown: true,
          scrollbar: true
      });

      $('#end_time').timepicker({
          timeFormat: 'h:mm p',
          interval: 60,
          minTime: '7:00',
          maxTime: '7:00pm',
          startTime: '00:00',
          dynamic: false,
          dropdown: true,
          scrollbar: true
      });
  });
</script>
@endsection
