@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('appointment-edit') }}" enctype="multipart/form-data">
      @csrf
      <h2 class="pageheading">Edit Appointment</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Appointment Details</legend>
        <input id="id" type="hidden" name="id" value="{{ $appt['id'] }}"/>
        <div class="colums">
          <div class="item">
            <label for="appointment_date">Appointment Date<span>*</span></label>
            <input id="appointment_date" type="text" name="appointment_date" value="{{ old('appointment_date', date('d-m-Y', strtotime($appt['appointment_date']))) }}" placeholder="dd-mm-yyyy" autocomplete="off" required />
          </div>
          <div class="item">
            <label for="appointment_time">Appointment Time<span>*</span></label>
            <input id="appointment_time" type="text" name="appointment_time" value="{{ old('appointment_time', date('g:i A', strtotime($appt['appointment_time']))) }}" autocomplete="off" required />
          </div>
          <div class="item">
            <label for="rep_id">Consultant/Rep<span>*</span>
              <span class="field-info"><a href="{{ route('rep-create-form') }}">Add Consultant/Rep</a></span>
            </label>
            <select class="form-control" id="rep_id" name="rep_id" required>
              <option disabled selected value> -- Select a Consultant -- </option> 
              @foreach($reps as  $rep)
              <option value="{{ $rep['id'] }}"
                @if ($rep['id'] == $appt['rep_id'])
                  selected
                @endif
                >{{ ucfirst($rep['fname']) }} {{ ucfirst($rep['lname']) }}</option>
              @endforeach      
            </select>
          </div>
        </div>
      </fieldset>
      <fieldset>
        <legend>Client Details</legend>
        <div class="colums">
          <div class="item">
            <label for="business_name">Business Name</label>
            <input id="business_name" type="text" name="business_name" value="{{ old('business_name', $appt['business_name']) }}"/>
          </div>
          <div class="item">
            <label for="client_type">Client Type</label>
            <select class="form-control" id="client_type" name="client_type" required>
                <option value="business"
                  @if ($appt['country'] == 'business')
                    selected
                  @endif
                >
                  Business
                </option>
                <option value="residential"
                  @if ($appt['country'] == 'residential')
                    selected
                  @endif
                >
                  Residential
                </option>           
            </select>
          </div>
          <div class="item">
            <label for="phone">Phone Number<span>*</span></label>
            <input id="phone" type="tel" name="phone" value="{{ old('phone', $appt['phone']) }}" required/>
          </div>
          <div class="item">
            <label for="mobile">Mobile Number</label>
            <input id="mobile" type="tel" name="mobile" value="{{ old('mobile', $appt['mobile']) }}" />
          </div>
          <div class="item">
            <label for="street_address">Street Address</label>
            <input id="street_address" type="text" name="street_address" value="{{ old('street_address', $appt['street_address']) }}"/>
          </div>
          <div class="item">
            <label for="city">City</label>
            <input id="city" type="text" name="city" value="{{ old('city', $appt['city']) }}" />
          </div>
          <div class="item">
            <label for="zipcode">Postal/Zip Code </label>
            <input id="zipcode" type="text" name="zipcode" value="{{ old('zipcode', $appt['zipcode']) }}" />
          </div>
          <div class="item">
            <label for="country">Country</label>
            <select class="form-control" id="country" name="country">
              <option disabled selected value> -- Select a country -- </option>
              <option value="Australia"
                @if ($appt['country'] == 'Australia')
                  selected
                @endif
              >
                Australia
              </option>
              <option value="New Zealand"
                @if ($appt['country'] == 'New Zealand')
                  selected
                @endif
              >
                New Zealand
              </option>           
            </select>
          </div>
          <div class="item">
            <label for="fname">First Name<span>*</span></label>
            <input id="fname" type="text" name="fname" value="{{ old('fname', $appt['fname']) }}" required />
          </div>
          <div class="item">
            <label for="lname">Last Name<span>*</span></label>
            <input id="lname" type="text" name="lname" value="{{ old('lname', $appt['lname']) }}" required />
          </div>
          <div class="item">
            <label for="bill_amount">Bill Amount</label>
            <input id="bill_amount" type="number" name="bill_amount" value="{{ old('bill_amount', $appt['bill_amount']) }}" />
          </div>
          <div class="item">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ old('email', $appt['email']) }}" />
          </div>
        </div>
      </fieldset>
      <fieldset>
        <legend>Agent Details</legend>
        <div class="colums">
          <div class="item">
            <label for="agent_notes">Agent Notes</label>
            <input id="agent_notes" type="text" name="agent_notes" value="{{ old('agent_notes', $appt['agent_notes']) }}" />
          </div>
          <div class="item">
            <label>Agent</label>
            <input type="text" value="{{ ucfirst($appt['agent_fname'])}} {{ ucfirst($appt['agent_lname'])}}" disabled />
          </div>
        </div>
      </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
      <br>
</form>    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
      $("#myform").validate();

      $( function() {
        $("#appointment_date").datepicker({
            dateFormat: "dd-mm-yy",
            minDate: 0,
        });
      });

      $('#appointment_time').timepicker({
          timeFormat: 'h:mm p',
          interval: 30,
          minTime: '7:00',
          maxTime: '7:00pm',
          startTime: '00:00',
          dynamic: false,
          dropdown: true,
          scrollbar: true
      });
  });
</script>
@endsection
