@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
@endsection

@section('content')
@csrf
<h2 class="pageheading">Confirm Appointments</h2>

<div id="appointment-table-container" class="table-container">
  @include('appointment.partial.appointment_confirm_table')
</div>

 <!-- Confirm Modal -->
 <div id="apptConfirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm Appointment</h4>
            </div>
            <div id="apptConfirmBody" class="modal-body">
              Are you sure you want to confirm this appointment?
              <br><br>
              <input id="confirm-appt-id" type="hidden" value=""/>
              <label for='confirmer_notes'>Confirmer Notes</label>
              <input class="form-control" id="confirmer_notes" type="text" name="confirmer_notes"/>
            </div>
            <div class="modal-footer">
              <button type="button" id="btnConfirmAppointment">Confirm</button>
              <button type="button" id="btnCancelConfirmAppointment" class="btn-negative">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Drop Modal -->
<div id="apptDropModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Drop Appointment</h4>
            </div>
            <div id="apptDropBody" class="modal-body">
              Are you sure you want to drop this appointment?
              <br><br>
              <input id="drop-appt-id" type="hidden" value=""/>
              <label for='drop_reason'>Reason for dropping appointment</label>
              <input class="form-control" id="drop_reason" type="text" name="drop_reason"/>
            </div>
            <div class="modal-footer">
              <button type="button" id="btnDropAppointment">Drop</button>
              <button type="button" id="btnCancelDropAppointment" class="btn-negative">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $('.maintable').DataTable({});

    $(document).on("click",".btnShowConfirmModal", function(e) {
      var buttonJustClicked = e.target;
      var apptId = $(buttonJustClicked).data("appt-id"); 
      $('#confirm-appt-id').val(apptId);
      $('#apptConfirmModal').modal('show');
    });

    $(document).on("click",".btnShowDropModal", function(e) {
      var buttonJustClicked = e.target;
      var apptId = $(buttonJustClicked).data("appt-id"); 
      $('#drop-appt-id').val(apptId);
      $('#apptDropModal').modal('show');
    });

    $(document).on("click","#btnCancelConfirmAppointment", function(e) {
      $('#apptConfirmModal').modal('hide');
    });

    $(document).on("click","#btnCancelDropAppointment", function(e) {
      $('#apptDropModal').modal('hide');
    });

    $(document).on("click","#btnConfirmAppointment", function(e) {
      var apptId = $('#confirm-appt-id').val();
      var confirmerNotes = $('#confirmer_notes').val();

      var data = new FormData();
      data.append("id", apptId);
      data.append("confirmer_notes", confirmerNotes);
           
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('appointment-confirm') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
            var jsonObj = JSON.parse(response); //convert JSON string to JSON object

            if (jsonObj.success == true) {
              $('#apptConfirmModal').modal('hide');
              swal("Success", jsonObj.message, "success");

              $('#appointment-table-container').html(jsonObj.html);
              $('.maintable').DataTable({});
            } else {
              swal("Failed", jsonObj.message, "error");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
      }); // $.ajax
    });

    $(document).on("click","#btnDropAppointment", function(e) {
      var apptId = $('#drop-appt-id').val();
      var dropReason = $('#drop_reason').val();

      var data = new FormData();
      data.append("id", apptId);
      data.append("drop_reason", dropReason);
           
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('appointment-drop') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
            var jsonObj = JSON.parse(response); //convert JSON string to JSON object

            if (jsonObj.success == true) {
              $('#apptDropModal').modal('hide');
              swal("Success", jsonObj.message, "success");

              $('#appointment-table-container').html(jsonObj.html);
              $('.maintable').DataTable({});
            } else {
              swal("Failed", jsonObj.message, "error");
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
      }); // $.ajax
    });
  });
</script>
@endsection
