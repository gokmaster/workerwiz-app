@foreach ($weeks as $wknum=>$week)
  <div id="weekdiv-{{$wknum}}" class="weekdiv table-container" 
    @if ($currentMonday != $week['monday'])
      style="display:none;"
    @endif
  >
    <div class="day-container">
      <button id="btn-{{$wknum}}-mon" class="btn-mon btn-day selected">{{ date("D d/M", strtotime($week['monday'])) }}</button>
      <button id="btn-{{$wknum}}-tue" class="btn-tue btn-day">{{ date("D d/M", strtotime($week['tuesday'])) }}</button>
      <button id="btn-{{$wknum}}-wed" class="btn-wed btn-day">{{ date("D d/M", strtotime($week['wednesday'])) }}</button>
      <button id="btn-{{$wknum}}-thu" class="btn-thu btn-day">{{ date("D d/M", strtotime($week['thursday'])) }}</button>
      <button id="btn-{{$wknum}}-fri" class="btn-fri btn-day">{{ date("D d/M", strtotime($week['friday'])) }}</button>
      <button id="btn-{{$wknum}}-sat" class="btn-sat btn-day">{{ date("D d/M", strtotime($week['saturday'])) }}</button>
      <button id="btn-{{$wknum}}-sun" class="btn-sun btn-day">{{ date("D d/M", strtotime($week['sunday'])) }}</button>
    </div>
    <table class="maintable">
      <thead>
        <tr>
          <th>Rep</th>
          <th>Home</th>
          <th class="day-col">{{ date("D d/M", strtotime($week['monday'])) }}</th>
          <th class="day-col" style="display:none;">{{ date("D d/M", strtotime($week['tuesday'])) }}</th>
          <th class="day-col" style="display:none;">{{ date("D d/M", strtotime($week['wednesday'])) }}</th>
          <th class="day-col" style="display:none;">{{ date("D d/M", strtotime($week['thursday'])) }}</th>
          <th class="day-col" style="display:none;">{{ date("D d/M", strtotime($week['friday'])) }}</th>
          <th class="day-col" style="display:none;">{{ date("D d/M", strtotime($week['saturday'])) }}</th>
          <th class="day-col" style="display:none;">{{ date("D d/M", strtotime($week['sunday'])) }}</th>
        </tr>
      </thead>
      <tbody>
        @if (array_key_exists("appointments", $week))
            @foreach ($week['appointments'] as $repkey=>$reps)
                <?php $rep = explode("->", $repkey);?>
                <tr>
                    <td>{{ ucfirst($rep[0]) }}</td><!-- Rep Name -->
                    <td>{{ $rep[1] }}</td><!-- Rep Home zipcode -->
                    @foreach ($reps as $daykey=>$day)
                      <td class="day-col"
                        @if ($daykey !== 'mon')
                          style="display:none;"
                        @endif
                      >
                        <div class="day-outerdiv">
                            <div class="day-div">
                            @foreach ($day as $appt)
                                <div class="appt-div
                                  @if(isset($appt['status']))
                                    @if ($appt['status'] != 'blocked')
                                      edit-appt
                                    @else
                                      @if ($userIsApprover == 1)
                                        edit-blocked-slot
                                      @endif
                                    @endif
                                  @endif" data-apptdata="{{json_encode($appt)}}">
                                  <div class="appt-innerdiv 
                                    @if(isset($appt['status']))
                                      @if($appt['status'] == 'confirmed')
                                        confirmed-appt
                                      @elseif($appt['first_for_day'] == 1)
                                        first-appt
                                      @elseif($appt['status'] == 'booked')
                                        booked-appt
                                      @elseif($appt['status'] == 'revisit')
                                        revisit-appt
                                      @elseif($appt['status'] == 'signup')
                                        signup-appt
                                      @elseif($appt['status'] == 'blocked')
                                        blocked-timeslot
                                      @endif
                                    @endif
                                  " data-apptdata="{{json_encode($appt)}}">
                                    {{ date("H:i", strtotime($appt['appointment_time'])) }}
                                  </div>
                                  <div class="appt-innerdiv" data-apptdata="{{json_encode($appt)}}">
                                    @if(isset($appt['zipcode'])) 
                                      {{ $appt['zipcode'] }}
                                    @endif
                                  </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                      </td>
                    @endforeach
                </tr>
            @endforeach
        @endif
      </tbody>
    </table>
  </div>
@endforeach