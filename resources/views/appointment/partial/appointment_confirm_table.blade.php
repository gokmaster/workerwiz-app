<table class="display maintable" style="width:100%">
  <thead>
      <tr>
          <th>Appointment Date</th>
          <th>Rep</th>
          <th>Contact Name</th>
          <th>Business Name</th>
          <th>Approver's Notes</th>
          <th>Status</th>
          <th>Confirmer</th>
          <th>Confirmer's Notes</th>
          <th></th>
      </tr>
  </thead>
  <tbody>
  @foreach($appointments as $appt)
  <tr>
      <td><a href="{{route('appointment-view',['appt_id'=>$appt['id']])}}" target="_blank">{{ date('Y-m-d', strtotime($appt['appointment_date'])) }} {{ date('g:i A', strtotime($appt['appointment_time'])) }}</a></td>
      <td>{{ strtoupper($appt['rep_fname']) }} {{ strtoupper($appt['rep_lname']) }}</td>
      <td>{{ strtoupper($appt['fname']) }} {{ strtoupper($appt['lname']) }}</td>
      <td>{{ strtoupper($appt['business_name']) }}</td>
      <td>{{ $appt['approver_notes'] }}</td>
      <td>{{ $appt['status'] }}</td>
      <td>{{ ucfirst($appt['confirmer_fname']) }} {{ ucfirst($appt['confirmer_lname']) }}</td>
      <td>{{ $appt['confirmer_notes'] }}</td>
      <td>
        <div class="tablebutton-div">
          @if ($appt['status'] != 'confirmed' && $appt['status'] != 'dropped')
            <button class="btnShowConfirmModal" data-appt-id="{{$appt['id']}}">Confirm</button>
          @endif
        </div>
        <div class="tablebutton-div">
          @if ($appt['status'] != 'dropped')
            <button class="btnShowDropModal btn-negative" data-appt-id="{{$appt['id']}}">Drop</button>
          @endif
        </div>
      </td>
  </tr>
  @endforeach
  </tbody>
</table>
