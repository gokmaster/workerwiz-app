<table class="display maintable" style="width:100%">
  <thead>
      <tr>
          <th>Appointment Date</th>
          <th>Rep</th>
          <th>Business Name</th>
          <th>Post Code</th>
          <th>Contact Name</th>
          <th>Contact Phone</th>
          <th>Comments</th>
          <th></th>
      </tr>
  </thead>
  <tbody>
  @foreach($appointments as $appt)
  <tr>
    <td><a href="{{route('appointment-view',['appt_id'=>$appt['id']])}}" target="_blank">{{ date('Y-m-d', strtotime($appt['appointment_date'])) }} {{ date('g:i A', strtotime($appt['appointment_time'])) }}</a></td>
    <td>{{ strtoupper($appt['rep_fname']) }} {{ strtoupper($appt['rep_lname']) }}</td>
    <td>{{ strtoupper($appt['business_name']) }}</td>
    <td>{{ $appt['zipcode'] }}</td>
    <td>{{ strtoupper($appt['fname']) }} {{ strtoupper($appt['lname']) }}</td>
    <td>{{ $appt['phone'] }}</td>
    <td>{{ $appt['drop_reason'] }}</td>
    <td>
      @if ($appt['reschedule_appointment_id'] == null)
        <div class="tablebutton-div">
          <button class="btnReschedule" data-appt-id="{{$appt['id']}}">Reschedule</button>
        </div>
      @endif
    </td>
  </tr>
  @endforeach
  </tbody>
</table>
