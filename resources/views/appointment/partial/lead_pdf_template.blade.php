<html>
<head>
  <style>
    h4 {
      background-color: #bdd2d9;
      margin-top: 0;
      margin-bottom: 3px;
      padding: 2px 5px;
    }

    .wrapper {
      width: 100%;
      margin: 0 auto;
    }

    #header-div {
      width: 100%;
      margin-bottom: 50px;
      position: relative;
    }

    #business-contact-div {
      position: absolute;
      right: 45px;
      width: 25%;
    }
    
    .logo {
      width: 270px;
    }

    .left-div, .right-div {
      width: 49%;
	    float: left;
    }

    .right-div {
      margin-left: 2%;
    }

    .box {
      border-radius: 5px;
      border: solid 1px #99b4bd;
      min-width: 15px;
      padding: 2px 3px;
    }

    div.line {
      padding: 2px;
      font-size: 0.9rem;
      border-bottom: solid 1px #c8d1e0;
      margin-bottom: 6px;
    }

    .line .label {
      font-weight: bold;
      width: 34%;
      display: inline-block;
      padding-left: 3px;
    }

    .line .value {
      display: inline-block;
    }
  </style>
</head>

<body>
  <div class="wrapper">
      <div id="header-div">
        <div id="business-contact-div">
         
        </div>
      </div>

      <div class="section">
        <div class="left-div">
          <div class="line">
            <div class="label">Salesperson</div>
            <div class="value">{{$rep_fname}} {{$rep_lname}}</div>
          </div>
          <div class="line">
            <div class="label">Project Manager</div>
            <div class="value">{{$projectmanager}}</div>
          </div>
          <div class="box">
            <h4>Site Details</h4>
            <div class="line">
              <div class="label">Name</div>
              <div class="value">{{ucfirst($business_name)}}</div>
            </div>
            <div class="line">
              <div class="label">Address</div>
              <div class="value">{{ ucfirst($street_address)}}, <br> {{ ucfirst($city)}} {{ $zipcode}}</div>
            </div>
            <div class="line">
              <div class="label">Contact</div>
              <div class="value"></div>
            </div>
            <div class="line">
              <div class="label">Telephone</div>
              <div class="value"></div>
            </div>
            <div class="line">
              <div class="label">Fax</div>
              <div class="value"></div>
            </div>
            <div class="line">
              <div class="label">Email</div>
              <div class="value"></div>
            </div>
          </div>
        </div>
        <div class="right-div">
          <div class="line">
            <div class="label">Date Created</div>
            <div class="value">{{ date("d-m-Y")}}</div>
          </div>
          <div class="line">
            <div class="label">Appointment Date</div>
            <div class="value">{{ date("d-m-Y", strtotime($appointment_date))}} @ {{ date("g:i A", strtotime($appointment_time))}}</div>
          </div>
          <div class="box">
            <h4>Customer Details</h4>
            <div class="line">
              <div class="label">Name</div>
              <div class="value">{{$fname}} {{$lname}}</div>
            </div>
            <div class="line">
              <div class="label">Address</div>
              <div class="value">{{ ucfirst($street_address)}}, <br> {{ ucfirst($city)}} {{ $zipcode}}</div>
            </div>
            <div class="line">
              <div class="label">Contact</div>
              <div class="value">{{$mobile}}</div>
            </div>
            <div class="line">
              <div class="label">Telephone</div>
              <div class="value">{{$phone}}</div>
            </div>
            <div class="line">
              <div class="label">Fax</div>
              <div class="value"></div>
            </div>
            <div class="line">
              <div class="label">Email</div>
              <div class="value">{{ $email }}</div>
            </div>
          </div>
        </div>
		<div style="clear:both;"></div>
      </div>
      <br><br>

      <div>
        <div class="box">
          <h4>Notes</h4>
          Agent Notes: {{ $agent_notes}} <br><br>
          Confirmer Notes: {{ $confirmer_notes}} 
        </div>
      </div>
  </div>
</body>
</html>