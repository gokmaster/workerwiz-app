@extends('layouts.app')

@section('assets')
<!-- Datetime picker -->
<link rel="stylesheet" type="text/css" href="{{ asset('datetimepicker/css/jquery.datetimepicker.min.css') }}"/>
<script src="{{ asset('datetimepicker/js/jquery.datetimepicker.js') }}"></script>

<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" enctype="multipart/form-data">
    @csrf
<h2 class="pageheading">Approve Timesheet</h2>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

    <select class="form-control shortwidth-select" id="user" name="user">
        <option disabled selected value> -- Select a user -- </option>
        @foreach($users as  $user)
        <option value="{{ $user['user_id'] }}">{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</option>
        @endforeach
    </select>
    <b>No Users in Dropdown List?</b><br>
    <a href="{{route('teamcreate-form')}}">Create a team</a> and add yourself and other users you want to the team. The above dropdown list will only list members of your team.
    <div id="user-records"></div> 
</form>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
    $("#myform").validate({
    });

    $('#user').on('change', function() {
        var data = new FormData();
        data.append("user_id",this.value);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('timesheet-userrecords') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                var jsonObj = JSON.parse(response); //convert JSON string to JSON object
                $('#user-records').html(jsonObj);

                $('.checkin').datetimepicker({
                    format:'Y-m-d H:i:s',
                    timepicker: true,
                    //minDate:'-1970/01/02', //yesterday is minimum date
                    maxDate:'+1970/01/01', //tomorrow is maximum date
                    allowTimes: allowedtimes
                });

                $('.checkout').datetimepicker({
                    format:'Y-m-d H:i:s',
                    timepicker: true,
                    //minDate:'-1970/01/02', //yesterday is minimum date
                    maxDate:'+1970/01/01', //tomorrow is maximum date
                    allowTimes: allowedtimes
                });  
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    });

    var allowedtimes = [
        '00:00', '00:30',
        '1:00', '1:30', '2:00', '2:30',
        '3:00', '3:30', '4:00', '4:30',
        '5:00', '5:30', '6:00', '6:30',
        '7:00', '7:30', '8:00', '8:30',
        '9:00', '9:30', '10:00', '10:30',
        '11:00', '11:30', '12:00', '12:30',
        '13:00', '13:30', '14:00', '14:30',
        '15:00', '15:30', '16:00', '16:30',
        '17:00', '17:30', '18:00', '18:30',
        '19:00', '19:30', '20:00', '21:30',
        '22:00', '22:30', '23:00', '23:30'
    ]; 

    $(document).on("click",".btnEdit", function(e){
        var buttonJustClicked = e.target;
        var timesheetId = $(buttonJustClicked).data("timesheet-id"); 
        
        $('#row-'+ timesheetId).hide();
        $('#editable-row-'+ timesheetId).show();
    });

    $(document).on("click",".btnSave", function(e){
        var buttonJustClicked = e.target;
        var timesheetId = $(buttonJustClicked).data("timesheet-id"); 
        var userId = $('.user-id').val();
        var checkin = $('#checkin-' + timesheetId).val();
        var checkout = $('#checkout-' + timesheetId).val();
        
        $('#row-'+ timesheetId).hide();
        $('#editable-row-'+ timesheetId).show();

        var data = new FormData();
        data.append("user_id",userId);
        data.append("checkin",checkin);
        data.append("checkout",checkout);
        data.append("id",timesheetId);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('timesheet-edit') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                if (jsonObj.success == true) {
                    $('#user-records').html(jsonObj.message);
                } else {
                    swal("Failed", jsonObj.message, "error");
                }
                
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    });   

    $(document).on("click","#btnApprove", function(e){
        var userId = $('.user-id').val();
        var timesheetIds = $('#timesheet-ids').val();
       
        var data = new FormData();
        data.append("user_id",userId);
        data.append("timesheet-ids", timesheetIds);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('timesheet-approve-all') }}",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                if (jsonObj.success == true) {
                    $('#user-records').html("");
                    $('#user-records').html(jsonObj.message);
                } else {
                    swal("Failed", jsonObj.message, "error");
                }
                console.log(response);
                
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    }); 
}); // document.ready
</script>
@endsection
