@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

<table class="maintable">
    <thead>
        <tr>
            <th>Type</th>
            <th>Checkin</th>
            <th>Checkout</th>
            <th>Approved</th>
            <th width="30px"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($timesheet as $t)
        <tr id="row-{{$t['id']}}">
            <td>{{ ucfirst($t['type']) }}</td>
            <td>
                @if ($t['checkin'] != null)
                    {{ $tz::utcToSpecificTimezone($t['checkin'], $timezone, "D d M, Y - H:i") }}
                @endif
            </td>
            <td>
                @if ($t['checkout'] != null)
                    {{ $tz::utcToSpecificTimezone($t['checkout'], $timezone, "D d M, Y - H:i") }}
                @endif
            </td>
            <td>
                @if ($t['approved'] == 1)
                <i class="fa fa-check tickicon"></i>
                @endif
            </td>
            <td><button type="button" class="btnEdit iconbutton"><i class="fa fa-edit" data-timesheet-id="{{$t['id']}}"></i></button></td>
        </tr>
        
        <tr id="editable-row-{{$t['id']}}" style="display:none;">
            <td>{{ ucfirst($t['type']) }}
                <input type="hidden" class="user-id" value="{{ $t['user_id'] }}"/>
            </td>
            <td>
                <input id="checkin-{{$t['id']}}" class="checkin" type="text" required 
                    value="@if ($t['checkin'] != null){{ $tz::utcToSpecificTimezone($t['checkin'], $timezone, 'Y-m-d H:i:s') }}@endif"/>
            </td>
            <td>
                <input id="checkout-{{$t['id']}}" class="checkout" type="text" required 
                    value="@if ($t['checkout'] != null){{ $tz::utcToSpecificTimezone($t['checkout'], $timezone, 'Y-m-d H:i:s') }}@endif"/>
            </td>
            <td>
                @if ($t['approved'] == 1)
                <i class="fa fa-check tickicon"></i>
                @endif
            </td>
            <td><button type="button" class="btnSave smallbutton" data-timesheet-id="{{$t['id']}}">Save</button></td>
        </tr>
    @endforeach
    </tbody>
</table>
<input id="timesheet-ids" name="timesheet-ids" type="hidden" value="{{ $timesheetIds }}"/>

<div class="btn-block">
    <button type="button" id="btnApprove">Approve All</button>
</div>
