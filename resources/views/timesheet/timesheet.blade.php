@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
    <form id="myform" method="post" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">My Timesheet</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
        <table class="maintable">
          <thead>
            <tr>
              <th>
                Type
              </th>
              <th>
                Checkin
              </th>
              <th>
                Checkout
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($timesheet as $t)
                <tr>
                  <td>
                    {{ ucfirst($t['type']) }}
                  </td>
                  <td>
                    {{ $tz::utcToSpecificTimezone($t['checkin'], $timezone, "D d M, Y - H:i") }}
                  </td>
                  <td>
                    @if ($t['checkout'] != null)
                      {{ $tz::utcToSpecificTimezone($t['checkout'], $timezone, "D d M, Y - H:i") }}
                    @endif
                  </td>
                </tr>
            @endforeach
          </tbody>
        </table>
    </form>
    
@endsection

@section('scripts')

@endsection
