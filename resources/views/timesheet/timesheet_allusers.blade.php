@extends('layouts.app')

@section('assets')

@endsection

@section('content')
<h2 class="pageheading">Timesheet - All Users</h2>
* The <b>maximum normal hours</b> for a user per day is the duration between the user's work-start-time and work-end-time entered in the <a href="{{route('user-all')}}">user's</a> profile. 
<br><br>
<div class="table-container">
  <table class="display maintable" style="width:100%">
    <thead>
      <tr>
        <th>Users</th>
        @foreach($dateHeader as $d)
          <th>{{ date("D d M", strtotime($d)) }}</th>
        @endforeach
        <th>* Normal Hours</th>
      </tr>
    </thead>
  <tbody>
      @foreach($timesheets as $user)
        <tr>
          <td>
            {{ ucfirst($user['timesheet'][0]['fname']) }} {{ ucfirst($user['timesheet'][0]['lname']) }}
          </td>
          @foreach($user['timesheet'] as $t)
            @if (array_key_exists('checkin' , $t) && $t['checkin'] != null)
              <td>
                {{ date("H:i", strtotime($t['checkin'])) }} - {{ date("H:i", strtotime($t['checkout'])) }} <!-- Times are already in user's timezone-->
              </td>
            @else
              <td></td>
            @endif
          @endforeach
          <td>{{ $user['weeklynormalhours'] }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div> 
@endsection

@section('scripts')
<script>
  $(document).ready(function() {
    $('.maintable').DataTable({
      dom: 'Bfrtip',
      buttons: [
          'excel'
      ]  
    });
    // var buttonCommon = { init: function(dt, node, config) { 
    //   var table = dt.table().context[0].nTable; 
    //   if (table) config.title = $('.pageheading').html() }, 
    //       title: 'default title'
    // };
    // $.extend( $.fn.dataTable.defaults, {
    //   "buttons": [
    //       $.extend( true, {}, buttonCommon, {
    //           extend: 'excel',
    //           orientation: 'landscape',
    //           title: $('.pageheading').html()
    //       } )
    //   ]
    // } );
  });

 
</script>
@endsection
