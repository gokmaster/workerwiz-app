@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('emailcampaign-create') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Create Email Campaign</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Email Campaign Details</legend>
        <div class="colums">
          <div class="item">
            <label for="campaign_name">Campaign Name<span>*</span></label>
            <input id="campaign_name" type="text" name="campaign_name" value="{{ old('campaign_name') }}" required/>
          </div>
        </div>      
      </fieldset>
      <br>
      <fieldset>
        <legend>Email Content Details</legend>
        <div class="colums">
          <div class="item">
            <label for="content_type">How do you want to add email content?<span>*</span></label>
            <select class="form-control" id="content_type" name="content_type" required>
                <option value="sourcecode">Add HTML Source code</option>
                <option value="template">Use Email Template</option>
            </select>
          </div>
          <div class="item">
            <label for="email_subject">Email Subject<span>*</span></label>
            <input id="email_subject" type="text" name="email_subject" value="{{ old('email_subject') }}" required/>
          </div>
        </div> 
        <div id="srccode-section">
            <div class="form-group">
              <label for="content">HTML Source code</label>
              <textarea class="form-control" rows="15" id="content" name="content"></textarea>
            </div>
        </div>      
      </fieldset>		

      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
    </form>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
        });
    }); // document.ready
</script>
@endsection
