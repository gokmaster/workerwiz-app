@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
@endsection

@section('content')
  <h2 class="pageheading">Send Email Campaign</h2>
  <br/>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

  <div class="table-container">    
    <table class="display maintable" style="width:100%">
        <thead>
            <tr>
              <th>Campaign</th>
              <th>Created at</th>
              <th>Send Campaign</th>
            </tr>
        </thead>
        <tbody>
          @if ($campaigns)
            @foreach($campaigns as $campaign)
              <tr>
                <td>{{ ucfirst($campaign['campaign_name']) }}</td>
                <td>{{ $tz::utcToSpecificTimezone($campaign['created_at'], $timezone, "d-M-Y, H:i") }}</td>
                <td><button class="btnShowMailingListModal smallbutton" type="button" data-emailcamapign-name="{{$campaign['campaign_name']}}" data-emailcamapign-id="{{$campaign['id']}}">Send</button></td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>  

  <!-- Modal -->
<div id="mailingListModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send <span id="campaign-in-title"></span> to Following Mailing Lists</h4>
            </div>
            <div id="mailingListBody" class="modal-body">
                <input type="hidden" id="emailcampaign-id" value=""/>
                
                <div class="table-container">    
                  <table id="mailinglist-table" class="display" style="width:100%">
                      <thead>
                          <tr>
                            <th>Mailing List</th>
                            <th>Created at</th>
                            <th>Send Campaign</th>
                          </tr>
                      </thead>
                      <tbody>
                        @if ($mailinglistgroups)
                          @foreach($mailinglistgroups as $ml)
                            <tr>
                              <td>{{ ucfirst($ml['title']) }}</td>
                              <td>{{ $tz::utcToSpecificTimezone($ml['created_at'], $timezone, "d-M-Y, H:i") }}</td>
                              <td id="send-td-{{$ml['id']}}"><button class="btnSendEmailCampaign smallbutton" type="button" data-mailinggroup-id="{{$ml['id']}}">Send</button></td>
                            </tr>
                          @endforeach
                        @endif
                      </tbody>
                  </table>
                </div> <!-- table container -->
            </div>
        </div>
    </div>
</div>  
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $.fn.dataTable.moment('D-MMM-YYYY, HH:mm');
    $('.maintable').DataTable({
      order: [[1, 'desc']],  // order by Created at
    });

    $('#mailinglist-table').DataTable({
      order: [[1, 'desc']],  // order by Created at
    });

    $(document).on("click",".btnShowMailingListModal", function(e){
      var campaignId = $(e.target).data('emailcamapign-id');
      var campaign = $(e.target).data('emailcamapign-name');

      $('#emailcampaign-id').val(campaignId);
      $('#campaign-in-title').text('"'+campaign+'"');

      $('#mailingListModal').modal('show');
    }); 

    $(document).on("click",".btnSendEmailCampaign", function(e) {        
        var mailingGrpId = $(e.target).data('mailinggroup-id');
        var emailCampaignId = $('#emailcampaign-id').val();

        $("#send-td-"+mailingGrpId).html("Please wait...");
      
        var formdata = new FormData();
        formdata.append("mailinggroup_id", mailingGrpId);
        formdata.append("email_campaign_id", emailCampaignId);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            enctype: 'multipart/form-data',
            url: "{{ route('emailcampaign-send') }}",
            data: formdata,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000000000,
            success: function (response) {
                var jsonObj = JSON.parse(response); //convert JSON string to JSON object
                if (jsonObj.success == true) {
                    $("#send-td-"+mailingGrpId).html(""); // remove "Please wait"
                    swal("Success", jsonObj.message, "success");
                } else {
                    swal("Failed", jsonObj.message, "error");
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        }); // $.ajax
    }); //  btnSendEmailCampaign

  }); // document.ready
</script>
@endsection
