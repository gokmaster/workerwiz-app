@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('sale-add') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Add Sale</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Customer Details</legend>
        <div class="colums">
          <div class="item">
            <label for="fname">First Name<span>*</span></label>
            <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required/>
          </div>
          <div class="item">
            <label for="lname">Last Name<span>*</span></label>
            <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required/>
          </div>
          <div class="item">
            <label for="phone">Phone<span>*</span></label>
            <input id="phone" type="text" name="phone" value="{{ old('phone') }}" maxlength="16" required/>
          </div>
          <div class="item">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ old('email') }}" />
          </div>
          <div class="item">
            <label for="preference">Preference</label>
            <input id="preference" type="text" name="preference" value="{{ old('preference') }}" />
          </div>
          <div class="item">
            <label for="price_comfort">Price Comfort</label>
            <input type="text" id="price_comfort" name="price_comfort" readonly>
            <div id="slider-range" style="width:98.5%;"></div>
          </div>
        </div>

        <h4>Physical Address</h4>
        <div class="colums">
          <div class="item">
            <label for="address1">Address Line 1<span>*</span></label>
            <input id="address1" type="text" name="address1" value="{{ old('address1') }}" required/>
          </div>
          <div class="item">
            <label for="address2">Address Line 2</label>
            <input id="address2" type="text" name="address2" value="{{ old('address2') }}" />
          </div>
          <div class="item">
            <label for="city">City<span>*</span></label>
            <input id="city" type="text" name="city" value="{{ old('city') }}" required/>
          </div>
          <div class="item">
            <label for="state">State</label>
            <input id="state" type="text" name="state" value="{{ old('state') }}" />
          </div>
          <div class="item">
            <label for="zipcode">Zip/Postal Code</label>
            <input id="zipcode" type="text" name="zipcode" value="{{ old('zipcode') }}" maxlength="12"/>
          </div>
          <div class="item">
            <label for="country">Country<span>*</span></label>
            <select class="form-control" id="country" name="country" required>
                <option disabled selected value> -- Select a country -- </option>
                @foreach($countries as  $country)
                <option value="{{ $country }}">{{ ucfirst($country) }}</option>
                @endforeach
            </select>
          </div>
        </div>

        <h4>Postal Address</h4>
        <div class="colums">
        <div class="item">
            <label for="postal_address1">Address Line 1<span>*</span></label>
            <input id="postal_address1" type="text" name="postal_address1" value="{{ old('postal_address1') }}" required/>
          </div>
          <div class="item">
            <label for="postal_address2">Address Line 2</label>
            <input id="postal_address2" type="text" name="postal_address2" value="{{ old('postal_address2') }}" />
          </div>
          <div class="item">
            <label for="postal_city">City<span>*</span></label>
            <input id="postal_city" type="text" name="postal_city" value="{{ old('postal_city') }}" required/>
          </div>
          <div class="item">
            <label for="postal_state">State</label>
            <input id="postal_state" type="text" name="postal_state" value="{{ old('postal_state') }}" />
          </div>
          <div class="item">
            <label for="postal_zipcode">Zip/Postal Code</label>
            <input id="postal_zipcode" type="text" name="postal_zipcode" value="{{ old('postal_zipcode') }}" maxlength="12"/>
          </div>
          <div class="item">
            <label for="postal_country">Country<span>*</span></label>
            <select class="form-control" id="postal_country" name="postal_country" required>
                <option disabled selected value> -- Select a country -- </option>
                @foreach($countries as  $country)
                <option value="{{ $country }}">{{ ucfirst($country) }}</option>
                @endforeach
            </select>
          </div>
        </div>
      </fieldset>	
      <br>
      <fieldset>
        <legend>Sale Details</legend>
        <div class="colums">
          @if($campaign_id != 0)
            <div class="item">
              <label for="campaign_name">Campaign</label>
              <input id="campaign_name" type="text" value="{{ $campaign }}" disabled/>
              <input id="campaign_id" name="campaign_id" type="hidden" value="{{ $campaign_id }}"/>
            </div>
          @endif
          <div class="item">
            <label for="description">Description</label>
            <input id="description" type="text" name="description" value="{{ old('description') }}"/>
          </div>
          <div class="item">
            <label for="sku">Item/SKU<span>*</span></label>
            <input id="sku" type="text" name="sku" value="{{ old('sku') }}" required/>
          </div>
          <div class="item">
            <label for="quantity">Quantity<span>*</span></label>
            <input id="quantity" type="number" name="quantity" value="{{ old('quantity') }}" min="1" required/>
          </div>
          <div class="item">
            <label for="saleprice">Sale Price<span>*</span></label>
            <input id="saleprice" type="number" name="saleprice" value="{{ old('saleprice') }}" step="0.01" min="0" required/>
          </div>
          <div class="item">
            <label for="notes">Notes</label>
            <input id="notes" type="text" name="notes" value="{{ old('notes') }}" />
          </div>
        </div>      
      </fieldset>		
      <br>
      <fieldset>
        <legend>Credit Card Details</legend>
        <div class="colums">
          <div class="item">
            <label for="cardholder_name">Cardholder Name</label>
            <input id="cardholder_name" type="text" name="cardholder_name" value="{{ old('cardholder_name') }}"/>
          </div>
          <div class="item">
            <label for="creditcard_number">Credit Card Number</label>
            <input id="creditcard_number" type="text" name="creditcard_number" value="{{ old('creditcard_number') }}"  maxlength="19"/>
          </div>
          <div class="item">
            <label for="card_expiry_date">Expiry Date</label>
            <input id="card_expiry_date" type="text" name="card_expiry_date" value="{{ old('card_expiry_date') }}" placeholder="MM/YYYY" minlength="7" maxlength="7"/>
          </div>
          <div class="item">
            <label for="cvv">CVV</label>
            <input id="cvv" type="text" name="cvv" value="{{ old('cvv') }}" minlength="3" maxlength="4"/>
          </div>
        </div>      
      </fieldset>	

      @if($customfields)
        @foreach($customfields as $sectionTitle=>$formSections)
        <br>
        <fieldset>
          <legend>{{$sectionTitle}}</legend>
          <div class="colums">
            @foreach($formSections as $field)
              <?php $inputName = $field['input_codename']; ?>
              <div class="item">
                <label for="{{$inputName}}">
                  {{ ucfirst($field['field_label'])}}
                  @if($field['required']==1)<span>*</span> @endif
                  <span class="field-info">{{$field['field_info']}}</span>
                </label>
                <input id="{{$inputName}}" type="{{$field['input_type']}}" name="{{$inputName}}" value="{{ old($inputName) }}" @if($field['required']==1) required @endif/>
              </div>
            @endforeach
          </div>      
        </fieldset>	
        @endforeach
      @endif

      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
    </form>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
          rules: {
            phone: {
              digits: true,
            },
            creditcard_number: {
              digits: true,
            },
            cvv: {
              digits: true,
            }
          } // rules
        });

        $( function() {
          $("#slider-range").slider({
            range: true,
            min: 0,
            max: 1000,
            values: [100, 300],
            slide: function( event, ui ) {
              $("#price_comfort").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
          });
          $("#price_comfort").val("$" + $("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1) );
        });

        $('#card_expiry_date').MonthPicker({ Button: false }); // KidSysco-month-picker
        $("#card_expiry_date").keydown(false);

        $("#fname, #lname, #email, #phone").keyup(function(){
          var fname = $("#fname").val();
          var lname = $("#lname").val();
          var email = $("#email").val();
          var phone = $("#phone").val();
                      
          if (fname !== null && fname !== ""
              && lname !== null && lname !== ""
              && (email !== null && email !== "" || phone !== null && phone !== "") 
          ) {
            var data = new FormData();
            data.append("fname", fname);
            data.append("lname", lname);
            data.append("email", email);
            data.append("phone", phone);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('customer-fetchbyname') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object
                    
                    // Auto-fill Address fields if customer record exist
                    if (jsonObj.success == true) {
                      var cust = jsonObj.customer_data;
                        $('#address1').val(cust.address1);
                        $('#address2').val(cust.address2);
                        $('#city').val(cust.city);
                        $('#state').val(cust.state);
                        $('#zipcode').val(cust.zipcode);
                        $('#country').val(cust.country);
                        $('#postal_address1').val(cust.postal_address1);
                        $('#postal_address2').val(cust.postal_address2);
                        $('#postal_city').val(cust.postal_city);
                        $('#postal_state').val(cust.postal_state);
                        $('#postal_zipcode').val(cust.postal_zipcode);
                        $('#postal_country').val(cust.postal_country);
                    }     
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
          }
        }); // $("#fname, #lname, #email, #phone").keyup

    }); // document.ready
</script>
@endsection
