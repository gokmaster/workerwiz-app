@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/profile.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<h2 class="pageheading">Sale</h2>
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

<div class="profile-wrapper">	  
	<div class="additional-block">
		<h2>
			Customer Details
		</h2>
		<div class="address-details">
			<div class="item">
				<div class="label">
					First Name:
				</div>
				<div class="value">
					{{$sale['fname']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Last Name:
				</div>
				<div class="value">
					{{$sale['lname']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Phone:
				</div>
				<div class="value">
					{{$sale['phone']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Email:
				</div>
				<div class="value">
					{{$sale['email']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Preference:
				</div>
				<div class="value">
					{{$sale['preference']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Price Comfort:
				</div>
				<div class="value">
					{{$sale['price_comfort']}}
				</div>
			</div>
		</div> <!--address-details-->

		<h4>Physical Address</h4>
		<div class="address-details">
			<div class="item">
				<div class="label">
					Address Line 1:
				</div>
				<div class="value">
					{{$sale['address1']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Address Line 2:
				</div>
				<div class="value">
					{{$sale['address2']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					City:
				</div>
				<div class="value">
					{{$sale['city']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					State:
				</div>
				<div class="value">
					{{$sale['state']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Zipcode:
				</div>
				<div class="value">
					{{$sale['zipcode']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Country:
				</div>
				<div class="value">
					{{$sale['country']}}
				</div>
			</div>
		</div> <!--address-details-->

		<h4>Postal Address</h4>
		<div class="address-details">
			<div class="item">
				<div class="label">
					Address Line 1:
				</div>
				<div class="value">
					{{$sale['postal_address1']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Address Line 2:
				</div>
				<div class="value">
					{{$sale['postal_address2']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					City:
				</div>
				<div class="value">
					{{$sale['postal_city']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					State:
				</div>
				<div class="value">
					{{$sale['postal_state']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Zipcode:
				</div>
				<div class="value">
					{{$sale['postal_zipcode']}}
				</div>
			</div>
			<div class="item">
				<div class="label">
					Country:
				</div>
				<div class="value">
					{{$sale['postal_country']}}
				</div>
			</div>
		</div> <!--address-details-->
	</div> <!--additional-block-->

	<div class="additional-block">
			<h2>
				Sale Details
			</h2>
			<div class="address-details">
				@if (isset($sale['campaign_name']))
					<div class="item">
						<div class="label">
							Campaign:
						</div>
						<div class="value">
							{{$sale['campaign_name']}}
						</div>
					</div>
				@endif
				<div class="item">
					<div class="label">
						Description:
					</div>
					<div class="value">
						{{$sale['description']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						SKU:
					</div>
					<div class="value">
						{{$sale['sku']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Quantity:
					</div>
					<div class="value">
						{{$sale['quantity']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Sale Price:
					</div>
					<div class="value">
						{{$sale['saleprice']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Notes:
					</div>
					<div class="value">
						{{$sale['notes']}}
					</div>
				</div>
			</div> <!--address-details-->
		</div> <!--additional-block-->
	
	@if (in_array("customer_view_creditcard_data", $viewerPermissions))
		<div class="additional-block">
			<h2>
				Credit Card Details
			</h2>
			<div class="address-details">
				<div class="item">
					<div class="label">
						Credit Card No.:
					</div>
					<div class="value">
						{{$sale['creditcard_number']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Cardholder Name:
					</div>
					<div class="value">
						{{$sale['cardholder_name']}}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Expiry Date:
					</div>
					<div class="value">
						@if ($sale['card_expiry_date'] != null)
							{{date("M/Y", strtotime($sale['card_expiry_date']))}}
						@endif
					</div>
				</div>
				<div class="item">
					<div class="label">
						CVV:
					</div>
					<div class="value">
						{{$sale['cvv']}}
					</div>
				</div>
			</div> <!--address-details-->
		</div> <!--additional-block-->
	@endif

	@if (isset($sale['customfields']))
		@foreach($sale['customfields'] as $sectionTitle=>$sections)
		<div class="additional-block">
			<h2>
				{{ucwords($sectionTitle)}}
			</h2>
			<div class="address-details">
				@foreach ($sections as $fieldLabel=>$field)
					<div class="item">
						<div class="label">
							{{ ucwords($fieldLabel) }}:
						</div>
						<div class="value">
							{{ $field }}
						</div>
					</div>
				@endforeach
			</div> <!--address-details-->
		</div> <!--additional-block-->
		@endforeach
	@endif
</div>
@endsection

@section('scripts')
<script>

</script>
@endsection
