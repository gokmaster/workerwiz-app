@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <h2 class="pageheading">Sales</h2>
    <a href="{{route('sale-all')}}?year={{$thisyearLess1}}" class="btnYear btnTab @if($yearViewed == $thisyearLess1) btnTabSelected @endif">{{$thisyearLess1}}</a>
    <a href="{{route('sale-all')}}?year={{$thisyear}}" class="btnYear btnTab @if($yearViewed == $thisyear) btnTabSelected @endif">{{$thisyear}}</a>
    <div id="sales-div" class="table-container">
        @include('sale.partial.sales_table')
    </div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $.fn.dataTable.moment('D-MMM-YYYY, HH:mm');
        $('.maintable').DataTable({  
            order: [[1, 'desc']],  // order by Transaction Date
        });

        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>
@endsection
