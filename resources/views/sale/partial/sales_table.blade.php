
<table class="display maintable" style="width:100%">
    <thead>
        <tr>
            <th>Action</th>
            <th>Transaction Date</th>
            <th>Campaign</th>
            <th>Customer Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Preference</th>
            <th>Price Comfort</th>
            <th>Physical Address</th>
            <th>Postal Address</th>
            <th>Description</th>
            <th>Item/SKU</th>
            <th>Quantity</th>
            <th>Sale Price</th>
            <th>Notes</th>
            @if (in_array("customer_view_creditcard_data", $viewerPermissions))
                <th>Cardholder Name</th>
                <th>Creditcard No.</th>
                <th>Expiry Date</th>
                <th>CVV</th>
            @endif
            <th>Sale Agent</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($sales as $sale)
    <tr>
        <td>
            <a href="{{ route('sale-edit-form', ['sale_id' => $sale['id'] ]) }}" class="btnEdit iconbutton" data-toggle="tooltip" data-placement="left" title="Edit"><i class="fa fa-edit"></i></a>
        </td>
        <td>{{ $tz::utcToSpecificTimezone($sale['created_at'], $timezone, "d-M-Y, H:i") }}</td>
        <td>{{ ucfirst($sale['campaign_name']) }}</td>
        <td>{{ ucfirst($sale['fname']) }} {{ ucfirst($sale['lname']) }}</td>
        <td>{{ $sale['phone'] }}</td>
        <td>{{ $sale['email'] }}</td>
        <td>{{ $sale['preference'] }}</td>
        <td>{{ $sale['price_comfort'] }}</td>
        <td>
            {{ $sale['address1'] }},
            @if ($sale['address2'] != null) {{ $sale['address2'] }}, @endif
            {{ $sale['city'] }},
            @if ($sale['state'] != null) {{ $sale['state'] }}, @endif
            @if ($sale['zipcode'] != null) {{ $sale['zipcode'] }}, @endif
            {{ $sale['country'] }}
        </td>
        <td>
            {{ $sale['postal_address1'] }},
            @if ($sale['postal_address2'] != null) {{ $sale['postal_address2'] }}, @endif
            {{ $sale['postal_city'] }},
            @if ($sale['postal_state'] != null) {{ $sale['postal_state'] }}, @endif
            @if ($sale['postal_zipcode'] != null) {{ $sale['postal_zipcode'] }}, @endif
            {{ $sale['postal_country'] }}
        </td>
        <td>{{ $sale['description'] }}</td>
        <td>{{ $sale['sku'] }}</td>
        <td>{{ $sale['quantity'] }}</td>
        <td>{{ $sale['saleprice'] }}</td>
        <td>{{ $sale['notes'] }}</td>
        @if (in_array("customer_view_creditcard_data", $viewerPermissions))
            <td>{{ $sale['cardholder_name'] }}</td>
            <td>{{ $sale['creditcard_number'] }}</td>
            <td>
                @if (isset($sale['card_expiry_date']))
                    {{ date('m/Y', strtotime($sale['card_expiry_date'])) }}
                @endif
            </td>
            <td>{{ $sale['cvv'] }}</td>
        @endif
        <td>{{ ucfirst($sale['sale_agent_fname']) }} {{ ucfirst($sale['sale_agent_lname']) }}</td>
        <td><a href="{{route('sale-details',['sale_id'=>$sale['id']])}}">See more</a></td>
    </tr>
    @endforeach
    </tbody>
</table>

