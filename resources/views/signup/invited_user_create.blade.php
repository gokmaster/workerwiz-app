@extends('layouts.site')

@section('assets')
<!-- Font Icon -->
<link rel="stylesheet" href="{{ asset('fonts/material-icon/css/material-design-iconic-font.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/signup.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="main">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

    @if ($success == true)
        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="signup-form" class="signup-form" action="{{ route('invited-user-create') }}">
                        @csrf
                        <h2 class="form-title">Create User Account</h2>
                        <input type="hidden" name="verification_code" id="verification_code" value="{{$user['verification_code']}}"/>
                        <div class="form-group">
                            <input type="text" class="form-input" name="fname" id="fname" placeholder="First Name" value="{{old('fname')}}" required/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input" name="lname" id="lname" placeholder="Last Name" value="{{old('lname')}}" required/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input" name="company_name" id="company_name" placeholder="Company Name" value="{{$user['company_name']}}" disabled/>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-input" name="email" id="email" placeholder="Email" value="{{$user['email']}}" disabled/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" name="password" id="password" placeholder="Password" required/>
                            <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" name="re_password" id="re_password" placeholder="Repeat your password" required/>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" required/>
                            <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                        </div>
                        <div class="form-group" style="text-align:center;">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Create Account"/>
                        </div>
                    </form>
                    <p class="loginhere">
                        Already have an account ? <a href="{{route('timesheet-form')}}" class="loginhere-link">Login Here</a>
                    </p>
                </div>
            </div>
        </section>
    @else 
        <div class="alert-fail">
            {{ $message }}
        </div>
    @endif
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#signup-form").validate({
            rules: {
                password: {
                    minlength: 8
                },
                re_password: {
                    minlength: 8,
                    equalTo: "#password"
                }
            }
        });

        (function($) {
            $(".toggle-password").click(function() {
                $(this).toggleClass("zmdi-eye zmdi-eye-off");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                input.attr("type", "text");
                } else {
                input.attr("type", "password");
                }
            });
        })(jQuery);
    });
</script>
@endsection
