@extends('layouts.site')

@section('assets')
<!-- Font Icon -->
<link rel="stylesheet" href="{{ asset('fonts/material-icon/css/material-design-iconic-font.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/signup.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="main">
    @if($success == true)
        <div class="alertbox">
            <h4>Account Activated</h4>
            Your account has been successfully activated
        </div>
        <div style="text-align:center;">
            <a href="{{route('timesheet-form')}}" class="btn btn-success">LOGIN</a>
        </div>
    @else
        <div class="alert-fail">
            {{ $message }}
        </div>
    @endif
       
</div>
@endsection

@section('scripts')

@endsection
