@extends('layouts.site')

@section('assets')
<!-- Font Icon -->
<link rel="stylesheet" href="{{ asset('fonts/material-icon/css/material-design-iconic-font.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/signup.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="main">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    
    <div class="alertbox">
        <h4>Account Created Successfully</h4>
        Click the button below to update your profile after logging-in.
    </div>
    <div style="text-align:center;">
        <a href="{{route('useredit-own-form')}}" class="btn btn-success">Update My Profile</a>
    </div>
     
</div>
@endsection

@section('scripts')

@endsection
