@extends('layouts.site')

@section('assets')
<!-- Font Icon -->
<link rel="stylesheet" href="{{ asset('fonts/material-icon/css/material-design-iconic-font.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/signup.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
<div class="main">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    
    <div class="alertbox">
        <h4>Signup Successful</h4>
        Please click on the link sent to <b>{{$email}}</b> to activate your account.
    </div>
    <div style="text-align:center;">
        <a href="#" class="btn btn-success">HOME</a>
    </div>
     
</div>
@endsection

@section('scripts')

@endsection
