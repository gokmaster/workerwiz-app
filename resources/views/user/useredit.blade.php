@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('user-edit') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Edit User</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Personal Details</legend>
        <input id="id" type="hidden" name="id" value="{{ $userdata['id'] }}"/>
        <div class="colums">
          <div class="item">
            <label for="fname">First Name<span>*</span></label>
            <input id="fname" type="text" name="fname" value="{{ old('fname', $userdata['fname']) }}" required/>
          </div>
          <div class="item">
            <label for="lname">Last Name<span>*</span></label>
            <input id="lname" type="text" name="lname" value="{{ old('lname', $userdata['lname']) }}" required/>
          </div>
          <div class="item">
            <label for="home_phone">Home Phone</label>
            <input id="home_phone" type="tel"   name="home_phone" value="{{ old('home_phone', $userdata['home_phone']) }}" />
          </div>
          <div class="item">
            <label for="mobile_phone">Mobile Phone</label>
            <input id="mobile_phone" type="tel"   name="mobile_phone" value="{{ old('mobile_phone' , $userdata['mobile_phone']) }}" />
          </div>
          <div class="item">
            <label for="streetaddress">Street Address</label>
            <input id="streetaddress" type="text"   name="streetaddress" value="{{ old('streetaddress' , $userdata['streetaddress']) }}" />
          </div>
          <div class="item">
            <label for="city">City</label>
            <input id="city" type="text"   name="city" value="{{ old('city' , $userdata['city']) }}" />
          </div>
          <div class="item">
            <label for="personal_email">Personal Email Address</label>
            <input id="personal_email" type="email"   name="personal_email" value="{{ old('personal_email', $userdata['personal_email']) }}"/>
          </div>
          <div class="item">
            <label for="dob">Date of Birth<span>*</span></label>
            <input id="dob" type="text" name="dob" value="{{ old('dob', date('d-m-Y', strtotime($userdata['dob']))) }}" placeholder="dd-mm-yyyy" autocomplete="off" required />
          </div>
          <div class="item">
            <label for="marital_status">Marital Status</label>
            <select class="form-control" id="marital_status" name="marital_status">
                <option disabled selected value> -- Select an option -- </option>
                <option value="single" @if ($userdata['marital_status'] == 'single') selected @endif>
                  Single
                </option>
                <option value="married" @if ($userdata['marital_status'] == 'married') selected @endif>
                  Married
                </option>
                <option value="widowed" @if ($userdata['marital_status'] == 'widowed') selected @endif>
                  Widowed
                </option>
                <option value="divorced" @if ($userdata['marital_status'] == 'divorced') selected @endif>
                  Divorced
                </option>
                <option value="separated" @if ($userdata['marital_status'] == 'separated') selected @endif>
                  Separated
                </option>               
            </select>
          </div>
          <div class="item">
            <label for="tin_number">TIN Number</label>
            <input id="tin_number" type="text"   name="tin_number" value="{{ old('tin_number', $userdata['tin_number']) }}" />
          </div>
          <div class="item">
            <label for="fnpf_number">Superannuation Account Number</label>
            <input id="fnpf_number" type="text"   name="fnpf_number" value="{{ old('fnpf_number', $userdata['fnpf_number']) }}" />
          </div>
          <div class="item">
              <label for="profilepic">Profile Picture</label>
              <input type="file" name="profilepic" id="profilepic" />
          </div>
		</div>
	</fieldset>
	 <br>
	<fieldset>
		<legend>Bank Account Details</legend>
		<div class="colums">
		  <div class="item">
			<label for="bank_name">Bank Name</label>
      <input id="bank_name" type="text"   name="bank_name" value="{{ old('bank_name') }}" />
		  </div>
		  <div class="item">
			<label for="bank_account_name">Account Name</label>
			<input id="bank_account_name" type="text"   name="bank_account_name" value="{{ old('bank_account_name', $userdata['bank_account_name']) }}" />
		  </div>
		  <div class="item">
			<label for="bank_account_number">Account Number</label>
			<input id="bank_account_number" type="text"   name="bank_account_number" value="{{ old('bank_account_number', $userdata['bank_account_number']) }}" />
		  </div>
		  <div class="item">
			<label for="bsb_number">BSB Number</label>
			<input id="bsb_number" type="text"   name="bsb_number" value="{{ old('bsb_number', $userdata['bsb_number']) }}" />
		  </div>
		</div>
	</fieldset>
      <br/>
	<fieldset>
	<legend>Employment Details</legend>
	<div class="colums">
        <div class="item">
          <label for=email>Work Email Address<span>*</span></label>
          <input id="email" type="email"   name="email" value="{{ old('email', $userdata['email']) }}" required/>
        </div>
        <div class="item">
            <label for="cug_number">Work Contact Number</label>
            <input id="cug_number" type="text" name="cug_number" value="{{ old('cug_number', $userdata['cug_number']) }}" />
        </div>
        <div class="item">
            <label for="phone_extension">Phone Extension</label>
            <input id="phone_extension" type="text" name="phone_extension" value="{{ old('phone_extension', $userdata['phone_extension']) }}" />
        </div>
        <div class="item">
            <label for="did_number">DID Number</label>
            <input id="did_number" type="text" name="did_number" value="{{ old('did_number', $userdata['did_number']) }}" />
        </div>
        <div class="item">
            <label for="emp_startdate">Employment Start Date<span>*</span></label>
            <input id="emp_startdate" type="text" name="emp_startdate" value="{{ old('emp_startdate', date('d-m-Y', strtotime($userdata['emp_startdate']))) }}" placeholder="dd-mm-yyyy" autocomplete="off" required />
        </div>
        <div class="item">
            <label for="emp_enddate">Employment End Date</label>
            <input id="emp_enddate" type="text" name="emp_enddate" value="{{ old('emp_enddate', date('d-m-Y', strtotime($userdata['emp_enddate'])))  }}" placeholder="dd-mm-yyyy" autocomplete="off"/>
        </div>
        <div class="item">
            <label for="work_starttime">Work Start Time<span>*</span></label>
            <input id="work_starttime" type="text" name="work_starttime" value="{{ old('work_starttime', date('H:i', strtotime($userdata['work_starttime'])))  }}" autocomplete="off" required />
        </div>
        <div class="item">
            <label for="work_endtime">Work End Time<span>*</span></label>
            <input id="work_endtime" type="text" name="work_endtime" value="{{ old('work_endtime', date('H:i', strtotime($userdata['work_endtime']))) }}" autocomplete="off" required />
        </div>
		    <div class="item">
            <label for="weekly_normal_hours">Weekly Normal Hours<span>*</span></label>
            <input id="weekly_normal_hours" type="number" name="weekly_normal_hours" min="0" value="{{ old('weekly_normal_hours', $userdata['weekly_normal_hours']) }}" required/>
        </div>
        <div class="item">
            <label for="hourly_rate">Hourly Rate<span>*</span></label>
            <input id="hourly_rate" type="number" name="hourly_rate" min="0" step=".01" value="{{ old('hourly_rate', $userdata['hourly_rate']) }}" required/>
        </div>
        <div class="item">
            <label for="job_title_id">Job Title</label>
            <select class="form-control" id="job_title_id" name="job_title_id">
                <option disabled selected value> -- Select a job title -- </option>
                @foreach($jobtitles as  $jobtitle)
                <option value="{{ $jobtitle['id'] }}"
                  @if ($jobtitle['id'] == $userdata['job_title_id']) selected @endif >
                  {{ ucfirst($jobtitle['job_title']) }}
                </option>
                @endforeach
            </select>
        </div>
        <div class="item">
            <label for="role_id">Department<span>*</span>
              <span class="field-info">This sets the permissions this user will have on the system. <a href="{{route('role-all')}}" target="_blank">Edit Department Permissions</a></span> 
            </label>
            <select class="form-control" id="role_id" name="role_id" required>
                <option disabled selected value> -- Select a department -- </option>
                @foreach($roles as  $role)
                <option value="{{ $role['id'] }}" 
                  @if ($role['id'] == $userdata['role_id']) selected @endif >
                  {{ ucfirst($role['role_name']) }}
                </option>
                @endforeach
            </select>
        </div>
	</div>
	</fieldset>
      <br>
	<fieldset>
		<legend>Emergency Contact Information</legend>
		<div class="colums">
		  <div class="item">
			<label for="emergency_contact_name">Full Name</label>
			<input id="emergency_contact_name" type="text"   name="emergency_contact_name" value="{{ old('emergency_contact_name', $userdata['emergency_contact_name']) }}" />
		  </div>
		  <div class="item">
			<label for="emergency_contact_relationship">Relationship</label>
			<input id="emergency_contact_relationship" type="text"   name="emergency_contact_relationship" value="{{ old('emergency_contact_relationship', $userdata['emergency_contact_relationship']) }}" />
		  </div>
		  <div class="item">
			<label for="emergency_contact_streetaddress">Street Address</label>
			<input id="emergency_contact_streetaddress" type="text"   name="emergency_contact_streetaddress" value="{{ old('emergency_contact_streetaddress', $userdata['emergency_contact_streetaddress']) }}" />
		  </div>
		  <div class="item">
			<label for="emergency_contact_city">City</label>
			<input id="emergency_contact_city" type="text"   name="emergency_contact_city" value="{{ old('emergency_contact_city', $userdata['emergency_contact_city']) }}" />
		  </div>
		  <div class="item">
			<label for="emergency_contact_phone">Primary Phone</label>
			<input id="emergency_contact_phone" type="tel"   name="emergency_contact_phone" value="{{ old('emergency_contact_phone', $userdata['emergency_contact_phone']) }}" />
		  </div>
		  <div class="item">
			<label for="emergency_contact_phone2">Alternative Phone</label>
			<input id="emergency_contact_phone2" type="tel"   name="emergency_contact_phone2" value="{{ old('emergency_contact_phone2', $userdata['emergency_contact_phone2']) }}" />
		  </div>
      <div class="item">
			<label for="medical_conditions">Allergies/Medical Conditions</label>
			<input id="medical_conditions" type="text"   name="medical_conditions" value="{{ old('medical_conditions', $userdata['medical_conditions']) }}" />
		  </div>
		</div>
	</fieldset>
		<br>
      <div class="btn-block">
      <button type="submit">Save</button>
      </div>
	
  </form>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
          rules: {
			home_phone: {
				digits: true,
			},
			mobile_phone: {
				digits: true,
			},
			cug_number: {
				digits: true,
			},
      phone_extension: {
        digits: true,
      },
      did_number: {
        digits: true,
      },
			bsb_number: {
				digits: true,
			},
			emergency_contact_phone: {
				digits: true,
			},
			emergency_contact_phone2: {
				digits: true,
			}
          }
        });

        $( function() {
          $( "#dob" ).datepicker({
                dateFormat: "dd-mm-yy",
                maxDate: '0', // set max day to +0 days from the current date (i.e. today)
            });

            $( "#emp_startdate" ).datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function(selected) {
                    $("#emp_enddate").datepicker("option","minDate", selected)
                } // end date should not be less than start date
            });
            
            $( "#emp_enddate" ).datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function(selected) {
                    $("#emp_startdate").datepicker("option","maxDate", selected)
                } // start date should not be greater than end date
            });

            $('#work_starttime').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                minTime: '00:00',
                maxTime: '23:00',
                //defaultTime: '8:00',
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $('#work_endtime').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                minTime: '00:00',
                maxTime: '23:00',
                //defaultTime: '17:00',
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        });
    });
</script>
@endsection
