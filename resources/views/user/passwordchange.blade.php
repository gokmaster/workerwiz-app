@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('passwordchange') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Change Password</h2>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="pword">Current Password<span>*</span></label>
            <input id="password" type="password" name="password" value="{{ old('password') }}" required/>
          </div>
          <div class="item">
            <!-- empty space-->
          </div>
          <div class="item">
            <label for="new_password">New Password<span>*</span></label>
            <input id="new_password" type="password" name="new_password" value="{{ old('new_password') }}" required/>
          </div>
          <div class="item">
            <label for="new_password">Repeat New Password<span>*</span></label>
            <input id="re_password" type="password" name="re_password" value="{{ old('re_password') }}" required/>
          </div>
		    </div>
      </fieldset>
      <input id="user_id" type="hidden" name="user_id" value="{{ $userId }}" required/>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
    </form>
    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
      $("#myform").validate({
        rules: {
            new_password: {
                minlength: 8
            },
            re_password: {
                minlength: 8,
                equalTo: "#new_password"
            }
        } // rules
      }); // form validate
  });
</script>
@endsection
