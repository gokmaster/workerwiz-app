@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('user-invite-create') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Invite User</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
      <legend>Employment Details</legend>
      <div class="colums">
        <div class="item">
          <label for="name">Name<span>*</span></label>
          <input id="name" type="text" name="name" value="{{ old('name') }}" required/>
        </div>
        <div class="item">
          <label for=email>Work Email Address<span>*</span></label>
          <input id="email" type="email"   name="email" value="{{ old('email') }}" required/>
        </div>
        <div class="item">
            <label for="emp_startdate">Employment Start Date<span>*</span></label>
            <input id="emp_startdate" type="text" name="emp_startdate" value="{{ old('emp_startdate') }}" placeholder="dd-mm-yyyy" autocomplete="off" required />
        </div>
        <div class="item">
            <label for="weekly_normal_hours">Weekly Normal Hours<span>*</span></label>
            <input id="weekly_normal_hours" type="number" name="weekly_normal_hours" min="0" value="{{ old('weekly_normal_hours') }}" required/>
        </div>
        <div class="item">
            <label for="work_starttime">Work Start Time<span>*</span></label>
            <input id="work_starttime" type="text" name="work_starttime" value="{{ old('work_starttime') }}" autocomplete="off" required />
        </div>
        <div class="item">
            <label for="work_endtime">Work End Time<span>*</span></label>
            <input id="work_endtime" type="text" name="work_endtime" value="{{ old('work_endtime') }}" autocomplete="off" required />
        </div>
        <div class="item">
            <label for="hourly_rate">Hourly Rate<span>*</span></label>
            <input id="hourly_rate" type="number" name="hourly_rate" min="0" step=".01" value="{{ old('hourly_rate') }}" required/>
        </div>
        <div class="item">
            <label for="role_id">Department<span>*</span></label>
            <select class="form-control" id="role_id" name="role_id" required>
                <option disabled selected value> -- Select a department -- </option>
                @foreach($roles as  $role)
                <option value="{{ $role['id'] }}">{{ ucfirst($role['role_name']) }}</option>
                @endforeach
            </select>
        </div>
      </div>
      </fieldset>
	
      <div class="btn-block">
        <button type="submit">Send Invite</button>
      </div>
  </form>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate({
        });

        $( function() {
          $( "#dob" ).datepicker({
                dateFormat: "dd-mm-yy",
                maxDate: '0', // set max day to +0 days from the current date (i.e. today)
            });

            $( "#emp_startdate" ).datepicker({
                dateFormat: "dd-mm-yy"
            });
            
            $('#work_starttime').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                minTime: '00:00',
                maxTime: '23:00',
                defaultTime: '8:00',
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });

            $('#work_endtime').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                minTime: '00:00',
                maxTime: '23:00',
                defaultTime: '17:00',
                startTime: '00:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        });
    });
</script>
@endsection
