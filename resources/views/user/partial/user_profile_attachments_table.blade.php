
@if($attachments)
    <table class="display maintable" style="width:100%">
        <thead>
            <tr>
                <th>Type</th>
                <th>Description</th>
                <th>Date Applied</th>
                <th>Last Modified</th>
                <th>Attached File</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($attachments as $atch)
            <tr id="atch-row-{{$atch['id']}}">
                <td>{{ ucfirst($atch['title']) }}</td>
                <td>{{ ucfirst($atch['description']) }}</td>
                <td>{{ date("d M, Y", strtotime($atch['date_applied'])) }}</td>
                <td>{{ $tz::utcToSpecificTimezone($atch['updated_at'], $timezone, "d-M-Y, H:i") }}</td>
                <td><a href="{{ route('attachment-profile-download', ['file' => bin2hex($atch['file']) ])}}">Download</a></td>
                <td>
                    <!-- if logged-in-user has required permission-->
                    @if( in_array("profile_attachment_all_users_edit", $viewerPermissions) )
                        <button type="button" class="btnEdit btnEditAtch iconbutton" data-toggle="tooltip" data-placement="left" title="Edit"><i class="fa fa-edit" data-atch-id="{{$atch['id']}}"></i></button>
                        &nbsp
                    @endif
                    @if( in_array("profile_attachment_all_users_delete", $viewerPermissions) )
                        <button type="button" class="btnDelete btnShowDeleteAtchModal iconbutton" data-toggle="tooltip" data-placement="left" title="Delete"><i class="fa fa-trash" data-atch-id="{{$atch['id']}}"></i></button>
                    @endif
                </td>
            </tr>
            <tr id="editable-atch-row-{{$atch['id']}}" style="display:none;">
                <form class="attachment-edit-form" method="post" action="{{ route('attachment-profile-edit') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ $atch['id'] }}"/>
                    <input type="hidden" name="user_id" value="{{ $userdata['id'] }}"/>
                    <td>
                        <select class="form-control" name="attachment_type_codename" required>
                            @foreach($attachmentTypes as  $atchType)
                            <option value="{{ $atchType['codename'] }}" 
                                @if($atch['attachment_type_codename'] == $atchType['codename']) selected @endif>{{ ucfirst($atchType['title']) }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td><input type="text" name="description" required value="{{ $atch['description'] }}"/></td>
                    <td><input class="date_applied" type="text" name="date_applied" required value="{{ date('d-m-Y', strtotime($atch['date_applied'])) }}"/></td>
                    <td>{{ $tz::utcToSpecificTimezone($atch['updated_at'], $timezone, "d-M-Y, H:i") }}</td>
                    <td><input type="file" name="file" id="file" placeholder="Upload Attachment"/></td>
                    <td><button type="submit" class="btnSaveAtchEdit smallbutton" data-atch-id="{{$atch['id']}}">Save</button></td>
                </form>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif

