
<table class="display maintable" style="width:100%">
    <thead>
        <tr>
            <th>Action</th>
            <th>Emp ID</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Work Email</th>
            <th>Personal Email</th>
            <th>Home Phone</th>
            <th>Mobile Phone</th>
            <th>Street Address</th>
            <th>City</th>
            <th>DOB</th>
            <th>Marital Status</th>
            <th>Tax Identification No.</th>
            <th>Superannuation A/c No.</th>
            <th>Bank</th>
            <th>Bank Account Name</th>
            <th>Bank Account Number</th>
            <th>BSB Number</th>
            <th>Emergency Contact</th>
            <th>Emergency Contact Phone</th>
            <th>Emergency Contact Alternative Phone</th>
            <th>Job Title</th>
            <th>Department</th>
            <th>Work Contact Number</th>
            <th>Phone Extension</th>
            <th>DID Number</th>
            <th>Hourly Rate</th>
            <th>Employment Start Date</th>
            <th>Employment End Date</th>
            <th>Work Start Time</th>
            <th>Work End Time</th>
            <th>Weekly Normal Hours</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <td>
            <a href="{{ route('useredit-form', ['user_id' => $user['id'] ]) }}" class="btnEdit iconbutton" data-toggle="tooltip" data-placement="left" title="Edit"><i class="fa fa-edit"></i></a>
            &nbsp
            <button type="button" class="btnDelete iconbutton" data-toggle="tooltip" data-placement="left" title="Archive"><i class="fa fa-trash" data-user-id="{{$user['id']}}"></i></button>
        </td>
        <td>{{ $user['emp_id'] }}</td>
        <td><a href="{{ route('user-profile', ['user_id' => $user['id'] ]) }}">{{ ucfirst($user['fname']) }}</a></td>
        <td>{{ ucfirst($user['lname']) }}</td>
        <td>{{ $user['email'] }}</td>
        <td>{{ $user['personal_email'] }}</td>
        <td>{{ $user['home_phone'] }}</td>
        <td>{{ $user['mobile_phone'] }}</td>
        <td>{{ ucfirst($user['streetaddress']) }}</td>
        <td>{{ ucfirst($user['city']) }}</td>
        <td>{{ $user['dob'] }}</td>
        <td>{{ $user['marital_status'] }}</td>
        <td>{{ $user['tin_number'] }}</td>
        <td>{{ $user['fnpf_number'] }}</td>
        <td>{{ $user['bank_name'] }}</td>
        <td>{{ $user['bank_account_name'] }}</td>
        <td>{{ $user['bank_account_number'] }}</td>
        <td>{{ $user['bsb_number'] }}</td>
        <td>{{ ucfirst($user['emergency_contact_name']) }}</td>
        <td>{{ $user['emergency_contact_phone'] }}</td>
        <td>{{ $user['emergency_contact_phone2'] }}</td>
        <td>{{ $user['job_title'] }}</td>
        <td>{{ $user['role'] }}</td>
        <td>{{ $user['cug_number'] }}</td>
        <td>{{ $user['phone_extension'] }}</td>
        <td>{{ $user['did_number'] }}</td>
        <td>$ {{ number_format((float) $user['hourly_rate'], 2, '.', '') }}</td>
        <td>{{ $user['emp_startdate'] }}</td>
        <td>{{ $user['emp_enddate'] }}</td>
        <td>{{ date("h:i a", strtotime($user['work_starttime'])) }}</td>
        <td>{{ date("h:i a", strtotime($user['work_endtime'])) }}</td>
        <td>{{ $user['weekly_normal_hours'] }}</td>
    </tr>
    @endforeach
    </tbody>
</table>

