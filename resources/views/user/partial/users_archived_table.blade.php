
<table class="display maintable" style="width:100%">
    <thead>
        <tr>
            <th>Action</th>
            <th>Emp ID</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Work Email</th>
            <th>DOB</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <td>
            <button type="button" class="btnUnarchive iconbutton" data-toggle="tooltip" data-placement="left" title="Restore"><i class="fas fa-trash-restore-alt" data-user-id="{{$user['id']}}"></i></button>
        </td>
        <td>{{ $user['emp_id'] }}</td>
        <td><a href="{{ route('user-profile', ['user_id' => $user['id'] ]) }}">{{ ucfirst($user['fname']) }}</a></td>
        <td>{{ ucfirst($user['lname']) }}</td>
        <td>{{ $user['email'] }}</td>
        <td>{{ $user['dob'] }}</td>
    </tr>
    @endforeach
    </tbody>
</table>

