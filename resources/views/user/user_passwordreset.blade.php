@extends('layouts.app')

@section('assets')

<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
    <form id="myform" method="post" action="{{ route('user-passwordreset') }}" enctype="multipart/form-data">
        @csrf
      <h2 class="pageheading">Reset User's Password</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="team_leader">User<span>*</span></label>
            <select class="form-control" id="id" name="id" required>
              <option disabled selected value> -- Select a user -- </option>
              @foreach($users as  $user)
              <option value="{{ $user['id'] }}"
                  @if ( strcmp($user['id'], old('user_id') ) === 0 )
                      selected="selected"
                  @endif
              >{{ ucfirst($user['fname']) }} {{ ucfirst($user['lname']) }}</option>
              @endforeach
          </select>
          </div>
		</div>
    </fieldset>
      <div class="btn-block">
        <button type="submit">Reset Password</button>
      </div>
	</div>
  </form>
    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
@endsection
