@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <h2 class="pageheading">Archived Users</h2>
    <div id="users-div" class="table-container">
        @include('user.partial.users_archived_table')
    </div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({    
        });

        $('[data-toggle="tooltip"]').tooltip();  

        $(document).on("click",".btnUnarchive",function(e) {
            var buttonJustClicked = e.target;
            var userId = $(buttonJustClicked).data("user-id"); 
      
            var data = new FormData();
            data.append("id",userId);
          
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                enctype: 'multipart/form-data',
                url: "{{ route('user-unarchive') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000000000,
                success: function (response) {
                    var jsonObj = JSON.parse(response); //convert JSON string to JSON object

                    if (jsonObj.success == true) {
                        $('#users-div').html(jsonObj.html);
                        $('.maintable').DataTable({});
                    } else {
                        swal("Failed", jsonObj.message, "error");
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            }); // $.ajax
        });
    });
</script>
@endsection
