@extends('layouts.app')

@section('assets')

@endsection

@section('content')
    <h2 class="pageheading">User Profiles</h2>
    <div class="center-box">
        @foreach ($users as $user)
            <a class="single-profile-link" href="{{ route('user-profile', ['user_id' => $user['id'] ]) }}">
                <div class="single-profile-icon">
                    <div class="single-profile-imgbox">
                        <?php $profilepicpath = 'images/'. $user['profilepic'] ?>
                        <img class="single-profile-img" src="{{ url($profilepicpath) }}"/>
                    </div>
                    <div class="single-profile-namebox">
                        {{ strtoupper($user['fname'])}} {{ strtoupper($user['lname'])}}
                    </div>
                </div> 
            </a>   
        @endforeach

        <div class="paginate-div">
            {{ $users->render() }}
        </div>
    </div>
@endsection

@section('scripts')

@endsection
