@inject('tz', 'App\Lib\Utils\Timezone') <!-- include Timezone Class -->

@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/customform.css') }}?rand={{mt_rand()}}">
<link rel="stylesheet" href="{{ asset('css/profile.css') }}?rand={{mt_rand()}}">
@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif

<div class="profile-wrapper">

	  <div class="additional-block estateinfo-block">
		<div class="box-container">
		  <div class="image-container">
		  	<?php $profilepicpath = 'images/'. $userdata['profilepic'] ?>
			<img id="profile-img" src="{{ url($profilepicpath) }}"/>
		  </div>
		  <div class="profile-info">
			<h2>
                {{ strtoupper($userdata['fname']) }} {{  strtoupper($userdata['lname']) }}
			</h2>
            <div class="item">
				<b>Emp ID: </b> &nbsp {{ $userdata['emp_id'] }}
			</div>
			<div class="item">
				<i class="fa fa-envelope fontawesome-icon"></i> &nbsp {{ $userdata['email'] }}
			</div>
		  </div>
		</div>
	  </div>
	  
	<!-- if logged-in-user is viewing someone else's profile AND has required permission-->
	@if( ($loggedInUser['id'] != $userdata['id'] && in_array("user_view_users_personal_data", $viewerPermissions) )
			|| $loggedInUser['id'] == $userdata['id']) <!-- OR if logged-in-user is viewing own profile -->
		<div class="additional-block">
			<h2>
			  Personal Details
			</h2>
			<div class="address-details">
				<div class="item">
					<div class="label">
						Home Phone:
					</div>
					<div class="value">
						{{ $userdata['home_phone'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Mobile Phone:
					</div>
					<div class="value">
						{{ $userdata['mobile_phone'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Street Address:
					</div>
					<div class="value">
						{{ $userdata['streetaddress'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
					  City:
					</div>
					<div class="value">
						{{ $userdata['city'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
					  Personal Email:
					</div>
					<div class="value">
						{{ $userdata['personal_email'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
					  Date of Birth:
					</div>
					<div class="value">
						{{ date("d M, Y", strtotime($userdata['dob']))  }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Marital Status:
					</div>
					<div class="value">
						{{ $userdata['marital_status'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Tax Identification No.:
					</div>
					<div class="value">
						{{ $userdata['tin_number'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Superannuation A/c No.:
					</div>
					<div class="value">
						{{ $userdata['fnpf_number'] }}
					</div>
				</div>
			</div> <!--address-details-->
		</div> <!--additional-block-->
	@endif
	
		<div class="additional-block">
			<h2>
			  Employment Details
			</h2>
			<div class="address-details">
				<div class="item">
					<div class="label">
						Start Date:
					</div>
					<div class="value">
						{{ date("d M, Y", strtotime($userdata['emp_startdate'])) }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						End Date:
					</div>
					<div class="value">
						@if ( $userdata['emp_enddate'] !== null )
							{{ date("d M, Y", strtotime($userdata['emp_enddate'])) }}
						@endif
					</div>
				</div>
				<div class="item">
					<div class="label">
						Work Start Time:
					</div>
					<div class="value">
						{{ date("h:i a", strtotime($userdata['work_starttime'])) }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Work End Time:
					</div>
					<div class="value">
						{{ date("h:i a", strtotime($userdata['work_endtime'])) }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Work Contact Number:
					</div>
					<div class="value">
						{{ $userdata['cug_number'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Phone Extension:
					</div>
					<div class="value">
						{{ $userdata['phone_extension'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						DID Number:
					</div>
					<div class="value">
						{{ $userdata['did_number'] }}
					</div>
				</div>
				<!-- if logged-in-user is viewing someone else's profile AND has required permission-->
			  @if( ($loggedInUser['id'] != $userdata['id'] && in_array("user_view_users_wage_rates", $viewerPermissions) )
				|| $loggedInUser['id'] == $userdata['id']) <!-- OR if logged-in-user is viewing own profile -->
				<div class="item">
					<div class="label">
						Hourly Rate:
					</div>
					<div class="value">
						$ {{ number_format((float) $userdata['hourly_rate'], 2, '.', '') }}
					</div>
				</div>
			  @endif
			  	<div class="item">
					<div class="label">
						Job Title:
					</div>
					<div class="value">
						{{ $userdata['job_title'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Department:
					</div>
					<div class="value">
						{{ $userdata['role'] }}
					</div>
				</div>
			</div><!--address-details-->
		</div><!--additional-block-->

	<!-- if logged-in-user is viewing someone else's profile AND has required permission-->	
	@if( ($loggedInUser['id'] != $userdata['id'] && in_array("user_view_users_bank_details", $viewerPermissions) )
		|| $loggedInUser['id'] == $userdata['id']) <!-- OR if logged-in-user is viewing own profile -->
		<div class="additional-block">
			<h2>
			  Bank Account Details
			</h2>
			<div class="address-details">
				<div class="item">
					<div class="label">
						Bank Name:
					</div>
					<div class="value">
						{{ $userdata['bank_name'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Account Name:
					</div>
					<div class="value">
						{{ $userdata['bank_account_name'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Account Number:
					</div>
					<div class="value">
						{{ $userdata['bank_account_number'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						BSB Number:
					</div>
					<div class="value">
						{{ $userdata['bsb_number'] }}
					</div>
				</div>
			</div> <!--address-details-->
		</div> <!--additional-block-->
	@endif
	
	<!-- if logged-in-user is viewing someone else's profile AND has required permission-->
	@if( ($loggedInUser['id'] != $userdata['id'] && in_array("user_view_users_emergency_contact", $viewerPermissions) )
		|| $loggedInUser['id'] == $userdata['id']) <!-- OR if logged-in-user is viewing own profile -->
		<div class="additional-block">
			<h2>
			  Emergency Contact Information
			</h2>
			<div class="address-details">
				<div class="item">
					<div class="label">
						Name:
					</div>
					<div class="value">
						{{ $userdata['emergency_contact_name'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Relationship:
					</div>
					<div class="value">
						{{ $userdata['emergency_contact_relationship'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Street Address:
					</div>
					<div class="value">
						{{ $userdata['emergency_contact_streetaddress'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						City:
					</div>
					<div class="value">
						{{ $userdata['emergency_contact_city'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Primary Phone:
					</div>
					<div class="value">
						{{ $userdata['emergency_contact_phone'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Alternative Phone:
					</div>
					<div class="value">
						{{ $userdata['emergency_contact_phone2'] }}
					</div>
				</div>
				<div class="item">
					<div class="label">
						Medical Conditions:
					</div>
					<div class="value">
						{{ $userdata['medical_conditions'] }}
					</div>
				</div>
			</div> <!--address-details-->
		</div> <!--additional-block-->
	@endif
	
	<!-- if logged-in-user is viewing someone else's profile AND has required permission-->
	@if( ($loggedInUser['id'] != $userdata['id'] && in_array("profile_attachment_all_users_view", $viewerPermissions) )
		|| $loggedInUser['id'] == $userdata['id']) <!-- OR if logged-in-user is viewing own profile -->
		<div class="additional-block">
			<h2>
				Attachments
			</h2>
			<!-- if logged-in-user is viewing someone else's profile AND has required permission-->
			@if( ($loggedInUser['id'] != $userdata['id'] && in_array("profile_attachment_all_users_add", $viewerPermissions) )
				|| $loggedInUser['id'] == $userdata['id']) <!-- OR if logged-in-user is viewing own profile -->
				<div class="container" style="margin-left:0;">
					<form id="attachmentform" method="post" action="{{ route('attachment-profile-add') }}" enctype="multipart/form-data">
						@csrf
						<input id="user_id" type="hidden" name="user_id" value="{{ $userdata['id'] }}"/>
						<div class="row">
							<div class="col-sm-5">
								<input id="description" type="text" name="description" value="{{ old('description') }}" placeholder="Description"/>
							</div>
							<div class="col-sm-5">
								<input class="date_applied" type="text" name="date_applied" value="{{ old('date_applied') }}" placeholder="Date Applied" autocomplete="off" required/>
							</div>			
						</div>
						<div class="row">
							<div class="col-sm-5">
								<select class="form-control" id="attachment_type_codename" name="attachment_type_codename" required>
									<option disabled selected value> -- Select attachment type -- </option>
									@foreach($attachmentTypes as  $atchType)
									<option value="{{ $atchType['codename'] }}">{{ ucfirst($atchType['title']) }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-5">
								<input type="file" name="file" id="file" placeholder="Upload Attachment" required/>
							</div>
							<div class="col-sm-2">
								<button type="submit" class="smallbutton">Add</button>
							</div>
						</div>
					</form>
				</div>
			@endif
			
			@include('user.partial.user_profile_attachments_table')
		</div><!--additional-block-->
	@endif
</div>

  <!-- Modal -->
<div id="atchDeleteModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Attachment</h4>
			</div>
			<form method="post" action="{{ route('attachment-profile-delete') }}">
				@csrf
				<div class="modal-body">
					Are you sure you want to delete this attachment?
					<input id="atch-delete-id" type="hidden" name="id" value=""/>
				</div>
				<div class="modal-footer">
					<button type="submit" id="btnDeleteAtch">Delete</button>
					<button type="button" id="btnCancelDeleteAtch" class="btn-negative" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$(document).ready(function(){
		$.fn.dataTable.moment('D-MMM-YYYY, HH:mm');
		$('.maintable').DataTable({
			order: [[3, 'desc']],  // order by Last Modified
			searching: false,
			lengthChange: false,   
			bInfo: false 
        });

        $('[data-toggle="tooltip"]').tooltip();

		$("#attachmentform").validate({
        });

		$(".attachment-edit-form").validate({
        });

		$( function() {
			$(".date_applied").datepicker({
				dateFormat: "dd-mm-yy"
			});
		});

		$(document).on("click",".btnEditAtch", function(e){
			var buttonJustClicked = e.target;
			var atchId = $(buttonJustClicked).data("atch-id"); 
			
			$('#atch-row-'+ atchId).hide();
			$('#editable-atch-row-'+ atchId).show();
		});

		$(document).on("click",".btnShowDeleteAtchModal", function(e) {
			var atchId = $(e.target).data('atch-id');
			$('#atch-delete-id').val(atchId);
			$('#atchDeleteModal').modal('show');
		});
	});
</script>
@endsection
