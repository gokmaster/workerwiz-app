@if (count($holidays))
    <table class="maintable">
        <thead>
        <tr>
            <th>
                Holiday
            </th>
            <th>
                Date
            </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($holidays as $h)
            <tr id="row-{{$h['id']}}">
                <td>
                    {{ ucfirst($h['holiday_name']) }}
                </td>
                <td>
                    {{ date("d M, Y", strtotime($h['holiday_date'])) }}
                </td>
                <td>
                    <button type="button" class="btnEdit iconbutton"><i class="fa fa-edit" data-holiday-id="{{$h['id']}}"></i></button>
                    <button type="button" class="btnDelete iconbutton"><i class="fa fa-trash" data-holiday-id="{{$h['id']}}"></i></button>
                </td>
            </tr>
            <tr id="editable-row-{{$h['id']}}" style="display:none;">
                <td><input id="holiday-name-{{$h['id']}}" type="text" required value="{{ $h['holiday_name'] }}"/></td>
                <td><input id="holiday-date-{{$h['id']}}" class="txtHolidayDate" type="text" required value="{{ $h['holiday_date'] }}"/></td>
                <td><button type="button" class="btnSave smallbutton" data-holiday-id="{{$h['id']}}">Save</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif