@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('holiday-create') }}" enctype="multipart/form-data">
      @csrf
      <h2 class="pageheading">Add Public Holiday</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <div class="colums">
          <div class="item">
            <label for="holiday_name">Holiday name<span>*</span></label>
            <input id="holiday_name" type="text" name="holiday_name" value="{{ old('holiday_name') }}" required/>
          </div>
          <div class="item">
            <label for="holiday_date">Holiday date<span>*</span></label>
            <input id="holiday_date" type="text" name="holiday_date" value="{{ old('holiday_date') }}" autocomplete="off" required/>
          </div>
		</div>
    </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
      <br>
     
      <div id="holiday-div">
        @include('holiday.partial.holiday_table')
      </div>
</form>    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $("#myform").validate();

    $( "#holiday_date" ).datepicker({
      dateFormat: "dd-mm-yy"
    });

    $( ".txtHolidayDate" ).datepicker({
      dateFormat: "dd-mm-yy"
    });

    $(document).on("click",".btnEdit", function(e){
        var buttonJustClicked = e.target;
        var holidayId = $(buttonJustClicked).data("holiday-id"); 
        
        $('#row-'+ holidayId).hide();
        $('#editable-row-'+ holidayId).show();
    });

    $(document).on("click",".btnDelete", function(e){
      var buttonJustClicked = e.target;
      var holidayId = $(buttonJustClicked).data("holiday-id"); 
      
      var data = new FormData();
      data.append("id",holidayId);
          
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('holiday-delete') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object

          if (jsonObj.success == true) {
            $('#holiday-div').html(jsonObj.html);
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
        }
      }); // $.ajax
    });

    $(document).on("click",".btnSave", function(e){
      var buttonJustClicked = e.target;
      var holidayId = $(buttonJustClicked).data("holiday-id"); 
      var holidayName = $('#holiday-name-' + holidayId).val();
      var holidayDate = $('#holiday-date-' + holidayId).val();
      
      var data = new FormData();
      data.append("id",holidayId);
      data.append("holiday_name", holidayName);
      data.append("holiday_date", holidayDate);
      
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        enctype: 'multipart/form-data',
        url: "{{ route('holiday-edit') }}",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 6000000000,
        success: function (response) {
          var jsonObj = JSON.parse(response); //convert JSON string to JSON object

          if (jsonObj.success == true) {
            $('#holiday-div').html(jsonObj.html);
          } else {
            swal("Failed", jsonObj.message, "error");
          }
        },
        error: function (e) {
          console.log("ERROR : ", e);
        }
      }); // $.ajax
    }); 
  });
</script>
@endsection
