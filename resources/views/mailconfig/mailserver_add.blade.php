@extends('layouts.app')

@section('assets')
<!-- CSS for custom form -->
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
<link rel="stylesheet" href="{{ asset('css/customform.css') }}">
@endsection

@section('content')
<form id="myform" method="post" action="{{ route('mailserver-add') }}" enctype="multipart/form-data">
      @csrf
      <h2 class="pageheading">Add Mail Server</h2>
      <br/>
        @if(session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @elseif(session()->has('fail_message'))
            <div class="alert alert-danger">
                {{ session()->get('fail_message') }}
            </div>
        @endif
      <fieldset>
        <legend>Mail Server Settings</legend>
        <div class="colums">
          <div class="item">
            <label for="driver">Driver<span>*</span></label>
            <select class="form-control" id="driver" name="driver" required>
                <option value="smtp">SMTP</option>
                <option value="imap">IMAP</option>
            </select>
          </div>
          <div class="item">
            <label for="host">Host<span>*</span></label>
            <input id="host" type="text" name="host" value="{{ old('host') }}" required/>
          </div>
          <div class="item">
            <label for="port">Port<span>*</span></label>
            <input id="port" type="text" name="port" value="{{ old('port') }}" required/>
          </div>
          <div class="item">
            <label for="encryption">Encryption</label>
            <input id="encryption" type="text" name="encryption" value="{{ old('encryption') }}"/>
          </div>
          <div class="item">
            <label for="username">Username<span>*</span></label>
            <input id="username" type="email" name="username" value="{{ old('username') }}" required/>
          </div>
          <div class="item">
            <label for="password">Password<span>*</span></label>
            <input id="password" type="password" name="password" value="{{ old('password') }}" required/>
          </div>
          <div class="item">
            <label for="from_address">From Email address<span>*</span></label>
            <input id="from_address" type="email" name="from_address" value="{{ old('from_address') }}" required/>
          </div>
          <div class="item">
            <label for="from_name">From Name</label>
            <input id="from_name" type="text" name="from_name" value="{{ old('from_name') }}"/>
          </div>
		</div>
    </fieldset>
      <div class="btn-block">
        <button type="submit">Save</button>
      </div>
</form>    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $("#myform").validate();
  });
</script>
@endsection
