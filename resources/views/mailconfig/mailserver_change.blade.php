@extends('layouts.app')

@section('assets')
<link rel="stylesheet" href="{{ asset('css/custominput.css') }}">
@endsection

@section('content')
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @elseif(session()->has('fail_message'))
        <div class="alert alert-danger">
            {{ session()->get('fail_message') }}
        </div>
    @endif
    <h2 class="pageheading">Change Mail Server</h2>
    
    <div class="table-container">    
        <table class="display maintable" style="width:100%">
            <thead>
                <tr>
                  <th>Host</th>
                  <th>Driver</th>
                  <th>Port</th>
                  <th>Username</th>
                  <th>From Email Address</th>
                  <th>From Name</th>
                  <th>Encryption</th>
                  <th>Currently Used</th>
                </tr>
            </thead>
            <tbody>
            @foreach($mailservers as $m)
                <tr>
                  <td>{{ $m['host'] }}</td>
                  <td>{{ $m['driver'] }}</td>
                  <td>{{ $m['port'] }}</td>
                  <td>{{ $m['username'] }}</td>
                  <td>{{ $m['from_address'] }}</td>
                  <td>{{ $m['from_name'] }}</td>
                  <td>{{ $m['encryption'] }}</td>
                  <td>
                    @if ($m['in_use'] == 1)
                      Yes
                    @else
                      <button class="btnShowMailServerChangeModal longbutton" type="button" data-mailhost="{{$m['host']}}" data-mailconfig-id="{{$m['id']}}">Change to this</button>
                    @endif
                  </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

     <!-- Modal -->
    <div id="mailServerChangeModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Mail Server</h4>
                </div>
                <div id="mailServerChangeBody" class="modal-body">
                    <form method="post" action="{{ route('mailserver-change') }}">
                      @csrf
                      Change mail server to <span id="mailhost-span"></span>?
                      <input type="hidden" id="mailconfig_id" name="mailconfig_id" value=""/>
                      <div class="btn-block">
                        <button type="submit">Change</button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>  

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('.maintable').DataTable({  
            
        });

        $(document).on("click",".btnShowMailServerChangeModal", function(e){
          var mailconfigId = $(e.target).data('mailconfig-id');
          var mailhost = $(e.target).data('mailhost');

          $('#mailconfig_id').val(mailconfigId);
          $('#mailhost-span').text(mailhost);

          $('#mailServerChangeModal').modal('show');
        });
    });
</script>
@endsection
