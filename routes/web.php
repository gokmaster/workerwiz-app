<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// test mail
// Route::get('mail/sendbasic','MailController@basic_email');
// Route::get('mail/sendhtml','MailController@html_email');
// Route::get('mail/sendattachment','MailController@attachment_email');

// AuthController
Route::post('accesstoken/receive', 'AuthController@receiveAccessToken')->name('receive-accesstoken');

// UserController
Route::get('signup', 'UserController@signupForm')->name('signup');
Route::post('signup/submit', 'UserController@signup')->name('user-signup');
Route::get('signup/emailverify/{verification_code}', 'UserController@signupEmailVerify')->name('signup-email-verify');
Route::get('user/invited/create/{verification_code}', 'UserController@invitedUserCreateForm')->name('invited-user-create-form');
Route::post('user/invited/create/submit', 'UserController@invitedUserCreate')->name('invited-user-create');

// EmailQueController
Route::post('emailque/unsent/send', 'EmailQueController@queuedEmailsSend')->name('emailque-unsent-send');

// Routes inside this group will require user to be logged in before they can access this routes
// The preventBackHistory middleware will prevent browsers' Back-button from loading cached pages after logout
Route::group(['middleware' => ['accessTokenVerify', 'preventBackHistory']], function() {
    // UserController
    Route::get('user/create', 'UserController@createUserForm')->name('usercreate-form')->middleware('checkPermission');
    Route::post('user/create/submit', 'UserController@createUser')->name('user-create');
    Route::get('user/edit/{user_id}', 'UserController@editUserForm')->name('useredit-form')->middleware('checkPermission');
    Route::post('user/edit/submit', 'UserController@editUser')->name('user-edit');
    Route::get('user/own/edit', 'UserController@userEditOwnForm')->name('useredit-own-form')->middleware('checkPermission');
    Route::post('user/own/edit/submit', 'UserController@userEditOwn')->name('useredit-own');
    Route::post('user/delete/submit', 'UserController@deleteUser')->name('user-delete');
    Route::get('user/all', 'UserController@allUsers')->name('user-all')->middleware('checkPermission');
    Route::get('user/profile/all', 'UserController@allProfiles')->name('user-profile-all');
    Route::get('user/profile/{user_id}', 'UserController@userProfile')->name('user-profile')->middleware('checkPermission');
    Route::get('user/password/change', 'UserController@passwordChangeForm')->name('passwordchange-form');
    Route::post('user/password/change/submit', 'UserController@passwordChange')->name('passwordchange');
    Route::get('user/password/reset', 'UserController@resetUserPasswordForm')->name('user-passwordreset-form')->middleware('checkPermission');
    Route::post('user/password/reset/submit', 'UserController@resetUserPassword')->name('user-passwordreset');
    Route::get('user/archived/all', 'UserController@allArchivedUsers')->name('user-archived-all')->middleware('checkPermission');
    Route::post('user/unarchive/submit', 'UserController@unArchiveUser')->name('user-unarchive');
    Route::get('user/invite', 'UserController@userInviteCreateForm')->name('userinvite-form')->middleware('checkPermission');
    Route::post('user/invite/create/submit', 'UserController@userInviteCreate')->name('user-invite-create');
    
    // TimesheetController
    Route::get('timesheet', 'TimesheetController@timesheetform')->name('timesheet-form')->middleware('checkPermission');
    Route::post('timesheet/add/submit', 'TimesheetController@timesheetAdd')->name('timesheet-add');
    Route::post('timesheet/edit/submit', 'TimesheetController@timesheetEdit')->name('timesheet-edit');
    Route::get('timesheet/approve', 'TimesheetController@timesheetApproveForm')->name('timesheet-approve')->middleware('checkPermission');
    Route::post('timesheet/userrecords', 'TimesheetController@timesheetUserRecords')->name('timesheet-userrecords');
    Route::get('timesheet/users/all', 'TimesheetController@timesheetAllUsers')->name('timesheet-allusers')->middleware('checkPermission');
    Route::post('timesheet/approve/all', 'TimesheetController@approveAll')->name('timesheet-approve-all');

    // TeamController
    Route::get('team/create', 'TeamController@teamCreateForm')->name('teamcreate-form')->middleware('checkPermission');
    Route::post('team/create/submit', 'TeamController@teamCreate')->name('team-create');
    Route::get('team/all', 'TeamController@teamAll')->name('team-all')->middleware('checkPermission');
    Route::get('team/members/edit/{team_id}/{encoded_team_name}', 'TeamController@membersEditForm')->name('team-members-edit-form')->middleware('checkPermission');
    Route::post('team/members/edit', 'TeamController@membersEdit')->name('team-members-edit');
    Route::get('team/{team_id}', 'TeamController@team')->name('team')->middleware('checkPermission');

    // RoleController
    Route::get('role/create', 'RoleController@roleCreateForm')->name('rolecreate-form')->middleware('checkPermission');
    Route::post('role/create/submit', 'RoleController@roleCreate')->name('role-create');
    Route::get('role/edit/{role_id}', 'RoleController@roleEditForm')->name('roleedit-form')->middleware('checkPermission');
    Route::post('role/edit/submit', 'RoleController@roleEdit')->name('role-edit');
    Route::get('role/all', 'RoleController@roleAll')->name('role-all')->middleware('checkPermission');

    // KpiController
    Route::get('kpi/create', 'KpiController@kpiCreateForm')->name('kpi-create-form')->middleware('checkPermission');
    Route::post('kpigroup/create/submit', 'KpiController@kpiGroupCreate')->name('kpigroup-create');
    Route::post('kpidetail/create/submit', 'KpiController@kpiDetailCreate')->name('kpi-detail-create');
    Route::post('kpi/assign/togroup/html', 'KpiController@kpiAssignToGroupHtml')->name('kpi-group-assign-html');
    Route::post('kpi/assign/togroup', 'KpiController@kpiAssignToGroup')->name('kpi-group-assign');
    Route::get('kpi/assign/touser', 'KpiController@kpiAssignToUserForm')->name('kpi-user-assign-form')->middleware('checkPermission');
    Route::post('kpi/assign/user/html', 'KpiController@kpiAssignToUserHtml')->name('kpi-user-assign-html');
    Route::post('kpi/assign/touser/submit', 'KpiController@kpiAssignToUser')->name('kpi-user-assign');
    Route::get('kpi/entry/foruser', 'KpiController@userKpiEntryForm')->name('user-kpi-entry-form')->middleware('checkPermission');
    Route::post('kpi/entry/foruser/html', 'KpiController@userKpisForEntryHtml')->name('user-kpi-entry-html');
    Route::post('kpi/foruser/update', 'KpiController@userKpiUpdate')->name('user-kpi-update');
    Route::get('kpi/report', 'KpiController@kpiReport')->name('kpi-report')->middleware('checkPermission');
    Route::post('kpi/report/html', 'KpiController@kpiReportHtml')->name('kpi-report-html');

    // PayrollController
    Route::get('payslip/generate', 'PayrollController@payslipGenerateForm')->name('payslip-generate-form')->middleware('checkPermission');
    Route::post('payslip/generate/submit', 'PayrollController@generatePayslips')->name('payslips-generate');
    Route::post('payroll/mastersheet/upload', 'PayrollController@excelMasterSheetUpload')->name('payroll-mastersheet-upload');
    Route::get('payroll/mastersheet/download/{filename}', 'PayrollController@uploadedExcelDownload')->name('payroll-mastersheet-download');
    Route::get('payslip', 'PayrollController@payslipOfUser')->name('payslip-ofuser')->middleware('checkPermission');
    Route::get('payslip/download/{filepath}', 'PayrollController@payslipDownload')->name('payslip-download');
    Route::get('payslip/users/all', 'PayrollController@payslipAllUsers')->name('payslip-users-all')->middleware('checkPermission');
    Route::post('payslip/ofuser/table/html', 'PayrollController@payslipOfUserTableHtml')->name('payslip-ofuser-table-html');
    Route::get('payslip/generator/exceltemplate/download', 'PayrollController@excelTemplateForPayslipGeneratorDownload')->name('payslipgen-exceltemplate-download');
    
    // HolidayController
    Route::get('holiday/add', 'HolidayController@holidayCreateForm')->name('holiday-create-form')->middleware('checkPermission');
    Route::post('holiday/add/submit', 'HolidayController@holidayCreate')->name('holiday-create');
    Route::post('holiday/edit/submit', 'HolidayController@holidayEdit')->name('holiday-edit');
    Route::post('holiday/delete/submit', 'HolidayController@holidayDelete')->name('holiday-delete');

    // DepartmentController
    Route::get('department/add', 'DepartmentController@departmentCreateForm')->name('department-create-form')->middleware('checkPermission');
    Route::post('department/add/submit', 'DepartmentController@departmentCreate')->name('department-create');
    Route::post('department/edit/submit', 'DepartmentController@departmentEdit')->name('department-edit');
    Route::post('department/delete/submit', 'DepartmentController@departmentDelete')->name('department-delete');

    // JobtitleController
    Route::get('jobtitle/add', 'JobtitleController@jobtitleCreateForm')->name('jobtitle-create-form')->middleware('checkPermission');
    Route::post('jobtitle/add/submit', 'JobtitleController@jobtitleCreate')->name('jobtitle-create');
    Route::post('jobtitle/edit/submit', 'JobtitleController@jobtitleEdit')->name('jobtitle-edit');
    Route::post('jobtitle/delete/submit', 'JobtitleController@jobtitleDelete')->name('jobtitle-delete');
    
    // RepController
    Route::get('rep/add', 'RepController@repCreateForm')->name('rep-create-form')->middleware('checkPermission');
    Route::post('rep/add/submit', 'RepController@repCreate')->name('rep-create');
    Route::post('rep/edit/submit', 'RepController@repEdit')->name('rep-edit');
    Route::post('rep/delete/submit', 'RepController@repDelete')->name('rep-delete');

    // AppointmentController
    Route::get('appointment/create', 'AppointmentController@appointmentCreateForm')->name('appointment-create-form')->middleware('checkPermission');
    Route::post('appointment/create/submit', 'AppointmentController@appointmentCreate')->name('appointment-create');
    Route::get('appointment/edit/{appt_id}', 'AppointmentController@appointmentEditForm')->name('appointment-edit-form')->middleware('checkPermission');
    Route::get('appointment/view/{appt_id}', 'AppointmentController@appointmentView')->name('appointment-view')->middleware('checkPermission');
    Route::post('appointment/edit/submit', 'AppointmentController@appointmentEdit')->name('appointment-edit');
    Route::post('appointment/edit/json/submit', 'AppointmentController@appointmentEditJson')->name('appointment-edit-json'); // returns json response
    Route::get('appointment/livediary', 'AppointmentController@appointmentsLiveDiary')->name('appointment-rep')->middleware('checkPermission');
    Route::post('appointment/livediary/table/html', 'AppointmentController@appointmentsRepTableHtml')->name('appointment-rep-table-html');
    Route::get('appointment/confirm', 'AppointmentController@appointmentConfirmForm')->name('appointment-confirm-form')->middleware('checkPermission');
    Route::post('appointment/confirm/submit', 'AppointmentController@appointmentConfirm')->name('appointment-confirm');
    Route::post('appointment/drop/submit', 'AppointmentController@appointmentDrop')->name('appointment-drop');
    Route::post('appointment/reschedule/submit', 'AppointmentController@appointmentReschedule')->name('appointment-reschedule');
    Route::get('appointment/iced', 'AppointmentController@appointmentsIcedFetch')->name('appointment-iced')->middleware('checkPermission');
    Route::get('appointment/dropped', 'AppointmentController@appointmentsDroppedFetch')->name('appointment-dropped')->middleware('checkPermission');
    Route::get('appointment/archived', 'AppointmentController@appointmentsArchivedFetch')->name('appointment-archived')->middleware('checkPermission');
    Route::post('appointment/leadpdf/send', 'AppointmentController@sendLeadPdf')->name('appointment-lead-pdf-send');
    Route::get('appointment/block/timeslot', 'AppointmentController@blockTimeSlotsForm')->name('appointment-block-timeslot-form')->middleware('checkPermission');
    Route::post('appointment/block/timeslot/submit', 'AppointmentController@blockTimeslot')->name('appointment-block-timeslot');
    Route::post('appointment/blockedtimeslot/remove/submit', 'AppointmentController@blockedTimeslotRemove')->name('appointment-blocktimeslot-remove');
    Route::get('confirmers/add', 'AppointmentController@confirmersAddForm')->name('confirmers-add-form')->middleware('checkPermission');
    Route::post('confirmers/add/submit', 'AppointmentController@confirmersAdd')->name('confirmers-add');

    // ErrorController
    Route::get('permission/denied', 'ErrorController@permissionDenied')->name('permission-denied');

    // SuscriptionController
    Route::get('subscription/upgrade/{expired}', 'SubscriptionController@upgradeSubscriptionForm')->name('subscription-upgrade');
    Route::post('subscription/order/submit', 'SubscriptionController@order')->name('subscription-order');
    Route::get('subscription/payment', 'SubscriptionController@paymentForm')->name('subscription-payment-form');
    Route::post('subscription/payment/submit', 'SubscriptionController@payment')->name('subscription-payment');
    Route::get('subscription/payment/response', 'SubscriptionController@paymentResponsePage')->name('subscription-payment-response');
    Route::get('subscription/order/all', 'SubscriptionController@ordersAll')->name('subscription-order-all')->middleware('checkPermission');
    Route::post('subscription/forcompany/create/submit', 'SubscriptionController@companySubscriptionCreate')->name('company-subscription-create');

    // AttachmentController
    Route::post('attachment/profile/add/submit', 'AttachmentController@profileAttachmentAdd')->name('attachment-profile-add');
    Route::post('attachment/profile/edit/submit', 'AttachmentController@profileAttachmentEdit')->name('attachment-profile-edit');
    Route::post('attachment/profile/delete/submit', 'AttachmentController@profileAttachmentDelete')->name('attachment-profile-delete');
    Route::get('attachment/profile/download/{file}', 'AttachmentController@profileAttachmentDownload')->name('attachment-profile-download');

    // SaleController
    Route::get('sale/add', 'SaleController@saleAddForm')->name('sale-add-form')->middleware('checkPermission');
    Route::post('sale/add/submit', 'SaleController@saleAdd')->name('sale-add');
    Route::get('sale/edit/{sale_id}', 'SaleController@saleEditForm')->name('sale-edit-form')->middleware('checkPermission');
    Route::post('sale/edit/submit', 'SaleController@saleEdit')->name('sale-edit');
    Route::get('sales', 'SaleController@salesAll')->name('sale-all')->middleware('checkPermission');
    Route::get('sales/my', 'SaleController@salesOfAgent')->name('sale-my')->middleware('checkPermission');
    Route::get('sale/details/{sale_id}', 'SaleController@saleDetails')->name('sale-details')->middleware('checkPermission');

    // CustomerController
    Route::get('customer/create', 'CustomerController@createForm')->name('customer-create-form')->middleware('checkPermission');
    Route::post('customer/create/submit', 'CustomerController@create')->name('customer-create');
    Route::get('customer/edit/{customer_id}', 'CustomerController@editForm')->name('customer-edit-form')->middleware('checkPermission');
    Route::post('customer/edit/submit', 'CustomerController@edit')->name('customer-edit');
    Route::post('customer/fetchby/name', 'CustomerController@fetchByNameEmailPhone')->name('customer-fetchbyname');
    Route::get('customer/all', 'CustomerController@customerAll')->name('customer-all')->middleware('checkPermission');
    Route::get('customer/profile/{customer_id}', 'CustomerController@customerProfile')->name('customer-profile')->middleware('checkPermission');

    // EmailcampaignController
    Route::get('emailcampaign/create', 'EmailcampaignController@createForm')->name('emailcampaign-create-form')->middleware('checkPermission');
    Route::post('emailcampaign/create/submit', 'EmailcampaignController@create')->name('emailcampaign-create');
    Route::get('emailcampaign/send', 'EmailcampaignController@sendCampaignForm')->name('emailcampaign-send-form')->middleware('checkPermission');
    Route::post('emailcampaign/send/submit', 'EmailcampaignController@sendCampaign')->name('emailcampaign-send');

    // MailinglistController
    Route::get('mailinglist/create', 'MailinglistController@createForm')->name('mailinglist-create-form')->middleware('checkPermission');
    Route::post('mailinglist/create/submit', 'MailinglistController@create')->name('mailinglist-create');

    // MailconfigController
    Route::get('mailserver/add', 'MailconfigController@mailServerAddForm')->name('mailserver-add-form')->middleware('checkPermission');
    Route::post('mailserver/add/submit', 'MailconfigController@mailServerAdd')->name('mailserver-add');
    Route::get('mailserver/change', 'MailconfigController@mailServerChangeForm')->name('mailserver-change-form')->middleware('checkPermission');
    Route::post('mailserver/change/submit', 'MailconfigController@mailServerChange')->name('mailserver-change');
});


